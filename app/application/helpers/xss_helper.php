<?php 
function xss_array(array $array)
{
	foreach ($array as $key => $value) {
		if(is_array($value))
		{
			$array[$key] = xss_array((array) $value);
		} else if(is_object($value))
		{
			$array[$key] = (object) xss_array((array) $value);
		} else {
			$array[$key] = htmlentities($value, ENT_QUOTES, 'UTF-8');
		}
	}
	return $array;
}
function between($string,$one,$two){
	$o = explode($one, $string);
	if(count($o) != 2)
		return null;
	$a = explode($two, $o[1]);
	return $a[0];
}
function findarray(string $required,array $array)
{
	if($required == "")
	{
		return $array;
	}
	$split = explode('.',$required);
	$found = false;
	foreach ($split as $key => $value) {
		$found = false;
		if(array_key_exists($value, $array))
		{
			$array = $array[$value];
			$found = true;
		} 
	}
	if($found)
	{
		return $array;
	}
	return null;
}