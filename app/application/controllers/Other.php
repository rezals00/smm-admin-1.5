<?php 
class Other extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$curl = $this->curl->curl;
		$this->curl = $curl;
	}
	public function landing()
	{
		if(file_exists('.dashboard.html'))
		{
			$landing = file_get_contents('.dashboard.html');
			if($landing == "")
			{
				return redirect('auth/signin');
			}
			echo $landing;
		}
	}
	public function forgot()
	{
	if($this->session->auth) return redirect('user/dashboard');
		if($this->input->method(TRUE) == 'POST')
		{
            if ($this->form_validation->run('auth/forgot') == FALSE)
            {
                $this->session->set_flashdata('messages', "<div class='alert alert-danger'>".validation_errors()."</div>");
                return redirect(base_url('auth/forgot'));
            }
            $username = $this->input->post('username');
            $email_input = $this->input->post('email');

            $user = $this->db->select('*')->from('users')->where('username',$username)->or_where('email',$email_input)->get();
            if($user->num_rows() == 1)
            {
            	$us = $user->result()[0];
            	$email = $email_input;
            	if($us->email != "")
            	{
            		$email = $us->email;
            	} else if($us->email != $email)
            	{
            		$this->session->set_flashdata('messages', "<div class='alert alert-danger'>User exist but email not same.</div>");
	            return redirect(base_url('auth/forgot'));
            	} else {
            		$this->Users->update($user->id,array('email' => $email));
            	}
            	if(!filter_var($email,FILTER_VALIDATE_EMAIL))
            	{
            		$this->session->set_flashdata('messages', "<div class='alert alert-danger'>User Exist But Doesn't Set Email , Contact Admin For support.</div>");
	            return redirect(base_url('auth/forgot'));
            	}
            	$code = rand(1111,9999);
            	$config = array(
	                'protocol' => 'smtp',
	                'smtp_host' => 'ssl://'.$this->Site->smtp_host,
	                'smtp_port' => 465,
	                'smtp_user' => $this->Site->smtp_username,
	                'smtp_pass' => $this->Site->smtp_password,
	                'mailtype'  => 'text', 
	                'charset'   => 'iso-8859-1',
	                'smtp_timeout' => 60,
	            );
	            $this->load->library('email',$config);
	            $body = "<!--- BEGIN SHA 512 ---> \n".hash('sha512', time())."\n";
	            $body .= "\n \n \nVerification Code : {$code} \n \n \n";
	            $body .= "<!-- END SHA 512 --> \n".hash('sha512',time()+$code);
	            $this->email->from($this->Site->smtp_username, 'Support');
	            $this->email->to($email);
	            $this->email->subject('Forgot Password Requested,Verification Code');
	            $this->email->message($body);

	            $send = $this->email->send();
	            if($send)
	            {
	            	$this->session->set_userdata('forgot_code',$code);
	            	$this->session->set_userdata('forgot_id',$us->id);
	                $this->session->set_flashdata('messages', "<div class='alert alert-success'>We Have Send Verification Code , Check It.</div>");
                	return redirect(base_url('auth/forgot_password'));
	            }
	            $this->session->set_flashdata('messages', "<div class='alert alert-danger'>Oops Something Went Wrong.</div>");
	            return redirect(base_url('auth/forgot'));

            }
            $this->session->set_flashdata('messages', "<div class='alert alert-danger'>User Not Found.</div>");
	        return redirect(base_url('auth/forgot'));
		}
		if($this->input->method(TRUE) == 'GET') return $this->load->view('user/forgot');
	}
	public function forgot_password()
	{
		if($this->session->auth) return redirect('user/dashboard');
		if($this->session->userdata('forgot_code') == null)
		{
			return redirect('auth/forgot','refresh');
		}
		if($this->input->method(TRUE) == 'POST')
		{
            if ($this->form_validation->run('auth/forgot_password') == FALSE)
            {
                $this->session->set_flashdata('messages', "<div class='alert alert-danger'>".validation_errors()."</div>");
                return redirect(base_url('auth/forgot_password'));
            }
            $code = $this->input->post('code');
            $password = $this->input->post('password');
            if($code == $this->session->userdata('forgot_code'))
            {
            	$this->Users->update($this->session->userdata('forgot_id'),array('password' => password_hash($password, PASSWORD_BCRYPT, ['cost' => 12])));
            	$this->session->set_flashdata('messages', "<div class='alert alert-success'>Password Successful Changed , Now please sign in.</div>");
            	$this->session->sess_destroy();
            	 return redirect(base_url('auth/signin'));
            } 
            $this->session->set_flashdata('messages', "<div class='alert alert-danger'>Invalid Code.</div>");
	        return redirect(base_url('auth/forgot_password'));
		}
		if($this->input->method(TRUE) == 'GET') return $this->load->view('user/forgot_password');
	}
	public function signin()
	{
		if($this->session->auth) return redirect('user/dashboard');
		if($this->input->method(TRUE) == 'POST')
		{
            if ($this->form_validation->run('auth/signin') == FALSE)
            {
                $this->session->set_flashdata('messages', "<div class='alert alert-danger'>".validation_errors()."</div>");
                return redirect(base_url('auth/signin'));
            }
            if($this->Site->recaptcha_public != '' && $this->Site->recaptcha_secret != '' && $this->session->userdata('invalid_attemp') > 2)
            {
            	$response = $this->input->post('g-recaptcha-response');
            	if($response == null || $response == "")
            	{
            		$this->session->set_flashdata('messages', "<div class='alert alert-danger'>Invalid Captcha</div>");
           		    return redirect(base_url('auth/signin'));
            	}
            	$captcha =  $this->curl->get('https://www.google.com/recaptcha/api/siteverify?secret='.$this->Site->recaptcha_secret.'&response='.$response);
            	if($captcha->success !== true)
            	{
            		$this->session->set_flashdata('messages', "<div class='alert alert-danger'>Invalid Captcha</div>");
           		    return redirect(base_url('auth/signin'));
            	}

            }
<<<<<<< HEAD
            $find = $this->db->select('*')->from('users')->where('username',$this->input->post('username',true))->or_where('email',$this->input->post('username',true))->get();
            if($find->num_rows() != 1) throw new Exception("User not found", 1);
            $user = $find->result()[0];
=======
            $user = $this->Users->find(array('username' => $this->input->post('username',true)))['data'][0];
>>>>>>> 5f22bf57b58558ce378746383a0aa20003cfcef1
            $ip = $this->input->ip_address();
            if(password_verify($this->input->post('password',true),$user->password))
            {
            	$this->session->set_userdata('auth',$user->id);
            	$this->Users->update_ip($user->id,$ip);
				return redirect(base_url('user/dashboard'));
            }
            if($user->password == md5($this->input->post('password',true)))
            {
            	$this->Users->update($user->id,array('password' => password_hash($this->input->post('password',true), PASSWORD_BCRYPT, ['cost' => 12])));
            	$this->Users->update_ip($user->id,$ip);
            	$this->session->set_userdata('auth',$user->id);
				return redirect(base_url('user/dashboard'));
            }
            if($this->session->userdata('invalid_attemp'))
            {
            	$this->session->set_userdata('invalid_attemp',$this->session->userdata('invalid_attemp') + 1);
            } else {
            	$this->session->set_userdata('invalid_attemp',1);
            }
            $this->session->set_flashdata('messages', "<div class='alert alert-danger'> Invalid Password </div>");
            return redirect(base_url('auth/signin'));
		}
		if($this->input->method(TRUE) == 'GET') return $this->load->view('user/signin');
	}
	public function signup()
	{
		if($this->session->auth) return redirect('user/dashboard');
		if($this->input->method(TRUE) == 'POST')
		{
			if($this->Site->register == 0)
			{
				echo "Currently Register is Closed";
				exit;
			}
            if ($this->form_validation->run('auth/signup') == FALSE)
            {
                $this->session->set_flashdata('messages', "<div class='alert alert-danger'>".validation_errors()."</div>");
                return redirect(base_url('auth/signup'));
            }
             if($this->Site->recaptcha_public != '' && $this->Site->recaptcha_secret != '')
            {
            	$response = $this->input->post('g-recaptcha-response');
            	if($response == null || $response == "")
            	{
            		$this->session->set_flashdata('messages', "<div class='alert alert-danger'>Invalid Captcha.</div>");
           		    return redirect(base_url('auth/signup'));
            	}
            	$captcha =  $this->curl->get('https://www.google.com/recaptcha/api/siteverify?secret='.$this->Site->recaptcha_secret.'&response='.$response);

            	if($captcha->success !== true)
            	{
            		$this->session->set_flashdata('messages', "<div class='alert alert-danger'>Invalid Captcha</div>");
           		    return redirect(base_url('auth/signup'));
            	}

            }
           
        	$this->Users->insert(array(
        		'email' => $this->input->post('email',true),
        		'username' => $this->input->post('username',true),
        		'password' => password_hash($this->input->post('password',true), PASSWORD_BCRYPT, ['cost' => 12]),
        		'balance' => 0,
        		'balance_used' => 0,
        		'level' => 1,
        		'via' => 'Signup Page',
        		'token' => sha1(md5(time)),
        	));
        	$this->session->set_flashdata('messages', "<div class='alert alert-success'> Signup successful , please signin </div>");
        	return redirect(base_url('auth/signin'));
		}
		if($this->input->method(TRUE) == 'GET') return $this->load->view('user/signup');
	}
	public function signout()
	{
		session_destroy();
		return redirect(base_url('auth/signin'));
	}
	public function services()
	{
		if($this->input->method(TRUE) == 'GET') return $this->load->view('user/services');
	}
	public function api_doc()
	{
		if($this->input->method(TRUE) == 'GET') return $this->load->view('user/api_doc');
	}
<<<<<<< HEAD
	public function page()
	{
		$page = $this->db->get_where('pages',['id' => $this->input->get('id')]);
		if($page->num_rows() != 1) return $this->load->view('errors/html/error_404',array('heading' => '404 Page Not Found','message' => '<p>The page you requested was not found.</p>'));
		if($this->input->method(TRUE) == 'GET') return $this->load->view('user/page',$page->result()[0]);
=======
	public function update()
	{
		// created_at and updated_at
		// foreach($this->db->query("show tables")->result() as $v)
		// {
		// 	foreach ($v as $key => $value) {
		// 		$column = $this->db->query("SHOW COLUMNS FROM ".$value)->result();
		// 		$sql = "ALTER TABLE {$value} ADD `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER ".$column[count($column)-1]->Field.", ADD updated_at TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER created_at;";
		// 		$this->db->query($sql);

		// 	}
		// }
		// replace table namec
		// $tables = array(
		// 	'orderan' => 'orders',
		// 	'user' => 'users',
		// 	'category' => 'categories',
		// 	'service' => 'services',
		// 	'api' => 'apis',
		// 	'balance_history' => 'balances',
		// 	'page' => 'pages',,
		// 	'ticket' => 'tickets',
		// 	'ticket_open' => 'ticket_message',
		// 	'topup_via' => 'topup_method',
		// 	'balance_history' => 'balances',
		// 	'request_topup' => 'topup_requests',
		// );
		// foreach ($tables as $key => $value) {
		// 	var_dump($this->db->query(" 
		// 			 RENAME TABLE $key TO $value ;
					
		// 		"));
		// 	// $this->db->query("RENAME TABLE $key TO $value");
		// }
>>>>>>> 5f22bf57b58558ce378746383a0aa20003cfcef1
	}
}