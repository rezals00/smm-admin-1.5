<?php 
class User extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		if(!$this->session->auth){
			if(file_exists('.dashboard.html'))
			{
				return redirect('landing');
			}
			return redirect('auth/signin');
		}
		
	}
	public function index()
	{
		if($this->input->method(TRUE) == 'GET') return $this->load->view('user/dashboard');
	}
	public function motm()
	{
		if($this->input->method(TRUE) == 'GET') return $this->load->view('user/motm');
	}
	public function profile()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			try {
				if ($this->form_validation->run('user/profile') == FALSE){
					throw new Exception(validation_errors(), 1);
					
				}
				$old = $this->input->post('passwordold');
				$new = $this->input->post('passwordnew');

				if(!password_verify($old,$this->Users->password)) throw new Exception("Invalid Old Password.", 1);
				$this->db->where('id',$this->Users->id)->update('users',['password' => password_hash($new, PASSWORD_BCRYPT, ['cost' => 12])]);
				$this->session->set_flashdata('messages', "<div class='alert alert-success'>Saved.</div>");
				return redirect(base_url('user/profile'));
			} catch (Exception $e) {
				$this->session->set_flashdata('messages', "<div class='alert alert-danger'>".$e->getMessage()."</div>");
				return redirect(base_url('user/profile'));
			}
		}
		if($this->input->method(TRUE) == 'GET') return $this->load->view('user/profile');
	}
	public function generateapikey()
	{
		try {
			$this->load->helper('string');
			$this->db->where('id',$this->Users->id)->update('users',
				[
					'token' => hash('sha512', time() + rand(1,256))]);
			return redirect(base_url('user/profile'));
		} catch (Exception $e) {
			
		}
	}
	public function verify()
	{
		if($this->Users->status == '2')
		{
			redirect(base_url('user'));
		}
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('user/verify') == FALSE){
				$this->session->set_flashdata('messages', "<div class='alert alert-danger'>".validation_errors()."</div>");
				return redirect(base_url('user/verify'));
			}
			$code = $this->input->post('code');
			$real = $this->session->userdata('code');
			if($code != $real)
			{
				$this->session->set_flashdata('messages', "<div class='alert alert-danger'>Invalid Activation Code</div>");
				return redirect(base_url('user/verify'));
			} 
			$this->Users->update($this->Users->id,array('status' => 2));
			return redirect('user');

		}
		if($this->input->method(TRUE) == 'GET') return $this->load->view('user/verify');
	}
	public function order()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('user/order') == FALSE){
				$this->session->set_flashdata('messages', "<div class='alert alert-danger'>".validation_errors()."</div>");
				return redirect(base_url('user/order'));
			}
			$post =$this->input->post();
			$quantity = (int) $post['quantity'];
			$target = (string) $post['target'];
			$service = (string) $post['service'];
			$order = $this->Orders->createOrder($service,$target,$quantity);
			
			if($order['error'] == false)
			{
				$this->session->set_flashdata('messages', "<div class='alert alert-success'> Order Successful Placed . <br/>
					#ID : ".$order['order']." <br/> 
					Service : ".$order['service']." <br>
					Target : {$target} <br/>
					Quantity : {$quantity} <br/>
					</div>");
				return redirect(base_url('user/order'));
			}
			$this->session->set_flashdata('messages', "<div class='alert alert-danger'>".$order['error']."</div>");
			return redirect(base_url('user/order'));

		}
		if($this->input->method(TRUE) == 'GET') return $this->load->view('user/order');
	}
	public function multiorder()
	{
		if($this->input->method(TRUE) == 'POST')
		{
	
			if ($this->form_validation->run('user/order') == FALSE){
				$this->session->set_flashdata('messages', "<div class='alert alert-danger'>".validation_errors()."</div>");
				return redirect(base_url('user/order'));
			}
			$post =$this->input->post();
			$quantity = (int) $post['quantity'];
			$targets = (string) $post['target'];
			$service = (string) $post['service'];
			$target = explode("\n",$targets);
			$message = "";
            for($i=0;$i<count($target);$i++){
				$order = $this->Orders->createOrder($service,$target[$i],$quantity);
				if($order['error'] === false)
				{
					$message .= "<div class='alert alert-success'> Order Successful Placed . <br/>
						#ID : ".$order['order']." <br/> 
						Service : ".$order['service']." <br>
						Target : ".$target[$i]." <br/>
						Quantity : {$quantity} <br/>
						</div>";
				} else 
				{
					$message .= "<div class='alert alert-danger'>".$order['error']."</div>";
				}
			}
			$this->session->set_flashdata('messages', $message);
		    return redirect(base_url('user/multiorder'));

		}
		if($this->input->method(TRUE) == 'GET') return $this->load->view('user/multiorder');
	}
	public function orders()
	{
		if($this->input->method(TRUE) == 'GET') return $this->load->view('user/orders');
	}
	public function addfunds_open()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			$bid = (int) $this->uri->uri_to_assoc(2)['addfunds'];
			$info = (string) $this->input->post('info');
			$action=  (string) $this->input->post('action');
			$topup = $this->Topup->submit($bid,$info,$action);
			if($topup['error'] !== false)
			{
				$this->session->set_flashdata('messages', "<div class='alert alert-danger'>".$topup['error']."</div>");
			}
			return redirect(base_url('user/addfunds/'.$bid));
		}
		if($this->input->method(TRUE) == 'GET')
		{
			$bid = $this->uri->uri_to_assoc(2)['addfunds'];
			$e = $this->db->select('topup_method.name,topup_method.type,topup_method.info,topup_requests.amount,topup_requests.info,topup_requests.status,topup_method.rate')
			->from('topup_requests')
			->join('topup_method','topup_method.id = topup_requests.via')
			->where('topup_requests.id',$bid)->get();
			if($e->num_rows() != 1)
			{
				return redirect(base_url('user/addfunds'));
			}
			$this->load->view('user/addfund_open',$e->result()[0]);
		}
	}
	public function addfunds()
	{
		$this->Topup->check();
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('user/addfunds') == FALSE){
				$this->session->set_flashdata('messages', "<div class='alert alert-danger'>".validation_errors()."</div>");
				return redirect(base_url('user/addfunds'));
			}
			$post =$this->input->post();
			$method = (int) $post['method'];
			$amount = (int) $post['amount'];
			$add = $this->Topup->start($method,$amount);
			
			if($add['error'] == false)
			{
				return redirect(base_url('user/addfunds/'.$add['id']));
			}
			$this->session->set_flashdata('messages', "<div class='alert alert-danger'>".$add['error']."</div>");
			return redirect(base_url('user/addfunds'));

		}
		if($this->input->method(TRUE) == 'GET') return $this->load->view('user/addfunds');
	}
	public function balance_manual()
	{
		if($this->input->method(TRUE) == 'GET') return $this->load->view('user/balance_manual');
	}
	public function balance_auto()
	{
		if($this->input->method(TRUE) == 'GET') return $this->load->view('user/balance_auto');
	}
	public function balances()
	{
		if($this->input->method(TRUE) == 'GET') return $this->load->view('user/balances');
	}
	public function send_balance()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('user/send_balance') == FALSE){
				$this->session->set_flashdata('messages', "<div class='alert alert-danger'>".validation_errors()."</div>");
				return redirect(base_url('user/send_balance'));
			}
			$username = (string) $this->input->post('username',true);
			$amount = (int) $this->input->post('balance',true);

			if($this->Users->balance <  $amount)
			{
				$this->session->set_flashdata('messages', "<div class='alert alert-danger'>Not enough balance</div>");
				return redirect(base_url('user/send_balance'));
			}
			if($amount < 1)
			{
				$this->session->set_flashdata('messages', "<div class='alert alert-danger'>Minimum Transfer : 1</div>");
				return redirect(base_url('user/send_balance'));
			}
			$send = $this->Users->send_balance($username,$amount);
			if($send['error'] === false)
			{
				$this->session->set_flashdata('messages', "<div class='alert alert-success'>Success , Transfer Rp.".number_format($amount)." To {$username}</div>");
				return redirect(base_url('user/send_balance'));
			} else {
				$this->session->set_flashdata('messages', "<div class='alert alert-danger'>".$send['error']."</div>");
				return redirect(base_url('user/send_balance'));
			}
			
		}
		if($this->input->method(TRUE) == 'GET') return $this->load->view('user/send_balance');
	}
	public function create_user()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('user/create_user') == FALSE){
				$this->session->set_flashdata('messages', "<div class='alert alert-danger'>".validation_errors()."</div>");
				return redirect(base_url('user/create_user'));
			}
			$username = (string) $this->input->post('username',true);
			$password = (string) $this->input->post('password',true);
			$balance = (int) $this->input->post('balance',true);
			$level = (int) $this->input->post('level',true);

			if($this->Users->balance <  $balance)
			{
				$this->session->set_flashdata('messages', "<div class='alert alert-danger'>Not enough balance</div>");
				return redirect(base_url('user/create_user'));
			}
			$create = $this->Users->create_user($username,$password,$balance,$level);
			if($create['error'] === false)
			{
				$this->session->set_flashdata('messages', "<div class='alert alert-success'>Success <br/> Username : {$username} <br/> Password : {$password} <br/>
					Balance  : {$balance} <br/>
					Level    : {$level} <br/>
					</div>");
				return redirect(base_url('user/create_user'));
			} else {
				$this->session->set_flashdata('messages', "<div class='alert alert-danger'>".$create['error']."</div>");
				return redirect(base_url('user/create_user'));
			}
			
		}
		if($this->input->method(TRUE) == 'GET') return $this->load->view('user/create_user');
	}
	public function ticket()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('user/ticket/new') == FALSE){
				$this->session->set_flashdata('messages', "<div class='alert alert-danger'>".validation_errors()."</div>");
				return redirect(base_url('user/ticket'));
			}

			$this->db->insert('tickets',array(
				'uid' => $this->Users->id,
				'subject' => strip_tags($this->input->post('subject')),
				'status' => 0
			));
			$id = $this->db->insert_id();
			$this->db->insert('ticket_message',array(
				'uid' => $this->Users->id,
				'tid' => $id,
				'msg' => strip_tags($this->input->post('message')),
				'position' => '0'
			));
			return redirect(base_url('user/ticket/'.$id));

		}
		if($this->input->method(TRUE) == 'GET') return $this->load->view('user/ticket');
	}
	public function ticketopen()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			$tid = $this->uri->uri_to_assoc(2)['ticket'];
			if($tid)
			{
				if ($this->form_validation->run('user/ticket/reply') == FALSE){
					$this->session->set_flashdata('messages', "<div class='alert alert-danger'>".validation_errors()."</div>");
					return redirect(base_url('user/ticket/'.$tid));
				}
				$reply = $this->Tickets->reply($tid,strip_tags($this->input->post('message')),0);
				if($reply['error'] !== false)
				{
					$this->session->set_flashdata('messages', "<div class='alert alert-danger'>".$reply['error']."</div>");
				}
			}
			return redirect(base_url('user/ticket/'.$tid));
		}
		if($this->input->method(TRUE) == 'GET')
		{
			$tid = $this->uri->uri_to_assoc(2)['ticket'];
			if($tid)
			{
				$ticket = $this->Tickets->find(array('id' => $tid,'user_id' => $this->Users->id));
				if($ticket['count'] == 1)
				{
					return $this->load->view('user/ticket_open',$ticket['data'][0]);
					
				}
			}
			return redirect(base_url('user/ticket'));
		}

	}
	public function sendbalance()
	{
		if($this->input->method(TRUE) == 'GET') return $this->load->view('user/sendbalance');
	}
	public function register()
	{
		if($this->input->method(TRUE) == 'GET') return $this->load->view('user/reg');
	}
	public function test()
	{
		$order = array();
		$order['form'] = array(
			'url' => '',
			'form' => array(
				'key' => '[key]',
				'service' => '[service]',
				'data' => '[target]',
				'quantity' => '[quantity]',
				'action' => 'add'
			)
			
		);
		$order['result']  = array(
			'orderid' => 'orderid',
			'msg' => 'msg'
		);

		$status = array();
		$status['form'] = array(
			'url' => 'o',
			'form' => array(
				'key' => '[key]',
				'order' => '[orderid]',
				'action' => 'status'
			)
		);
		$status['result'] = array(
			'status' => 'data.status',
			'start_count' => 'data.start_count',
			'remains' => 'data.remains',
			'statusTo' => [
				'Pending' => '0',
				'Pending' => '1',
				'Processing' => '2',
				'Processing' => '3',
				'Completed' => '4',
				'Partial' => '5',
				'Canceled' => '6'
			]
		);
		$service = array();
		$service['form'] = array(
			'url' => '',
			'form' => array(
				'key' => '[key]',
				'action' => 'services',
			)
		);
		$service['result'] = array(
			'data' => 'data',
			'service' => [
				'id' => 'id',
				'name' => 'layanan',
				'price' =>  'proceperk',
				'min' => 'min',
				'max' => 'max'
			]
		);
		echo json_encode($service);
	}
}