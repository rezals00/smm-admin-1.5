<?php 
class AjaxUser extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		if(!$this->session->auth)
			redirect(base_url().'auth/signin');
	}
	private function percent($num,$total){
        if($num == 0){
            return 0;
        } 
        return floor(($num / $total) * 100);
    }
	public function checkservice()
	{
        if($this->input->method(TRUE) == 'GET')
        { 
            $e = $this->input->get('id',true);
            $w = false;
            foreach($this->db->get_where('services',array('id' => $e,'status' => 1))->result() as $w);
            // print_r($w);
            if($w){
                $total = $this->Orders->find(array('service_id' => $e))['count'];
                $pending = $this->Orders->find(array('service_id' => $e,'status' => 'Pending'))['count'];
                $completed = $this->Orders->find(array('service_id' => $e,'status' => 'Completed'))['count'];
                $canceled = $this->Orders->find(array('service_id' => $e,'status' => 'Canceled'))['count'];
                $partial = $this->Orders->find(array('service_id' => $e,'status' => 'Partial'))['count'];
                $procces = $this->Orders->find(array('service_id' => $e,'status' => 'Processing'))['count'];
                echo json_encode(array(
                    'pending' => $this->percent($pending,$total),
                    'completed' =>  $this->percent($completed,$total),
                    'canceled' =>  $this->percent($canceled,$total),
                    'partial' =>  $this->percent($partial,$total),
                    'proccess' =>  $this->percent($procces,$total),
                ));
            }
        }
	}
    public function sendemail()
    {
        if($this->input->method(TRUE) == 'POST')
        {
            if ($this->form_validation->run('ajax/sendemail') == FALSE){
                echo json_encode(array('error' => validation_errors()));
                return null;
            }
            $email = $this->input->post('email');
            $ex = explode('@',$email);
            $domain = array(
                'yahoo.com',
                'gmail.com',
            );
            if(!in_array($ex[1], $domain))
            {
                echo json_encode(array('error' => 'email not supported , please use @gmail.com or @yahoo.com'));
                return null;
            }
            $code = rand(1111,9999);
            if($this->session->userdata('code'))
            {
                $code = $this->session->userdata('code');
            }
            if($this->session->userdata('send'))
            {
                if($this->session->userdata('send') > time())
                {
                    $until = intval($this->session->userdata('send')) - time();

                    echo json_encode(array('error' => 'you are trying to fast,try again after '.$until.' second'));
                    return null;
                }
            }
            $this->session->set_userdata('send',time() + 60);
            $this->session->set_userdata('code',$code);
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://'.$this->Site->smtp_host,
                'smtp_port' => 465,
                'smtp_user' => $this->Site->smtp_username,
                'smtp_pass' => $this->Site->smtp_password,
                'mailtype'  => 'text', 
                'charset'   => 'iso-8859-1',
                'smtp_timeout' => 60,
            );
            $this->load->library('email',$config);
            $body = "<!--- BEGIN SHA 512 ---> \n".hash('sha512', time())."\n";
            $body .= "\n \n \nActivation Code : {$code} \n \n \n";
            $body .= "<!-- END SHA 512 --> \n".hash('sha512',time()+$code);
            $this->email->from($this->Site->smtp_username, 'Support');
            $this->email->to($email);
            $this->email->subject('Activation Code');
            $this->email->message($body);

            $send = $this->email->send();
            if(!$send)
            {
                echo json_encode(array('error' => 'ooops error,try again or contact admin'));
                exit;
            }
            $this->Users->update($this->Users->id,array('email' => $email));
            echo json_encode(array('error' => false));

        }
    }
}