<?php 
class Api extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function order()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('api/order') == FALSE){
				echo json_encode(array('error' => validation_errors()));
				exit;
			}
			$target = (string) $this->input->post('target');
			$quantity = (string) $this->input->post('quantity');
			$service = (int) $this->input->post('service');
			$key = (string) $this->input->post('key');
			echo json_encode($this->Orders->createOrder($service,$target,$quantity,$key));
		} else {
			echo json_encode(array('error' => 'Only POST Method Allowed,see documentation'));
			return null;
		}
	}
	public function status()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('api/status') == FALSE){
				echo json_encode(array('error' => validation_errors()));
				exit;
			}
			$order = (int) $this->input->post('order');
			$key = (string) $this->input->post('key');
			echo json_encode($this->Orders->status($order,$key));
		} else {
			echo json_encode(array('error' => 'Only POST Method Allowed,see documentation'));
			return null;
		}
	}
	public function balance()
	{
		
	}
}