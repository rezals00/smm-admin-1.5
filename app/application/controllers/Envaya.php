<?php 
class Envaya extends CI_Controller
{
	private $password = "1234";
	public function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$check = false;
		if($this->input->get('password') !== null)
		{
			if($this->input->get('password') != $this->password){
				echo "OK";
				exit;
			} else {
				$check = true;
			}
		}
		if(!$check) exit;
		if($this->input->post('action') == "incoming"){
			if($this->input->post('message_type') == "sms" && $this->input->post('from') == '858' && strpos($this->input->post('message'),"penambahan"))
			{
				$from = trim(between($this->input->post('message'), "dari nomor 62", " tgl"));
				$amount = trim(between($this->input->post('message'), "penambahan pulsa Rp ", " dari nomor "));
				echo "From : {$from} <br/> Amount : {$amount}";
				$find = $this->db->select('topup_requests.id,topup_method.username,topup_requests.amount,topup_requests.user_id')->from('topup_requests')->join('topup_method','topup_method.id = topup_requests.via')
				->where('topup_requests.info' ,$from)
				->where('topup_requests.amount' , $amount)
				->where('topup_method.username',$this->input->post('phone_number'))->get();
				if($find->num_rows() == 1)
				{
					$res = $find->result()[0];
					echo "Added";
					$this->Topup->admin($res->id,'2');
					$this->db->insert('sms',
						array('message_from' => $this->input->post('from'),'message_body' => $this->input->post('message'),'autotopup' => $res->id,'timestamp' => $this->input->post('timestamp')));
				} else {
					$this->db->insert('sms',array('sms',array('message_from' => $this->input->post('from'),'message_body' => $this->input->post('message'),'autotopup' => '0','timestamp' => $this->input->post('timestamp'))));
				}

			} else {
				echo "NOTHING";
			}
		}
	}
}