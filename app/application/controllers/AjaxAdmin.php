<?php 
class AjaxAdmin extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		if(!$this->session->auth)
		{
			return redirect(base_url().'auth/signin');
			exit;
		}
		if($this->Users->level != '0')
		{
			return redirect(base_url().'user');
			exit;
		}
		
	}
								/* User */ 
	/* ================================================================ */
	public function infouser()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('ajaxadmin/info') == FALSE){
				echo json_encode(array('error' => validation_errors()));
				exit;
			}
			$ID = (int) $this->input->post('ID');
			$user =$this->Users->find(array('id' => $ID));
			$order = $this->Orders->find(array('user_id' => $ID));
			if($user['count'] == 1 && $order)
			{
				$user['data'][0]->orders = $order['count'];
				echo json_encode(xss_array((array) $user['data'][0]));
				exit;
			}
			
		}
	}
	public function listuser()
	{
		if($this->input->method(TRUE) == 'GET')
		{ 
				 // Datatables Variables
	          $draw = intval($this->input->get("draw"));
	          $start = intval($this->input->get("start"));
	          $length = intval($this->input->get("length"));
	          $search = (array) $this->input->get('search');
	          $order = (array) $this->input->get('order');
	          if(count($order) == 0 || count($search) == 0) return null;
	          switch ($order[0]['column']) {
	          	case '1':
	          		$col = 'username';
	          		break;
	          	case '2':
	          		$col = 'level';
	          		break;
	          	case '3':
	          		$col = 'balance';
	          		break;
	          	case '4':
	          		$col = 'balance_used';
	          		break;
	          	case '6':
	          		$col = 'created_at';
	          		break;
	          	case '7':
	          		$col = 'updated_at';
	          		break;
	          	
	          	default:
	          		$col = 'id';
	          		break;
	          }
	          switch ($search['value']) {
	          		case 'Admin':
	          			$level = '0';
	          			break;
	          		case 'Reseller':
	          			$level = 'Reseller';
	          			break;
	          		
	          		default:
	          			$level = 'Member';
	          			break;
	          }
	          $this->db
	          ->select('*')
	          ->from('users')
	          ->group_start()
           	  ->like('username',$search['value'])
              ->or_like('balance',$search['value'])
              ->or_like('id',$search['value'])
              ->or_like('created_at',$search['value'])
              ->or_like('updated_at',$search['value'])
              ->or_like('level',$level)
              ->group_end()
           	  ->order_by($col,$order[0]['dir'])
           	  ->limit($length,$start);
	          $users = $this->db->get();

	          $data = array();

	          foreach($users->result() as $r) {
	          	switch ($r->level) {
	          		case '0':
	          			$level = 'Admin';
	          			break;
	          		case '2':
	          			$level = 'Reseller';
	          			break;
	          		
	          		default:
	          			$level = 'Member';
	          			break;
	          	}
	          	$orders = $this->Orders->find(array('user_id' => $r->id));
	               $data[] = array(
	                    xss_clean($r->id),
	                    xss_clean($r->username),
	                    xss_clean($level),
	                    xss_clean($r->balance),
	                    xss_clean($r->balance_used),
	                    xss_clean($orders['count']),
	                    xss_clean($r->created_at),
	                    xss_clean($r->updated_at)
	               );
	          }
	          if($order[0]['column'] == '5')
	          {
	          	function desc($a,$b){
			        return($a[5] <= $b[5]) ? 1 : -1;
			    }
			    function asc($a,$b){
			        return($a[5] <= $b[5]) ? -1 : 1;
			    }
			    usort($data, $order[0]['dir']);
	          }
	          $output = array(
	               "draw" => $draw,
	                 "recordsTotal" => $this->db->get_where('users')->num_rows(),
	                 "recordsFiltered" => $this->db->get_where('users')->num_rows(),
	                 "data" => $data
	            );
	          echo json_encode($output);
	          exit;
        }
            		
	}
	public function deleteuser()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('ajaxadmin/info') == FALSE){
				echo json_encode(array('error' => validation_errors()));
				exit;
			}
			$ID = (int) $this->input->post('ID');
			$this->db->delete('users',array('id' => $ID));
			echo json_encode(array('error' => false));
		}	          
	}
	public function updateuser()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('ajaxadmin/updateuser') == FALSE){
				echo json_encode(array('error' => validation_errors()));
				exit;
			}
			$ID = (int) $this->input->post('ID');
			$balance = (int) $this->input->post('balance');
			$level = (int) $this->input->post('level');
			$password = (string) $this->input->post('password');
			$user =$this->Users->find(array('id' => $ID));
			if($user['count'] == 1)
			{
				$update = array(
					'level' => $level,
					'balance' => $balance,
				);
				if($password != $user['data'][0]->password)
	            {
	            	$update['password'] = password_hash($this->input->post('password',true), PASSWORD_BCRYPT, ['cost' => 12]);
	            }
	            $this->Users->update($ID,$update);
	            echo json_encode(array('error' => false));
			} else {
				echo json_encode(array('error' => 'User Not Found'));
			}
			
		}
	}
									/* Categories */ 
	/* ================================================================ */
	public function addcategory()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('ajaxadmin/addcategory') == FALSE){
				echo json_encode(array('error' => validation_errors()));
				exit;
			}
			$name = (string) $this->input->post('name');
			$status = (int) $this->input->post('status');
			$ins = $this->Categories->insert(array('name' => $name,'status' => $status));
			echo json_encode(array('error' => false));
			
		}
	}
	public function infocategory()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('ajaxadmin/info') == FALSE){
				echo json_encode(array('error' => validation_errors()));
				exit;
			}
			$ID = (int) $this->input->post('ID');
			$category = (array) $this->Categories->find(array('id' => $ID));
			$service = (array) $this->Services->find(array('category_id' => $ID));
			if($category['count'] == 1 && $service)
			{
				$category['data'][0]->services = (int) $category['count'];
				echo json_encode(xss_array((array) $category['data'][0]));
				exit;
			}
			
		}
	}
	public function listcategories()
	{
		if($this->input->method(TRUE) == 'GET')
		{ 
				 // Datatables Variables
	          $draw = intval($this->input->get("draw"));
	          $start = intval($this->input->get("start"));
	          $length = intval($this->input->get("length"));
	          $search = (array) $this->input->get('search');
	          $order = (array) $this->input->get('order');
	          if(count($order) == 0 || count($search) == 0) return null;
	          switch ($order[0]['column']) {
	          	case '1':
	          		$col = 'name';
	          		break;
	          	case '3':
	          		$col = 'status';
	          		break;
	          	case '4':
	          		$col = 'created_at';
	          		break;
	          	case '5':
	          		$col = 'updated_at';
	          		break;
	          	
	          	default:
	          		$col = 'id';
	          		break;
	          }
	          switch ($search['value']) {
	          		case 'Admin':
	          			$level = '0';
	          			break;
	          		case 'Reseller':
	          			$level = 'Reseller';
	          			break;
	          		
	          		default:
	          			$level = 'Member';
	          			break;
	          }
	          $this->db
	          ->select('*')
	          ->from('categories')
	          ->group_start()
           	  ->like('name',$search['value'])
              ->group_end()
           	  ->order_by($col,$order[0]['dir'])
           	  ->limit($length,$start);
	          $users = $this->db->get();

	          $data = array();

	          foreach($users->result() as $r) {
	          	switch ($r->status) {
	          		case '0':
	          			$status = 'Disable';
	          			break;
	          		
	          		default:
	          			$status = 'Enable';
	          			break;
	          	}
	          	$services = $this->Services->find(array('category_id' => $r->id));
	               $data[] = array(
	                    xss_clean($r->id),
	                    xss_clean($r->name),
	                    xss_clean($services['count']),
	                    xss_clean($status),
	                    xss_clean($r->created_at),
	                    xss_clean($r->updated_at)
	               );
	          }
	          if($order[0]['column'] == '2')
	          {
	          	function desc($a,$b){
			        return($a[2] <= $b[2]) ? 1 : -1;
			    }
			    function asc($a,$b){
			        return($a[2] <= $b[2]) ? -1 : 1;
			    }
			    usort($data, $order[0]['dir']);
	          }
	          $output = array(
	               "draw" => $draw,
	                 "recordsTotal" => $this->db->get_where('categories')->num_rows(),
	                 "recordsFiltered" => $this->db->get_where('categories')->num_rows(),
	                 "data" => $data
	            );
	          echo json_encode($output);
	          exit;
        }
            		
	}
	public function updatecategory()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('ajaxadmin/updatecategory') == FALSE){
				echo json_encode(array('error' => validation_errors()));
				exit;
			}
			$ID = (int) $this->input->post('ID');
			$name = (string) $this->input->post('name');
			$status = (int) $this->input->post('status');
			$category =$this->Categories->find(array('id' => $ID));
			if($category['count'] == 1)
			{
				$update = array(
					'name' => $name,
					'status' => $status,
				);
	            $this->Categories->update($ID,$update);
	            echo json_encode(array('error' => false));
			} else {
				echo json_encode(array('error' => 'Category Not Found'));
			}
			
		}
	}
	public function deletecategory()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('ajaxadmin/info') == FALSE){
				echo json_encode(array('error' => validation_errors()));
				exit;
			}
			$ID = (int) $this->input->post('ID');
			$this->db->delete('categories',array('id' => $ID));
			echo json_encode(array('error' => false));
		}	          
	}
	public function infonews()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('ajaxadmin/info') == FALSE){
				echo json_encode(array('error' => validation_errors()));
				exit;
			}
			$ID = (int) $this->input->post('ID');
			$news = (array) $this->News->find(array('id' => $ID));
			if($news['count'] == 1)
			{
				echo json_encode(xss_array((array) $news['data'][0]));
				exit;
			}
			
		}
	}
	public function updatenews()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('ajaxadmin/updatenews') == FALSE){
				echo json_encode(array('error' => validation_errors()));
				exit;
			}
			$ID = (int) $this->input->post('ID');
			$section = (string) $this->input->post('section');
			$message = (string) $this->input->post('message');
			$category =$this->News->find(array('id' => $ID));
			if($category['count'] == 1)
			{
				$update = array(
					'section' => $section,
					'message' => $message,
				);
	            $this->News->update($ID,$update);
	            echo json_encode(array('error' => false));
			} else {
				echo json_encode(array('error' => 'News Not Found'));
			}
			
		}
	}
	public function listnews()
	{
		if($this->input->method(TRUE) == 'GET')
		{ 
				 // Datatables Variables
	          $draw = intval($this->input->get("draw"));
	          $start = intval($this->input->get("start"));
	          $length = intval($this->input->get("length"));
	          $search = (array) $this->input->get('search');
	          $order = (array) $this->input->get('order');
	          if(count($order) == 0 || count($search) == 0) return null;
	          switch ($order[0]['column']) {
	          	case '1':
	          		$col = 'section';
	          		break;
	          	case '2':
	          		$col = 'message';
	          		break;
	          	case '3':
	          		$col = 'created_at';
	          		break;
	          	case '4':
	          		$col = 'updated_at';
	          		break;
	         
	          	
	          	default:
	          		$col = 'id';
	          		break;
	          }
	          
	          $this->db
	          ->select('*')
	          ->from('news')
	          ->group_start()
           	  ->like('section',$search['value'])
              ->or_like('section',$search['value'])
              ->or_like('id',$search['value'])
              ->group_end()
           	  ->order_by($col,$order[0]['dir'])
           	  ->limit($length,$start);
	          $users = $this->db->get();

	          $data = array();

	          foreach($users->result() as $r) {
	          	
	               $data[] = array(
	                    xss_clean($r->id),
	                    xss_clean($r->section),
	                    xss_clean($r->message),
	                    xss_clean($r->created_at),
	                    xss_clean($r->updated_at)
	               );
	          }
	         
	          $output = array(
	               "draw" => $draw,
	                 "recordsTotal" => $this->db->get_where('news')->num_rows(),
	                 "recordsFiltered" =>$this->db->get_where('news')->num_rows(),
	                 "data" => $data
	            );
	          echo json_encode($output);
	          exit;
        }
            		
	}
	public function addnews()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('ajaxadmin/addnews') == FALSE){
				echo json_encode(array('error' => validation_errors()));
				exit;
			}
			$section = (string) $this->input->post('section');
			$message = (string) $this->input->post('message');
			$ins = $this->News->insert(array('section' => $section,'message' => $message));
			echo json_encode(array('error' => false));
			
		}
	}
	public function deletenews()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('ajaxadmin/info') == FALSE){
				echo json_encode(array('error' => validation_errors()));
				exit;
			}
			$ID = (int) $this->input->post('ID');
			$this->db->delete('news',array('id' => $ID));
			echo json_encode(array('error' => false));
		}	          
	}





	public function deleteservice()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('ajaxadmin/info') == FALSE){
				echo json_encode(array('error' => validation_errors()));
				exit;
			}
			$ID = (int) $this->input->post('ID');
			$this->db->delete('services',array('id' => $ID));
			echo json_encode(array('error' => false));
		}	
	}
	public function addservice()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('ajaxadmin/addservice') == FALSE){
				echo json_encode(array('error' => validation_errors()));
				exit;
			}
			$name = (string) $this->input->post('name');
			$category = (int) $this->input->post('category');
			$min = (int) $this->input->post('min');
			$max = (int) $this->input->post('max');
			$price = (string) $this->input->post('price');
			$profit = (string) $this->input->post('profit');
			$desc = (string) $this->input->post('description');
			$api = (int) $this->input->post('api');
			$mode = (string) $this->input->post('mode');
			$provider_service_id = (int) $this->input->post('serviceid');

			$a = $this->Apis->find(array('id' => $api));
			if($a['count'] != 1 && $api != 0)
			{
				echo json_encode(array('error' => 'API Not Found'));
				exit;
			}
			$cat = $this->Categories->find(array('id' => $category));
			if($cat['count'] != 1)
			{
				echo json_encode(array('error' => 'Category Not Found'));
				exit;
			}


			$ins = $this->Services->insert(array(
				'api_id' => $api,
				'category_id' => $category,
				'pro_id' => $provider_service_id,
				'name' => $name,
				'info' => $desc,
				'price' => $price,
				'profit' => $profit,
				'min' => $min,
				'max' => $max,
				'type' => 'Default',
				'status' => 1,
				'mode' => $mode
			));
			echo json_encode(array('error' => false));
			
		}
	}
	public function infoservice()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('ajaxadmin/info') == FALSE){
				echo json_encode(array('error' => validation_errors()));
				exit;
			}
			$ID = (int) $this->input->post('ID');
			$service = (array) $this->Services->find(array('id' => $ID));
			if($service['count'] == 1)
			{
				$service['data'][0]->api = "Manual";
				if($service['data'][0]->api_id != '0')
				{
					$api = (array) $this->Apis->find(array('id' => $service['data'][0]->api_id));
					if($api['count'] == 1)
					{
						$service['data'][0]->api = $api['data'][0]->name;
					}
				}
				$cat = (array) $this->Categories->find(array('id' => $service['data'][0]->category_id));
				if($cat['count'] == 1)
				{
					$service['data'][0]->category = $cat['data'][0]->name;
				}
				echo json_encode(xss_array((array) $service['data'][0]));
				exit;
			}
			
		}
	}

	public function listservices()
	{
		if($this->input->method(TRUE) == 'GET')
		{ 
				 // Datatables Variables
	          $draw = intval($this->input->get("draw"));
	          $start = intval($this->input->get("start"));
	          $length = intval($this->input->get("length"));
	          $search = (array) $this->input->get('search');
	          $order = (array) $this->input->get('order');
	          if(count($order) == 0 || count($search) == 0) return null;

	          $col = "services.id";
	          switch ($order[0]['column']) {
	          	case '1':
	          		$col = 'services.api_id';
	          		break;
	          	case '2':
	          		$col = 'services.category_id';
	          		break;
	          	case '3':
	          		$col = 'services.name';
	          		break;
	          	case '5':
	          		$col = 'services.min';
	          		break;
	          	case '6':
	          		$col = 'services.price';
	          		break;
	          	case '7':
	          		$col = 'services.created_at';
	          		break;
	          	case '7':
	          		$col = 'services.updated_at';
	          		break;
	         
	          	
	          	default:
	          		$col = 'services.id';
	          		break;
	          }
	          
	          $this->db
	          ->select('services.name,services.price,services.min,services.max,categories.name as catname,services.id,services.api_id,services.created_at,services.updated_at,services.mode,services.status')
	          ->from('services')
	          ->join('categories','services.category_id = categories.id')
	          ->group_start()
           	  ->like('services.name',$search['value'])
              ->or_like('services.price',$search['value'])
              ->or_like('services.min',$search['value'])
              ->or_like('services.max',$search['value'])
              ->group_end()
           	  ->order_by($col,$order[0]['dir'])
           	  ->limit($length,$start);
	          $users = $this->db->get();

	          $data = array();

	          foreach($users->result() as $r) {
	          	   $name = "Manual";
	          	   if($r->api_id != '0')
	          	   {
	          	   		$api = $this->Apis->find(array('id' => $r->api_id));
	          	   		$name = "API ERROR";
	          	   		if($api['count'] == 1)
	          	   		{
	          	   			$name = $api['data'][0]->name;
	          	   		}
	          	   }
	          	   $o = $this->Orders->find(array('service_id' => $r->id));
	          	   switch ($r->status) {
	          	   	case '1':
	          	   		$status = "Enable";
	          	   		break;
	          	   	
	          	   	default:
	          	   		$status = "Disable";
	          	   		break;
	          	   }
	               $data[] = array(
	                    xss_clean($r->id),
	                    xss_clean($name." / ".$r->mode),
	                    $r->catname,
	                    xss_clean($r->name.' / '.$status),
	                    xss_clean($o['count']),
	                    xss_clean($r->min." / ".$r->max),
	                    xss_clean($r->price),
	                    xss_clean($r->created_at),
	                    xss_clean($r->updated_at)
	               );
	          }
	          if($order[0]['column'] == '4')
	          {
	          	function desc($a,$b){
			        return($a[4] <= $b[4]) ? 1 : -1;
			    }
			    function asc($a,$b){
			        return($a[4] <= $b[4]) ? -1 : 1;
			    }
			    usort($data, $order[0]['dir']);
	          }
	         
	          $output = array(
	               "draw" => $draw,
	                 "recordsTotal" => $this->db->get_where('services')->num_rows(),
	                 "recordsFiltered" => $this->db->get_where('services')->num_rows(),
	                 "data" => $data
	            );
	          echo json_encode($output);
	          exit;
        }
            		
	}
	public function updateservice()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('ajaxadmin/updateservice') == FALSE){
				echo json_encode(array('error' => validation_errors()));
				exit;
			}
			$ID = (string) $this->input->post('ID');
			$name = (string) $this->input->post('name');
			$category = (int) $this->input->post('category');
			$min = (int) $this->input->post('min');
			$max = (int) $this->input->post('max');
			$price = (string) $this->input->post('price');
			$profit = (string) $this->input->post('profit');
			$desc = (string) $this->input->post('description');
			$mode = (string) $this->input->post('mode');
			$api = (int) $this->input->post('api');
			$status = (int) $this->input->post('status');
			$provider_service_id = (int) $this->input->post('serviceid');
			$service = $this->Services->find(array('id' => $ID));
			if($service['count'] != 1)
			{
				echo json_encode(array('error' => 'Service Not Found'));
				exit;
			}
			$a = $this->Apis->find(array('id' => $api));
			if($a['count'] != 1 && $api != 0)
			{
				echo json_encode(array('error' => 'API Not Found'));
				exit;
			}
			$cat = $this->Categories->find(array('id' => $category));
			if($cat['count'] != 1)
			{
				echo json_encode(array('error' => 'Category Not Found'));
				exit;
			}


			$ins = $this->Services->update($ID,array(
				'api_id' => $api,
				'category_id' => $category,
				'pro_id' => $provider_service_id,
				'name' => $name,
				'info' => $desc,
				'price' => $price,
				'min' => $min,
				'max' => $max,
				'profit' => $profit,
				'mode' => $mode,
				'type' => 'Default',
				'status' => $status
			));
			echo json_encode(array('error' => false));
			
		}
	}
	/* ================================================================ */
	public function addapi()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('ajaxadmin/addapi') == FALSE){
				echo json_encode(array('error' => validation_errors()));
				exit;
			}
			$name = (string) $this->input->post('name');
			$url = (string) $this->input->post('url');
			$type = (string) $this->input->post('type');
			$api = (string) $this->input->post('api');
			$apis =$this->Apis->find(array('url' => $url,'api' => $api,'type' => $type));
			$builder =$this->db->get_where('api_builder',array('name' => $type));
			if($builder->num_rows() != 1)
			{
				echo json_encode(array('error' => 'API Type Not Found'));
				exit;

			}
			if($apis['count'] == 1)
			{
				echo json_encode(array('error' => 'API With Same Data was registered'));
				exit;

			}
			$ins = $this->Apis->insert(array('name' => $name,'url' => $url,'api' => $api,'type' => $type));
			echo json_encode(array('error' => false));
			
		}
	}
	public function infoapi()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('ajaxadmin/info') == FALSE){
				echo json_encode(array('error' => validation_errors()));
				exit;
			}
			$ID = (int) $this->input->post('ID');
			$Apis = (array) $this->Apis->find(array('id' => $ID));
			$service = (array) $this->Services->find(array('api_id' => $ID));
			if($Apis['count'] == 1 && $service)
			{
				$Apis['data'][0]->services = (int) $Apis['count'];
				echo json_encode(xss_array((array) $Apis['data'][0]));
				exit;
			}
			
		}
	}
	public function listapis()
	{
		if($this->input->method(TRUE) == 'GET')
		{ 
				 // Datatables Variables
	          $draw = intval($this->input->get("draw"));
	          $start = intval($this->input->get("start"));
	          $length = intval($this->input->get("length"));
	          $search = (array) $this->input->get('search');
	          $order = (array) $this->input->get('order');
	          if(count($order) == 0 || count($search) == 0) return null;

	          switch ($order[0]['column']) {
	          	case '1':
	          		$col = 'name';
	          		break;
	          	case '2':
	          		$col = 'type';
	          		break;
	          	case '3':
	          		$col = 'url';
	          		break;
	          	case '4':
	          		$col = 'created_at';
	          		break;
	          	case '5':
	          		$col = 'updated_at';
	          		break;
	          	
	          	default:
	          		$col = 'id';
	          		break;
	          }
	          $this->db
	          ->select('*')
	          ->from('apis')
	          ->group_start()
           	  ->like('name',$search['value'])
           	  ->or_like('url',$search['value'])
           	  ->or_like('type',$search['value'])
              ->group_end()
           	  ->order_by($col,$order[0]['dir'])
           	  ->limit($length,$start);
	          $users = $this->db->get();

	          $data = array();

	          foreach($users->result() as $r) {

	          	$services = $this->Services->find(array('api_id' => $r->id));
	               $data[] = array(
	                    xss_clean($r->id),
	                    xss_clean($r->name),
	                    xss_clean($r->type),
	                    xss_clean($r->url),
	                    xss_clean($services['count']),
	                    xss_clean($r->created_at),
	                    xss_clean($r->updated_at)
	               );
	          }
	          if($order[0]['column'] == '2')
	          {
	          	function desc($a,$b){
			        return($a[2] <= $b[2]) ? 1 : -1;
			    }
			    function asc($a,$b){
			        return($a[2] <= $b[2]) ? -1 : 1;
			    }
			    usort($data, $order[0]['dir']);
	          }
	          $output = array(
	               "draw" => $draw,
	                 "recordsTotal" => $this->db->get_where('apis')->num_rows(),
	                 "recordsFiltered" => $this->db->get_where('apis')->num_rows(),
	                 "data" => $data
	            );
	          echo json_encode($output);
	          exit;
        }
            		
	}
	public function deleteapi()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('ajaxadmin/info') == FALSE){
				echo json_encode(array('error' => validation_errors()));
				exit;
			}
			$ID = (int) $this->input->post('ID');
			$this->db->delete('apis',array('id' => $ID));
			echo json_encode(array('error' => false));
		}	
	}
	public function updateapi()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('ajaxadmin/updateapi') == FALSE){
				echo json_encode(array('error' => validation_errors()));
				exit;
			}
			$ID = (int) $this->input->post('ID');
			$name = (string) $this->input->post('name');
			$url = (string) $this->input->post('url');
			$type = (string) $this->input->post('type');
			$api = (string) $this->input->post('api');
			$apis =$this->Apis->find(array('id' => $ID));
			$builder =$this->db->get_where('api_builder',array('name' => $type));
			if($builder->num_rows() != 1)
			{
				echo json_encode(array('error' => 'API Type Not Found'));
				exit;

			}
			if($apis['count'] == 1)
			{
				$update = array(
					'name' => $name,
					'type' => $type,
					'api' => $api,
					'url' => $url
				);
	            $this->Apis->update($ID,$update);
	            echo json_encode(array('error' => false));
			} else {
				echo json_encode(array('error' => 'API Not Found'));
			}
			
		}
	}
	public function infoorder()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('ajaxadmin/info') == FALSE){
				echo json_encode(array('error' => validation_errors()));
				exit;
			}
			$ID = (int) $this->input->post('ID');
			 $this->db
	          ->select('orders.target,orders.quantity,services.name,users.username,users.balance,services.price,orders.status,orders.id,orders.created_at,orders.updated_at,orders.order_id_pro,orders.remains,orders.start')
	          ->from('orders')
	          ->join('services','services.id = orders.service_id')
	          ->join('users','users.id = orders.user_id')
           	  ->where('orders.id',$ID);
	          $order = $this->db->get();
	          if($order->num_rows() == 1)
	          {
	          	 echo json_encode(xss_array((array) $order->result()[0]));
	          	 exit;
	          }
	          echo json_encode(array('error' => 'Order not found'));

			
		}
	}
	public function refreshorder()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('ajaxadmin/info') == FALSE){
				echo json_encode(array('error' => validation_errors()));
				exit;
			}
			$ID = (int) $this->input->post('ID');
			 echo json_encode($this->Orders->checkOrder($ID));

			
		}
	}
	public function processorder()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('ajaxadmin/info') == FALSE){
				echo json_encode(array('error' => validation_errors()));
				exit;
			}
			$ID = (int) $this->input->post('ID');
			echo json_encode($this->Orders->proccessOrder($ID));
		}
	}
	public function updateorder()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('ajaxadmin/updateorder') == FALSE){
				echo json_encode(array('error' => validation_errors()));
				exit;
			}
			$ID = (int) $this->input->post('ID');
			$status = (string) $this->input->post('status');
			$start = (int) $this->input->post('start');
			$remains = (int) $this->input->post('remains');
			$order = (array) $this->Orders->find(array('id' => $ID));
 			if($order['count'] == 1)
			{
				$update = array(
					'status' => $status,
					'start' => $start,
					'remains' => $remains,
				);
				$service = (array) $this->Services->find(array('id' => $order['data'][0]->service_id));
				if($status == 'Canceled' && $service['count'] == 1 && $order['data'][0]->status != 'Canceled')
				{
					$refund = $service['data'][0]->price * $order['data'][0]->quantity;
					$this->db->query("UPDATE users set balance = balance + ? , balance_used = balance_used - ? WHERE id = ?",[$refund,$refund,$order['data'][0]->user_id]);
				}
				if($status == 'Partial' && $service['count'] == 1  && $order['data'][0]->status != 'Partial')
				{
					$refund = $service['data'][0]->price * ($remains > 0 ? $remains : $order['data'][0]->quantity);
					$this->db->query("UPDATE users set balance = balance + ? , balance_used = balance_used - ? WHERE id = ?",[$refund,$refund,$order['data'][0]->user_id]);
				}
	            $this->Orders->update($ID,$update);
	            echo json_encode(array('error' => false));
			} else {
				echo json_encode(array('error' => 'Order Not Found'));
			}
			
		}
	}
	public function listorders()
	{
		if($this->input->method(TRUE) == 'GET')
		{ 
				 // Datatables Variables
	          $draw = intval($this->input->get("draw"));
	          $start = intval($this->input->get("start"));
	          $length = intval($this->input->get("length"));
	          $search = (array) $this->input->get('search');
	          $order = (array) $this->input->get('order');
	          if(count($order) == 0 || count($search) == 0) return null;
	          switch ($order[0]['column']) {
	          	case '1':
	          		$col = 'users.username';
	          		break;
	          	case '2':
	          		$col = 'services.name';
	          		break;
	          	case '3':
	          		$col = 'orders.target';
	          		break;
	          	case '4':
	          		$col = 'orders.quantity';
	          		break;
	          	case '6':
	          		$col = 'orders.status';
	          		break;
	          	case '8':
	          		$col = 'orders.created_at';
	          		break;
	          	case '9':
	          		$col = 'orders.updated_at';
	          		break;
	          	
	          	default:
	          		$col = 'orders.id';
	          		break;
	          }
	          $this->db
	          ->select('orders.target,orders.quantity,services.name,users.username,users.balance,services.price,orders.status,orders.id,orders.created_at,orders.updated_at')
	          ->from('orders')
	          ->join('services','services.id = orders.service_id')
	          ->join('users','users.id = orders.user_id')
	          ->group_start()
           	  ->like('orders.target',$search['value'])
           	  ->or_like('orders.id',$search['value'])
           	  ->or_like('services.name',$search['value'])
           	  ->or_like('users.username',$search['value'])
           	  ->or_like('services.name',$search['value'])
           	  ->or_like('orders.status',$search['value'])
              ->group_end()
           	  ->order_by($col,$order[0]['dir'])
           	  ->limit($length,$start);
	          $users = $this->db->get();

	          $data = array();

	          foreach($users->result() as $r) {
	               $data[] = array(
	                    xss_clean($r->id),
	                    xss_clean($r->username."/".number_format($r->balance)),
	                    xss_clean($r->name),
	                    xss_clean($r->target),
	                    xss_clean($r->quantity),
	                    xss_clean($r->quantity * $r->price),
	                    xss_clean($r->status),
	                    xss_clean($r->created_at),
	                    xss_clean($r->updated_at)
	               );
	          }
	          if($order[0]['column'] == '2')
	          {
	          	function desc($a,$b){
			        return($a[2] <= $b[2]) ? 1 : -1;
			    }
			    function asc($a,$b){
			        return($a[2] <= $b[2]) ? -1 : 1;
			    }
			    usort($data, $order[0]['dir']);
	          }
	          $output = array(
	               "draw" => $draw,
	                 "recordsTotal" => $this->db->get_where('orders')->num_rows(),
	                 "recordsFiltered" => $this->db->get_where('orders')->num_rows(),
	                 "data" => $data
	            );
	          echo json_encode($output);
	          exit;
        }
	}
	public function listbalances()
	{
		if($this->input->method(TRUE) == 'GET')
		{ 
				 // Datatables Variables
	          $draw = intval($this->input->get("draw"));
	          $start = intval($this->input->get("start"));
	          $length = intval($this->input->get("length"));
	          $search = (array) $this->input->get('search');
	          $order = (array) $this->input->get('order');
	          if(count($order) == 0 || count($search) == 0) return null;
	          switch ($order[0]['column']) {
	          	case '1':
	          		$col = 'users.username';
	          		break;
	          	case '2':
	          		$col = 'balances.message';
	          		break;
	          	case '6':
	          		$col = 'balances.created_at';
	          		break;
	          	
	          	default:
	          		$col = 'balances.id';
	          		break;
	          }
	          $this->db
	          ->select('balances.id,balances.message,balances.created_at,balances.balance_used,balances.balance_after,balances.balance_before,balances.type,users.username')
	          ->from('balances')
	          ->join('users','users.id = balances.user_id')
	          ->group_start()
           	  ->like('users.username',$search['value'])
           	  ->or_like('balances.message',$search['value'])
           	  ->or_like('balances.id',$search['value'])
           	  ->or_like('users.balance',$search['value'])
           	  ->or_like('balances.balance_used',$search['value'])
           	  ->or_like('balances.balance_after',$search['value'])
           	  ->or_like('balances.balance_before',$search['value'])
              ->group_end()
           	  ->order_by($col,$order[0]['dir'])
           	  ->limit($length,$start);
	          $users = $this->db->get();

	          $data = array();

	          foreach($users->result() as $r) {
	          	switch ($r->type) {
	          		case '0':
	          			$type = 'subtraction';
	          			break;
	          		
	          		default:
	          			$type = 'addition';
	          			break;
	          	}
	               $data[] = array(
	                    xss_clean($r->id),
	                    xss_clean($r->username),
	                    xss_clean($r->message),
	                    xss_clean($type),
	                    xss_clean($r->balance_before),
	                    xss_clean($r->balance_used),
	                    xss_clean($r->balance_after),
	                    xss_clean($r->created_at),
	               );
	          }
	          if($order[0]['column'] == '4')
	          {
	          	function desc($a,$b){
			        return($a[4] <= $b[4]) ? 1 : -1;
			    }
			    function asc($a,$b){
			        return($a[4] <= $b[4]) ? -1 : 1;
			    }
			    usort($data, $order[0]['dir']);
	          }
	          if($order[0]['column'] == '5')
	          {
	          	function desc($a,$b){
			        return($a[5] <= $b[5]) ? 1 : -1;
			    }
			    function asc($a,$b){
			        return($a[5] <= $b[5]) ? -1 : 1;
			    }
			    usort($data, $order[0]['dir']);
	          }
	          if($order[0]['column'] == '6')
	          {
	          	function desc($a,$b){
			        return($a[6] <= $b[6]) ? 1 : -1;
			    }
			    function asc($a,$b){
			        return($a[6] <= $b[6]) ? -1 : 1;
			    }
			    usort($data, $order[0]['dir']);
	          }
	          $output = array(
	               "draw" => $draw,
	                 "recordsTotal" => $this->db->get_where('balances')->num_rows(),
	                 "recordsFiltered" => $this->db->get_where('balances')->num_rows(),
	                 "data" => $data
	            );
	          echo json_encode($output);
	          exit;
        }
	}
	public function infoaddfundmethod()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('ajaxadmin/info') == FALSE){
				echo json_encode(array('error' => validation_errors()));
				exit;
			}
			$ID = (int) $this->input->post('ID');
			$method =$this->Topup->find(array('id' => $ID));
	          if($method['count'] == 1)
	          {
	          	 echo json_encode(xss_array((array) $method['data'][0]));
	          	 exit;
	          }
	          echo json_encode(array('error' => 'Method not found'));

			
		}
	}
	public function updateaddfundmethod()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('ajaxadmin/updateaddfundmethod') == FALSE){
				echo json_encode(array('error' => validation_errors()));
				exit;
			}
			$ID = (int) $this->input->post('ID');
			$name = (string) $this->input->post('name');
			$rate = (string) $this->input->post('rate');
			$min = (int) $this->input->post('min');
			$max = (int) $this->input->post('max');
			$type = (string) $this->input->post('type');
			$username = (string) $this->input->post('username');
			$password = (string) $this->input->post('password');
			$info = (string) $this->input->post('info');
			$status = (int) $this->input->post('info');
			$topup = $this->Topup->find(array('id' => $ID));
 			if($topup['count'] == 1)
			{
				$update = array(
					'name' => $name,
					'rate' => $rate,
					'min' => $min,
					'max' => $max,
					'type' => $type,
					'username' => $username,
					'password' => $password,
					'info' => $info,
					'status' => $status
				
				);
	            $this->Topup->update($ID,$update);
	            echo json_encode(array('error' => false));
			} else {
				echo json_encode(array('error' => 'Add Fund Method Not Found'));
			}
			
		}
	}
	public function addaddfundmethod()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('ajaxadmin/addaddfundmethod') == FALSE){
				echo json_encode(array('error' => validation_errors()));
				exit;
			}
			$name = (string) $this->input->post('name');
			$rate = (string) $this->input->post('rate');
			$min = (int) $this->input->post('min');
			$max = (int) $this->input->post('max');
			$type = (string) $this->input->post('type');
			$username = (string) $this->input->post('username');
			$password = (string) $this->input->post('password');
			$info = (string) $this->input->post('info');
			$this->Topup->insert(array(
				'name' => $name,
				'rate' => $rate,
				'min' => $min,
				'max' => $max,
				'type' => $type,
				'username' => $username,
				'password' => $password,
				'info' => $info,
				'status' => 1
			));
			echo json_encode(array('error' => false));
			
		}
	}
	public function listaddfundmethods()
	{
		if($this->input->method(TRUE) == 'GET')
		{ 
	          $draw = intval($this->input->get("draw"));
	          $start = intval($this->input->get("start"));
	          $length = intval($this->input->get("length"));
	          $search = (array) $this->input->get('search');
	          $order = (array) $this->input->get('order');
	          if(count($order) == 0 || count($search) == 0) return null;
	          switch ($order[0]['column']) {
	          	case '1':
	          		$col = 'name';
	          		break;
	          	case '3':
	          		$col = 'min';
	          		break;
	          	case '4':
	          		$col = 'type';
	          		break;
	          	case '5':
	          		$col = 'min';
	          		break;
	          	case '6':
	          		$col = 'created_at';
	          		break;
	          	case '7':
	          		$col = 'updated_at';
	          		break;
	          	default:
	          		$col = 'id';
	          		break;
	          }
	          $this->db
	          ->select('*')
	          ->from('topup_method')
	          ->group_start()
           	  ->like('name',$search['value'])
           	  ->or_like('type',$search['value'])
           	  ->or_like('rate',$search['value'])
           	  ->or_like('min',$search['value'])
           	  ->or_like('max',$search['value'])
           	  ->or_like('id',$search['value'])
              ->group_end()
           	  ->order_by($col,$order[0]['dir'])
           	  ->limit($length,$start);
	          $users = $this->db->get();

	          $data = array();
	         
	          foreach($users->result() as $r) {
	          	 switch ($r->status) {
	          	case '1':
	          		$status = "Enable";
	          		break;
	          	
	          	default:
	          		$status = "Disable";
	          		break;
	          }
	          	  $request = $this->db->get_where('topup_requests',array('via' => $r->id));
	               $data[] = array(
	                    xss_clean($r->id),
	                    xss_clean($r->name. ' <br/>  ( ' .$status. ' )'),
	                    xss_clean($request->num_rows()),
	                    xss_clean($r->rate),
	                    xss_clean($r->type),
	                    xss_clean($r->min.' / '.$r->max),
	                    xss_clean($r->created_at),
	                    xss_clean($r->updated_at)
	               );
	          }
	         
	          $output = array(
	               "draw" => $draw,
	                 "recordsTotal" => $this->db->get_where('topup_method')->num_rows(),
	                 "recordsFiltered" => $this->db->get_where('topup_method')->num_rows(),
	                 "data" => $data
	            );
	          echo json_encode($output);
	          exit;
        }
	}
	public function deleteaddfundmethod()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('ajaxadmin/info') == FALSE){
				echo json_encode(array('error' => validation_errors()));
				exit;
			}
			$ID = (int) $this->input->post('ID');
			$this->db->delete('topup_method',array('id' => $ID));
			echo json_encode(array('error' => false));
		}	
	}
	public function infoaddfundrequests()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('ajaxadmin/info') == FALSE){
				echo json_encode(array('error' => validation_errors()));
				exit;
			}
			$ID = (int) $this->input->post('ID');
			$request = $this->db
	          ->select('topup_requests.id,topup_requests.info,topup_requests.via,topup_requests.amount,topup_requests.status,topup_requests.created_at,topup_requests.updated_at,topup_method.name,topup_method.type,topup_method.rate,users.username,users.balance')
	          ->from('topup_requests')
	          ->join('topup_method','topup_method.id = topup_requests.via')
	          ->join('users','users.id = topup_requests.user_id')
	          ->where('topup_requests.id',$ID)->get();
			if($request->num_rows() == 1)
			{
				echo json_encode(xss_array((array) $request->result()[0]));
				exit;
			} 
		}
	}
	public function updateaddfundrequest()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('ajaxadmin/updateaddfundrequest') == FALSE){
				echo json_encode(array('error' => validation_errors()));
				exit;
			}
			$ID = (int) $this->input->post('ID');
			$status = (int) $this->input->post('status');
			$topup = $this->Topup->admin($ID,$status);

			echo json_encode($topup);
			
		}
	}
	public function listaddfundrequests()
	{
		if($this->input->method(TRUE) == 'GET')
		{ 
	          $draw = intval($this->input->get("draw"));
	          $start = intval($this->input->get("start"));
	          $length = intval($this->input->get("length"));
	          $search = (array) $this->input->get('search');
	          $order = (array) $this->input->get('order');
	          if(count($order) == 0 || count($search) == 0) return null;
	          switch ($order[0]['column']) {
	          	case '1':
	          		$col = 'users.username';
	          		break;
	          	case '2':
	          		$col = 'topup_method.name';
	          		break;
	          	case '3':
	          		$col = 'topup_method.type';
	          		break;
	          	case '4':
	          		$col = 'topup_requests.amount';
	          		break;
	          	case '5':
	          		$col = 'topup_requests.status';
	          		break;
	          	case '6':
	          		$col = 'topup_requests.created_at';
	          		break;
	          	case '7':
	          		$col = 'topup_requests.updated_at';
	          		break;
	          	default:
	          		$col = 'topup_requests.id';
	          		break;
	          }
	          $this->db
	          ->select('topup_requests.id,topup_requests.amount,topup_requests.status,topup_requests.created_at,topup_requests.updated_at,topup_method.name,topup_method.type,users.username,users.balance')
	          ->from('topup_requests')
	          ->join('topup_method','topup_method.id = topup_requests.via')
	          ->join('users','users.id = topup_requests.user_id')
	          ->group_start()
           	  ->like('users.username',$search['value'])
           	  ->or_like('topup_method.name',$search['value'])
           	  ->or_like('topup_requests.status',$search['value'])
           	  ->or_like('topup_requests.amount',$search['value'])
           	  ->or_like('topup_method.type',$search['value'])
           	  ->or_like('topup_method.id',$search['value'])
              ->group_end()
           	  ->order_by($col,$order[0]['dir'])
           	  ->limit($length,$start);
	          $users = $this->db->get();

	          $data = array();
	         
	          foreach($users->result() as $r) {
	          	 switch ($r->status) {
	          	case '0':
	          		$status = "Pending";
	          		break;
	          	case '1':
	          		$status = "Waiting Validation";
	          		break;
	          	case '3':
	          		$status = "Canceled";
	          		break;
	          	
	          	default:
	          		$status = "Completed";
	          		break;
	          }
	          	  $request = $this->db->get_where('topup_requests',array('via' => $r->id));
	               $data[] = array(
	                    xss_clean($r->id),
	                    xss_clean($r->username.'<br/> (Rp.'.number_format($r->balance).')'),
	                    xss_clean($r->name),
	                    xss_clean($r->type),
	                    xss_clean($r->amount),
	                    xss_clean($status),
	                    xss_clean($r->created_at),
	                    xss_clean($r->updated_at)
	               );
	          }
	         
	          $output = array(
	               "draw" => $draw,
	                 "recordsTotal" => $users->num_rows(),
	                 "recordsFiltered" => $users->num_rows(),
	                 "data" => $data
	            );
	          echo json_encode($output);
	          exit;
        }
	}
	public function listtickets()
	{
		if($this->input->method(TRUE) == 'GET')
		{ 
	          $draw = intval($this->input->get("draw"));
	          $start = intval($this->input->get("start"));
	          $length = intval($this->input->get("length"));
	          $search = (array) $this->input->get('search');
	          $order = (array) $this->input->get('order');
	          if(count($order) == 0 || count($search) == 0) return null;
	          switch ($order[0]['column']) {
	          	case '1':
	          		$col = 'users.username';
	          		break;
	          	case '2':
	          		$col = 'tickets.subject';
	          		break;
	          	case '3':
	          		$col = 'tickets.status';
	          		break;
	          	case '4':
	          		$col = 'tickets.created_at';
	          		break;
	          	case '5':
	          		$col = 'tickets.updated_at';
	          		break;
	          	default:
	          		$col = 'tickets.id';
	          		break;
	          }
	          $this->db
	          ->select('users.username,tickets.subject,tickets.created_at,tickets.updated_at,tickets.status,tickets.id')
	          ->from('tickets')
	          ->join('users','users.id = tickets.user_id')
	          ->group_start()
           	  ->like('users.username',$search['value'])
           	  ->or_like('tickets.subject',$search['value'])
           	  ->or_like('tickets.status',$search['value'])
           	  ->or_like('tickets.id',$search['value'])
              ->group_end()
           	  ->order_by($col,$order[0]['dir'])
           	  ->limit($length,$start);
	          $users = $this->db->get();

	          $data = array();
	         
	          foreach($users->result() as $r) {
	          	// if($result->status == '0'){ echo '<p class="label label-success">Created</p>'; } else if($result->status == '1'){ echo '<p class="label label-success">Answered</p>'; } else if($result->status == '2'){ echo '<p class="label label-info">Asked</p>'; } else if($result->status == "3") { echo '<p class="label label-danger">Closed</p>'; }
	          	 switch ($r->status) {
		          	case '0':
		          		$status = '<p class="label label-success">Created</p>';
		          		break;
		          	case '1':
		          		$status = '<p class="label label-success">Answered</p>';
		          		break;
		          	case '3':
		          		$status = '<p class="label label-danger">Closed</p>';
		          		break;
		        
		          	default:
		          		$status = '<p class="label label-info">Asked</p>';
		          		break;
		          }
	               $data[] = array(
	                    xss_clean($r->id),
	                    xss_clean($r->username),
	                    xss_clean($r->subject),
	                    xss_clean($status),
	     
	                    xss_clean($r->created_at),
	                    xss_clean($r->updated_at)
	               );
	          }
	         
	          $output = array(
	               "draw" => $draw,
	                 "recordsTotal" => $this->db->get_where('tickets')->num_rows(),
	                 "recordsFiltered" => $this->db->get_where('tickets')->num_rows(),
	                 "data" => $data
	            );
	          echo json_encode($output);
	          exit;
        }
	}
}