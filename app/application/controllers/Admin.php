<?php
class Admin extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		if(!$this->session->auth)
			return redirect(base_url().'auth/signin');
		if($this->Users->level != '0')
			return redirect(base_url().'user');
		
	}
	public function orders()
	{
		if($this->input->method(TRUE) == 'GET') return $this->load->view('admin/orders');
	}
	public function index()
	{
		if($this->input->method(TRUE) == 'GET') return $this->load->view('admin/dashboard');
	}
	public function apis()
	{
		if($this->input->method(TRUE) == 'GET') return $this->load->view('admin/apis');
	}
	public function users()
	{
		if($this->input->method(TRUE) == 'GET') return $this->load->view('admin/users');
	}
	public function news()
	{
		if($this->input->method(TRUE) == 'GET') return $this->load->view('admin/news');
	}
	public function categories()
	{
		if($this->input->method(TRUE) == 'GET') return $this->load->view('admin/categories');
	}
	public function services()
	{
		if($this->input->method(TRUE) == 'GET') return $this->load->view('admin/services');
	}
	public function user_balances()
	{
		if($this->input->method(TRUE) == 'GET') return $this->load->view('admin/balances');
	}
	public function addfund_method()
	{
		if($this->input->method(TRUE) == 'GET') return $this->load->view('admin/addfundmethod');
	}
	public function addfund_request()
	{
		if($this->input->method(TRUE) == 'GET') return $this->load->view('admin/addfundrequest');
	}
	public function settings()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('admin/settings') == FALSE){
				$this->session->set_flashdata('messages', "<div class='alert alert-danger'>".validation_errors()."</div>");
				return redirect(base_url('admin/settings'));
			}
			$name = (string) $this->input->post('name');
			$theme = (string) $this->input->post('theme');
			$meta = (string) $this->input->post('meta');
			$register = (int) $this->input->post('register');
			$neworder = (int) $this->input->post('neworder');
			$smtp_host = (string) $this->input->post('smtp_host');
			$smtp_username = (string) $this->input->post('smtp_username');
			$smtp_password = (string) $this->input->post('smtp_password');
			$recaptcha_public = (string) $this->input->post('recaptcha_public');
			$recaptcha_secret = (string) $this->input->post('recaptcha_secret');
			$this->Site->update('1',array(
				'name' => $name,
				'meta' => $meta,
				'theme' => $theme,
				'register' => $register,
				'neworder' => $neworder,
				'smtp_host' => $smtp_host,
				'smtp_password' => $smtp_password,
				'smtp_username' => $smtp_username,
				'recaptcha_public' => $recaptcha_public,
				'recaptcha_secret' => $recaptcha_secret));
			$this->session->set_flashdata('messages', "<div class='alert alert-success'>Saved.</div>");
			return redirect(base_url('admin/settings'));
 		}
		if($this->input->method(TRUE) == 'GET') return $this->load->view('admin/settings');
	}
	public function create_page()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			if ($this->form_validation->run('admin/page/create') == FALSE){
				$this->session->set_flashdata('messages', "<div class='alert alert-danger'>".validation_errors()."</div>");
				return redirect(base_url('admin/page/create'));
			}
			$title = (string) $this->input->post('title');
			$body = (string) $this->input->post('body');

			$this->db->insert('pages',array('name' => $title,'html' => $body,'status' => 1));
			$this->session->set_flashdata('messages', "<div class='alert alert-success'>Saved</div>");
			return redirect(base_url('admin/page/list'));
		}
		if($this->input->method(TRUE) == 'GET') return $this->load->view('admin/createpage');
	}
	public function list_page()
	{
		if($this->input->method(TRUE) == 'GET') return $this->load->view('admin/pages');
	}
	public function edit_page()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			$pid = $this->uri->uri_to_assoc(2)['page'];
			if ($this->form_validation->run('admin/page/create') == FALSE){
				$this->session->set_flashdata('messages', "<div class='alert alert-danger'>".validation_errors()."</div>");
				return redirect(base_url('admin/page/'.$pid.'/edit'));
			}
			$title = (string) $this->input->post('title');
			$body = (string) $this->input->post('body');

			$this->db->where('id',$pid)->update('pages',array('name' => $title,'html' => $body,'status' => 1));
			$this->session->set_flashdata('messages', "<div class='alert alert-success'>Saved</div>");
			return redirect(base_url('admin/page/'.$pid.'/edit'));
		}
		if($this->input->method(TRUE) == 'GET') {
			$pid = $this->uri->uri_to_assoc(2)['page'];
			$page = $this->db->get_where('pages',array('id' => $pid))->result()[0];
			return $this->load->view('admin/editpage',$page);
		}
	}
	public function tickets()
	{
		if($this->input->method(TRUE) == 'GET') return $this->load->view('admin/tickets');
	}
	public function ticket_open()
	{
		if($this->input->method(TRUE) == 'POST')
		{
			$tid = $this->uri->uri_to_assoc(2)['ticket'];
			if($tid)
			{
				if ($this->form_validation->run('user/ticket/reply') == FALSE){
					$this->session->set_flashdata('messages', "<div class='alert alert-danger'>".validation_errors()."</div>");
					return redirect(base_url('admin/ticket/'.$tid.'/reply'));
				}
				$reply = $this->Tickets->reply($tid,strip_tags($this->input->post('message')),1);
				if($reply['error'] !== false)
				{
					$this->session->set_flashdata('messages', "<div class='alert alert-danger'>".$reply['error']."</div>");
				}
			}
			return redirect(base_url('admin/ticket/'.$tid.'/reply'));
		}
		if($this->input->method(TRUE) == 'GET')
		{
			$tid = $this->uri->uri_to_assoc(2)['ticket'];
			if($tid)
			{
				$ticket = $this->Tickets->find(array('id' => $tid,'user_id' => $this->Users->id));
				if($ticket['count'] == 1)
				{
					return $this->load->view('admin/ticketopen',$ticket['data'][0]);
					
				}
			}
			return redirect(base_url('admin/tickets'));
		}
	}
	public function serviceimport()
	{
		if($this->input->method(TRUE) == 'GET') return $this->load->view('admin/serviceimport');
	}
	public function servicechecker()
	{
		if($this->input->method(TRUE) == 'GET') return $this->load->view('admin/servicechecker');
	}
}