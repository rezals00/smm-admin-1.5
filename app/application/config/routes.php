<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'User';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['user'] = "User";
$route['landing'] = "Other/landing";
$route['user/dashboard'] = "User/index";
$route['user/order'] = "User/order";
$route['user/multiorder'] = "User/multiorder";
$route['user/history/orders'] = "User/orders";
$route['user/history/balances'] = "User/balances";
$route['user/addfunds/(:any)'] = "User/addfunds_open";
$route['user/balance/auto'] = "User/balance_auto";
$route['user/ticket/(:any)'] = "User/ticketopen";

$route['user/motm'] = "User/motm";

$route['admin/users/balances'] = "Admin/user_balances";
$route['admin/addfund/method'] = "Admin/addfund_method";
$route['admin/addfund/request'] = "Admin/addfund_request";
$route['admin/page/create'] = "Admin/create_page";
$route['admin/page/list'] = "Admin/list_page";
$route['admin/service/checker'] = "Admin/servicechecker";
$route['admin/service/import'] = "Admin/serviceimport";

$route['admin/page/(:any)/edit'] = "Admin/edit_page";
$route['admin/ticket/(:any)/reply'] = "Admin/ticket_open";

$route['ajax/checkservice'] = "AjaxUser/checkservice";
$route['ajax/send_email'] = "AjaxUser/sendemail";

$route['auth/forgot'] = "Other/forgot";
$route['auth/forgot_password'] = "Other/forgot_password";

$route['auth/signin'] = "other/signin";
$route['auth/signup'] = "other/signup";
$route['auth/signout'] = "other/signout";

$route['services'] = "other/services";
$route['api/doc'] = "other/api_doc";
<<<<<<< HEAD
$route['page'] = "other/page";
=======

>>>>>>> 5f22bf57b58558ce378746383a0aa20003cfcef1


