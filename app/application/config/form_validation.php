<?php 
$config = array(
	'auth/signup' => [
		[
			'field' => 'email',
			'label' => 'Email',
			'rules' => 'required|valid_emails|is_unique[users.email]|max_length[40]'
		],
		[
			'field' => 'username',
			'label' => 'Username',
			'rules' => 'required|alpha_numeric|is_unique[users.username]|max_length[24]'
		],
		[
			'field' => 'password',
			'label' => 'Password',
			'rules' => 'required|max_length[60]|min_length[8]',
		],
		[
			'field' => 'passwordconf',
			'label' => 'Password Confirmation',
			'rules' => 'required|matches[password]|max_length[60]',
		]

	],
	'auth/signin' => [
		[
			'field' => 'username',
			'label' => 'Username',
<<<<<<< HEAD
			'rules' => 'required|max_length[32]'
=======
			'rules' => 'required|alpha_numeric|max_length[24]'
>>>>>>> 5f22bf57b58558ce378746383a0aa20003cfcef1
		],
		[
			'field' => 'password',
			'label' => 'Password',
			'rules' => 'required|max_length[60]',
		],

	],
	'user/profile' => [
		[
			'field' => 'passwordold',
			'label' => 'OLD Password',
			'rules' => 'required|max_length[60]|min_length[8]',
		],
		[
			'field' => 'passwordnew',
			'label' => 'New Password',
			'rules' => 'required|max_length[60]|min_length[8]',
		],
		[
			'field' => 'passwordnew1',
			'label' => 'Password Confirmation',
			'rules' => 'required|matches[passwordnew]|max_length[60]',
		],
	],
	'user/order' => [
		[
			'field' => 'category',
			'label' => 'Category',
			'rules' => 'required|numeric|max_length[24]'
		],
		[
			'field' => 'service',
			'label' => 'Service',
			'rules' => 'required|numeric|max_length[24]'
		],
		[
			'field' => 'target',
			'label' => 'Target/Link',
			'rules' => 'required',
		],
		[
			'field' => 'quantity',
			'label' => 'Quantity',
			'rules' => 'required|numeric',
		],

	],
	'user/settings' => [
		[
			'field' => 'oldpassword',
			'label' => 'OLD Password',
			'rules' => 'required|max_length[60]',
		],
		[
			'field' => 'newpassword',
			'label' => 'New Password',
			'rules' => 'required|max_length[60]|min_length[8]',
		],
		[
			'field' => 'passwordconf',
			'label' => 'Password Confirmation',
			'rules' => 'required|matches[newpassword]|min_length[8]',
		],

	],
	'user/send_balance' => [
		[
			'field' => 'username',
			'label' => 'Username',
			'rules' => 'required',
		],
		[
			'field' => 'balance',
			'label' => 'Balance Amount',
			'rules' => 'required',
		],

	],
	'user/create_user' => [
		[
			'field' => 'username',
			'label' => 'Username',
			'rules' => 'required|alpha_numeric|is_unique[users.username]|max_length[24]'
		],
		[
			'field' => 'password',
			'label' => 'Password',
			'rules' => 'required|max_length[60]|min_length[8]',
		],
		[
			'field' => 'balance',
			'label' => 'Balance',
			'rules' => 'required',
		],
		[
			'field' => 'level',
			'label' => 'Level',
			'rules' => 'required|in_list[0,1]',
		],

	],
	'user/addfunds' => [
		[
			'field' => 'amount',
			'label' => 'Amount',
			'rules' => 'required|numeric'
		],
		[
			'field' => 'method',
			'label' => 'Method',
			'rules' => 'required|numeric',
		]

	],
	'user/ticket/new' => [
		[
			'field' => 'subject',
			'label' => 'Subject',
			'rules' => 'required|max_length[255]|min_length[4]',
		],
		[
			'field' => 'message',
			'label' => 'Message',
			'rules' => 'required|max_length[255]|min_length[4]',
		],

	],
	'user/ticket/reply' => [
		[
			'field' => 'message',
			'label' => 'Message',
			'rules' => 'required|max_length[255]|min_length[4]',
		],

	],
	'admin/settings' => [
		[
			'field' => 'name',
			'label' => 'Website Name',
			'rules' => 'required|max_length[255]',
		],
		[
			'field' => 'meta',
			'label' => 'Website Meta',
			'rules' => 'required',
		],
		[
			'field' => 'theme',
			'label' => 'Website Theme',
			'rules' => 'required',
		],
		[
			'field' => 'register',
			'label' => 'Free Register',
			'rules' => 'required',
		],
		[
			'field' => 'neworder',
			'label' => 'Website Mode',
			'rules' => 'required',
		],

	],
	'ajaxadmin/updateuser' => [
		[
			'field' => 'ID',
			'label' => 'User ID',
			'rules' => 'required|max_length[255]',
		],
		[
			'field' => 'level',
			'label' => 'Level',
			'rules' => 'required',
		],
		[
			'field' => 'password',
			'label' => 'Password',
			'rules' => 'required|max_length[60]|min_length[8]',
		],

	],
	'ajaxadmin/info' => [
		[
			'field' => 'ID',
			'label' => 'ID',
			'rules' => 'required|max_length[255]',
		]

	],
	'ajaxadmin/updatecategory' => [
		[
			'field' => 'ID',
			'label' => 'Category ID',
			'rules' => 'required|max_length[255]',
		],
		[
			'field' => 'name',
			'label' => 'Category Name',
			'rules' => 'required|max_length[255]',
		],
		[
			'field' => 'status',
			'label' => 'Category Status',
			'rules' => 'required',
		],

	],
	'ajaxadmin/addcategory' => [
		[
			'field' => 'name',
			'label' => 'Category Name',
			'rules' => 'required|max_length[255]',
		],
		[
			'field' => 'status',
			'label' => 'Category Status',
			'rules' => 'required',
		],

	],
	'ajaxadmin/updatenews' => [
		[
			'field' => 'ID',
			'label' => 'News ID',
			'rules' => 'required|max_length[255]',
		],
		[
			'field' => 'section',
			'label' => 'Section',
			'rules' => 'required',
		],
		[
			'field' => 'message',
			'label' => 'message',
			'rules' => 'required',
		],

	],
	'ajaxadmin/addnews' => [
		[
			'field' => 'section',
			'label' => 'Section',
			'rules' => 'required|max_length[255]',
		],
		[
			'field' => 'message',
			'label' => 'message',
			'rules' => 'required',
		],

	],
	'ajaxadmin/addservice' => [
		[
			'field' => 'name',
			'label' => 'Name',
			'rules' => 'required|max_length[255]',
		],
		[
			'field' => 'category',
			'label' => 'Category',
			'rules' => 'required',
		],
		[
			'field' => 'min',
			'label' => 'Min',
			'rules' => 'required',
		],
		[
			'field' => 'max',
			'label' => 'Max',
			'rules' => 'required',
		],
		[
			'field' => 'price',
			'label' => 'Price',
			'rules' => 'required',
		],
		[
			'field' => 'description',
			'label' => 'Description',
			'rules' => 'required',
		],
		[
			'field' => 'api',
			'label' => 'API Provider',
			'rules' => 'required',
		],
		[
			'field' => 'serviceid',
			'label' => 'serviceid',
			'rules' => 'required',
		],
		[
			'field' => 'mode',
			'label' => 'Mode',
			'rules' => 'required|in_list[Instant,Manual]',
		],

	],
	'ajaxadmin/updateservice' => [
		[
			'field' => 'ID',
			'label' => 'ID',
			'rules' => 'required|max_length[255]',
		],
		[
			'field' => 'name',
			'label' => 'Name',
			'rules' => 'required|max_length[255]',
		],
		[
			'field' => 'category',
			'label' => 'Category',
			'rules' => 'required',
		],
		[
			'field' => 'min',
			'label' => 'Min',
			'rules' => 'required',
		],
		[
			'field' => 'max',
			'label' => 'Max',
			'rules' => 'required',
		],
		[
			'field' => 'price',
			'label' => 'Price',
			'rules' => 'required',
		],
		[
			'field' => 'description',
			'label' => 'Description',
			'rules' => 'required',
		],
		[
			'field' => 'api',
			'label' => 'API Provider',
			'rules' => 'required',
		],
		[
			'field' => 'serviceid',
			'label' => 'serviceid',
			'rules' => 'required',
		],
		[
			'field' => 'mode',
			'label' => 'Mode',
			'rules' => 'required|in_list[Instant,Manual]',
		],
		[
			'field' => 'status',
			'label' => 'Status',
			'rules' => 'required|in_list[0,1]',
		],

	],
	'ajaxadmin/updateapi' => [
		[
			'field' => 'ID',
			'label' => 'ID',
			'rules' => 'required|max_length[255]',
		],
		[
			'field' => 'name',
			'label' => 'Name',
			'rules' => 'required|max_length[255]',
		],
		[
			'field' => 'type',
			'label' => 'type',
			'rules' => 'required|max_length[255]',
		],
		[
			'field' => 'url',
			'label' => 'URL',
			'rules' => 'required|valid_url',
		],
		[
			'field' => 'api',
			'label' => 'api',
			'rules' => 'required',
		]

	],
	'ajaxadmin/addapi' => [
		[
			'field' => 'name',
			'label' => 'Name',
			'rules' => 'required|max_length[255]',
		],
		[
			'field' => 'type',
			'label' => 'type',
			'rules' => 'required|max_length[255]',
		],
		[
			'field' => 'url',
			'label' => 'URL',
			'rules' => 'required|valid_url',
		],
		[
			'field' => 'api',
			'label' => 'api',
			'rules' => 'required',
		]

	],
	'ajaxadmin/updateorder' => [
		[
			'field' => 'ID',
			'label' => 'ID',
			'rules' => 'required|max_length[255]',
		],
		[
			'field' => 'start',
			'label' => 'Start',
			'rules' => 'required|max_length[255]',
		],
		[
			'field' => 'remains',
			'label' => 'Remains',
			'rules' => 'required|max_length[255]',
		],
		[
			'field' => 'status',
			'label' => 'Status',
			'rules' => 'required|in_list[Waiting,Pending,Processing,Completed,Partial,Canceled]',
		]

	],
	'ajaxadmin/addaddfundmethod' => [
		[
			'field' => 'name',
			'label' => 'Name',
			'rules' => 'required|max_length[255]',
		],
		[
			'field' => 'type',
			'label' => 'type',
			'rules' => 'required|in_list[Manual,Telkomsel,Mandiri,BCA]',
		],
		[
			'field' => 'rate',
			'label' => 'rate',
			'rules' => 'required',
		],
		[
			'field' => 'min',
			'label' => 'min',
			'rules' => 'required',
		],
		[
			'field' => 'max',
			'label' => 'max',
			'rules' => 'required',
		],


	],
	'ajaxadmin/updateaddfundmethod' => [
		[
			'field' => 'ID',
			'label' => 'ID',
			'rules' => 'required',
		],
		[
			'field' => 'name',
			'label' => 'Name',
			'rules' => 'required|max_length[255]',
		],
		[
			'field' => 'type',
			'label' => 'type',
			'rules' => 'required|in_list[Manual,Telkomsel,Mandiri,BCA]',
		],
		[
			'field' => 'rate',
			'label' => 'rate',
			'rules' => 'required',
		],
		[
			'field' => 'min',
			'label' => 'min',
			'rules' => 'required',
		],
		[
			'field' => 'max',
			'label' => 'max',
			'rules' => 'required',
		],
		[
			'field' => 'status',
			'label' => 'Status',
			'rules' => 'required|in_list[0,1]',
		],


	],
	'ajaxadmin/updateaddfundrequest' => [
		[
			'field' => 'ID',
			'label' => 'ID',
			'rules' => 'required',
		],
		[
			'field' => 'status',
			'label' => 'Status',
			'rules' => 'required|in_list[0,1,2,3]',
		],


	],
	'admin/page/create' => [
		[
			'field' => 'title',
			'label' => 'Title',
			'rules' => 'required',
		],
		[
			'field' => 'body',
			'label' => 'Body',
			'rules' => 'required',
		],


	],
	'ajax/sendemail' => [
		[
			'field' => 'email',
			'label' => 'Email',
			'rules' => 'required|valid_emails',
		]


	],
	'user/verify' => [
		[
			'field' => 'code',
			'label' => 'Activation Code',
			'rules' => 'required',
		]


	],
	'api/order' => [
		[
			'field' => 'key',
			'label' => 'API Key',
			'rules' => 'required',
		],
		[
			'field' => 'service',
			'label' => 'Service ID',
			'rules' => 'required|numeric',
		],
		[
			'field' => 'quantity',
			'label' => 'Quantity',
			'rules' => 'required|numeric',
		],
		[
			'field' => 'target',
			'label' => 'Target',
			'rules' => 'required',
		],


	],
	'api/status' => [
		[
			'field' => 'key',
			'label' => 'API Key',
			'rules' => 'required',
		],
		[
			'field' => 'order',
			'label' => 'Order ID',
			'rules' => 'required|numeric',
		],
		


	],
	'auth/forgot' => [
		[
			'field' => 'username',
			'label' => 'Username',
			'rules' => 'required',
		],
		[
			'field' => 'email',
			'label' => 'Email',
			'rules' => 'required|valid_emails',
		]
		


	],
	'auth/forgot_password' => [
		[
			'field' => 'code',
			'label' => 'Verification Code',
			'rules' => 'required',
		],
		[
			'field' => 'password',
			'label' => 'Password',
			'rules' => 'required|max_length[60]|min_length[8]',
		],
		


	],



);
?>