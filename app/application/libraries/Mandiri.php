<?php 
class Mandiri {
	public $cookie;
	public $username;
	public $password;
	public $debug;
	public function __construct(array $params){
		$this->username = $params['username'];
		$this->password = $params['password'];
		$this->debug = false;
		if($cookie == null){
			$cookie = ".cookie.txt";
		}
		$this->cookie = $cookie;
	}
	public function dos($url,$data = null,$ref = null,$fol = null){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		if($data == null){
			curl_setopt($ch, CURLOPT_POST, 0);
		} else {
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		}
		if($fol !== null){
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		}
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookie);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookie);
		if($ref !== null){
			curl_setopt($ch, CURLOPT_REFERER, $ref);
		}
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:10.0.2) Gecko/20100101 Firefox/10.0.2");
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		$data =  curl_exec($ch);
		curl_close($ch);
		return $data;

	}
	public function get($string,$data,$datas){
		$e = explode($data, $string);
		$x = explode($datas, $data[1]);
		print_r($e);
		print_r($x);
	}
	public function logout(){
		$Logout = $this->dos("https://ib.bankmandiri.co.id/retail/Logout.do?action=result");
		if($this->debug == true){
			file_put_contents("./Logout.html", $Logout);
		}
	}
	public function login(){
		$this->dos("https://ib.bankmandiri.co.id/retail/Login.do?action=form&lang=in_ID",null,"https://ib.bankmandiri.co.id/retail/Logout.do?action=result");
		$Login = $this->dos("https://ib.bankmandiri.co.id/retail/Login.do","action=result&userID={$this->username}&password={$this->password}&image.x=0&image.y=0",null,"true");
		if($this->debug == true){
			file_put_contents("./Login.html", $Login);
		}
	}
	public function getLastTransaction($num= null){
		if($num==null){
			$num=5;
		}
		$Trx = $this->dos("https://ib.bankmandiri.co.id/retail/TrxHistoryInq.do?action=form","https://ib.bankmandiri.co.id/retail/Login.do");
		$accID = strpos($Trx,'name="fromAccountID"');
		if( $accID && ($accID=strpos($Trx,' value="',++$accID)) && ($accID=strpos($Trx,' value="',++$accID)) ) {
		$accID = substr($Trx,$accID+8,15);
		$accID = rtrim($accID, '"> ');
		}
		$TrxMut = $this->dos("https://ib.bankmandiri.co.id/retail/TrxHistoryInq.do","action=result&fromAccountID=$accID&searchType=L&lastTransaction={$num}&sortType=Date&orderBy=ASC","https://ib.bankmandiri.co.id/retail/TrxHistoryInq.do?action=form");
		if($this->debug == true){
			file_put_contents("./TrxMut.html", $TrxMut);
		}
		$remove = explode("<html>", $TrxMut);
		$s = $this->getdata("<html>".$remove[1]);
		return $this->toarr($s);
	}
	public function getdata($data){
	    $DOM = new DOMDocument;
	    $DOM->loadHTML($data);

	    $items = $DOM->getElementsByTagName('td');
	    foreach ($items as $node) {
	        if($node->getAttribute("class") == "tabledata"){
	            $array[] = $this->tdrows($node->childNodes);
	        }
	    }
	    return $array;
	}
	public function toint($data){
		$find = array(".",",");
		$replace = array("","");
		return str_replace($find, $replace, $data);
	}
	public function toarr($data){
	    $ret = array();
	    $bt = '-4';
	    $bk = '-3';
	    $bd = '-2';
	    $bkr = '-1';
	    for($i=0;$i<count($data)/4-1;$i++){
	        $bt = $bt+4;
	        $bk = $bk+4;
	        $bd = $bd+4;
	        $bkr = $bkr+4;
	        $ret[$i]['tanggal'] = $this->toint($data[$bt]);
	        $ret[$i]['keterangan'] = $this->toint($data[$bk]);
	        $ret[$i]['debet'] = (int) $this->toint($data[$bd]);
	        $ret[$i]['kredit'] = (int) $this->toint($data[$bkr]);
	    }
	    return $ret;
	}
	function tdrows($elements){
	    $str = "";
	    foreach ($elements as $element) {
	        $str .= $element->nodeValue . ", ";
	    }

	    return $str;
	}
}
?>