<?php 
use SMSGatewayMe\Client\ApiClient;
use SMSGatewayMe\Client\Configuration;
use SMSGatewayMe\Client\Api\MessageApi;
class Sms
{

	public function set(string $key)
	{
		$config = Configuration::getDefaultConfiguration();
		$config->setApiKey('Authorization', $key);
		$apiClient = new ApiClient($config);
		$messageClient = new MessageApi($apiClient);
		$this->messageClient = $messageClient;
	}
	public function search(array $array)
	{
		$messages = $this->messageClient->searchMessages($array);
		return $messages;
	}
}