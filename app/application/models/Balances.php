<?php 
class Balances extends CI_Model
{
	protected $table = 'balances';
	public function __construct()
	{
		parent::__construct();
	}

	public function find(array $array)
	{
		$data = $this->db->get_where($this->table,$array);
		return array('data' => xss_array($data->result()) , 'count' => $data->num_rows());
	}
	public function update(string $userid,array $update)
	{
		return $this->db->where('id',$userid)->update($this->table,$update);
	}
	public function insert(array $insert)
	{
		return $this->db->insert($this->table,$insert);
	}
	public function listBalances($q = null,$limit_start = 0,$count = false){
            if(!$this->session->auth) return null;
            $data = $this->db->select('*')
            ->from('balances')
            ->where('user_id',$this->Users->id)
            ->group_start()
            ->like('message',$q)
            ->or_like('balance_before',$q)
            ->or_like('balance',$q)
            ->or_like('date',$q)
            ->group_end()
            ->order_by('id','DESC');
            if($count){
                  return $data->get()->num_rows();
            }
            return $data->limit(25,$limit_start)->get()->result();
      }
}