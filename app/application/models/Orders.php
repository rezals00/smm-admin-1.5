<?php 
class Orders extends CI_Model
{
	protected $table = 'orders';
	public function __construct()
	{
		parent::__construct();
		$curl = $this->curl->curl;
		$this->curl = $curl;
	}

	public function find(array $array)
	{
		$data = $this->db->get_where($this->table,$array);
		return array('data' => xss_array($data->result()) , 'count' => $data->num_rows());
	}
	public function update(string $id,array $update)
	{
		return $this->db->where('id',$id)->update($this->table,$update);
	}
	public function insert(array $insert)
	{
		return $this->db->insert($this->table,$insert);
	}
	private function findarray(string $required,array $array)
	{
		$split = explode('.',$required);
		$found = false;
		foreach ($split as $key => $value) {
			$found = false;
			if(array_key_exists($value, $array))
			{
				$array = $array[$value];
				$found = true;
			} 
		}
		if($found)
		{
			return $array;
		}
		return null;
	}
	public function autoupdate()
	{
	    $check=[];
		foreach ($this->db->query("
			SELECT o.id,o.status,o.order_id_pro 
			FROM orders as o 
			inner join services as s 
			on o.service_id = s.id
			WHERE o.status <> 'Completed' 
			AND o.status <> 'Canceled' 
			AND o.status <> 'Partial' 
			AND o.order_id_pro <> '' 
			AND o.order_id_pro <> '0' 
			AND o.order_id_pro <> '-'
			AND s.api_id <> '0'
			ORDER BY RAND() LIMIT 10")->result() as $data) {
			$start = time();
			$c = $this->Orders->checkOrder((int) $data->id);
			$c['id'] = $data->id;
			$c['time'] = time() - $start;
			$check[] = $c;
		}
		echo json_encode($check);
	}
	public function status(int $order,string $key)
	{
		$find = $this->db->select('orders.status,orders.start,orders.remains,users.token')->from('orders')->join('users','users.id = orders.user_id')->where('orders.id',$order)->get();
		if($find->num_rows() != 1)
		{
			return array('error' => 'order not found');
		} 
		$result = $find->result()[0];
		if($result->token !== $key)
		{
			return array('error' => 'you cant view other user orders');
		}
		return array('error' => false,'status' => $result->status,'start' => $result->start,'remains' => $result->remains);
	}
	public function proccessOrder(int $orderid)
	{
		$order = $this->find(array('id' => $orderid,'order_id_pro' => '-','status' => 'Waiting'));
			if($order['count'] == 1)
			{
				$this->db->select('
			services.api_id,
			services.category_id,
			services.pro_id,
			services.name,
			services.min,
			services.max,
			services.type,
			services.price,
			apis.url,
			apis.api,
			api_builder.orders
			')
		->from('services')
		->join('apis','apis.id = services.api_id')
		->join('api_builder','api_builder.name = apis.type')
		->where('services.id',$order['data'][0]->service_id)
		->where('services.status', '1');
		$r =$this->db->get();
		if($r->num_rows() == 0){
			return array('error' => 'Service Not Found');
		}
		$data = $r->result()[0];
		$format = json_decode($data->orders);
		if($format == null)
		{
			return array('error' => 'BAD TYPES');
		}
		$order_find = array('[service]','[quantity]','[target]','[key]');
		$order_replace = array($data->pro_id,$order['data'][0]->quantity,$order['data'][0]->target,$data->api);
		$order = [];

		foreach ($format->form->form as $key => $value) {
			$order[$key] = str_replace($order_find, $order_replace, $value);
		}
		$url = $data->url.''.str_replace($order_find, $order_replace, $format->form->url);
		$this->curl->post($url,$order);
		$res = (array) json_decode(json_encode($this->curl->response),true);
		$msg_path = $this->findarray($format->result->msg, $res);
		$orderid_path = $this->findarray($format->result->orderid, $res);
		if($orderid_path == null)
		{
			if($msg_path != null)
			{
				return array('error' => $msg_path);
			}
			if($msg_path == null)
			{
				return array('error' => 'Something Went Wrong ');
			}
		}
		$this->Orders->update($orderid,array('order_id_pro' => $orderid_path,'status' => 'Pending'));
		return array('error' => false,'order' => $orderid,'service' => $data->name,'charge' => $charge);
		} else {
			return array('error' => 'Not Found or not eligeble');
		}
	}
	public function createOrder(int $service,string $target,int $quantity,string $apikey = null)
	{
		try{
				$this->db->trans_start();
				if($apikey == null)
				{
					if(!$this->session->userdata('auth'))
					{
						throw new Exception("U must Sign in before order", 1);
					}
				}
				if($apikey != null)
				{
					$user = $this->Users->find(array('token' => $apikey));
					if($user['count'] == 0)
					{
						throw new Exception("invalid api key not found", 1);
					}
				}
				$user = ($apikey == null ? $this->Users->info() : $this->Users->find(array('token' => $apikey))['data'][0]);
				if($user->balance <= 0)
				{
					throw new Exception("insufficient balance", 1);					
				}
				$via = ($apikey == null ? 'WEB' : 'API');
				$ser = $this->Services->find(array('id' => $service,'status' => 1));
				if($ser['count'] != 1)
				{
					throw new Exception("service not found", 1);
				}
				$data = $ser['data'][0];
				$charge = (float) $data->price * $quantity;
			
				if($user->balance < $charge)
				{
					throw new Exception("insufficient balance ", 1);
				}
				if($quantity < $data->min)
				{
					throw new Exception('min '.$data->min, 1);					
				}
				if($quantity > $data->max)
				{
					throw new Exception('max '.$data->max,1);
				}
				// ORDER MANUAL
				if($ser['count'] == 1 && $ser['data'][0]->api_id == '0')
				{
					$balance_before = $user->balance;
					$this->Users->update($user->id,
						array('balance' => $user->balance - $charge,'balance_used' => $user->balance_used + $charge)
					);

					$this->insert(array(
						'user_id' => $user->id,
						'service_id' => $service,
						'order_id_pro' => '0',
						'start' => 0,
						'remains' => 0,
						'status' => 'Pending',
						'quantity' => $quantity,
						'target' => $target,
						'via' => $via
					));
					$id = $this->db->insert_id();
					$after = ($apikey == null ? $this->Users->info() : $this->Users->find(array('token' => $apikey))['data'][0]);
					if($after->balance >= $balance_before)
					{
						$this->db->trans_rollback(); 
						$this->Users->update($after->id, array('status' => '1'));
						throw new Exception('Not Enough Balance',1);
					}
					$this->Balances->insert(array(
						'balance_before' => $balance_before,
						'balance_used' => $charge,
						'type' => '0',
						'message' => 'Order #'.$id.' [ '.$via.' ]',
						'user_id' => $user->id,
						'balance_after' => $after->balance
					));
					$this->db->trans_complete();

					return array('error' => false,'order' => $id,'service' => $data->name,'charge' => $charge);
			}

			// ORDER WITH API
			$this->db->select('
				services.api_id,
				services.category_id,
				services.pro_id,
				services.name,
				services.min,
				services.max,
				services.type,
				services.price,
				services.mode,
				apis.url,
				apis.api,
				api_builder.orders
				')
			->from('services')
			->join('apis','apis.id = services.api_id')
			->join('api_builder','api_builder.name = apis.type')
			->where('services.id',$service)
			->where('services.status', '1');
			$r =$this->db->get();
			if($r->num_rows() == 0){
				throw new Exception('Service Not Found',1);
			}
			$data = $r->result()[0];
			$charge = (float)$data->price * $quantity;
			$format = json_decode($data->orders);
			if($format == null)
			{
				throw new Exception('BAD TYPES',1);
			}
			if($this->Site->neworder == 0 || $data->mode == "Manual")
			{
				$balance_before = $user->balance;
			 	$this->db->query("update users set balance = balance - ? ,balance_used  = balance_used + ? where id = ?",[$charge,$charge,$user->id]);
				$this->insert(array(
					'user_id' => $user->id,
					'service_id' => $service,
					'order_id_pro' => '-',
					'start' => 0,
					'remains' => 0,
					'status' => 'Waiting',
					'quantity' => $quantity,
					'target' => $target,
					'via' => $via
				));
				$id = $this->db->insert_id();
				$after = ($apikey == null ? $this->Users->info() : $this->Users->find(array('token' => $apikey))['data'][0]);
				$this->Balances->insert(array(
					'balance_before' => $balance_before,
					'balance_used' => $charge,
					'type' => '0',
					'message' => 'Order #'.$id.' [ '.$via.' ]',
					'user_id' => $user->id,
					'balance_after' => $after->balance
				));
				if($after->balance >= $balance_before)
				{
					$this->db->trans_rollback(); 
					$this->Users->update($after->id, array('status' => '1'));
					throw new Exception('Not Enough Balance',1);
				}
				$this->db->trans_complete();
				return array('error' => false,'order' => $id,'service' => $data->name,'charge' => $charge);
			}
			$order_find = array('[service]','[quantity]','[target]','[key]');
			$order_replace = array($data->pro_id,$quantity,$target,$data->api);
			$order = [];

			foreach ($format->form->form as $key => $value) {
				$order[$key] = str_replace($order_find, $order_replace, $value);
			}
			$balance_before = $user->balance;
			$this->db->query("update users set balance = balance - ? ,balance_used  = balance_used + ? where id = ?",[$charge,$charge,$user->id]);
			$after = ($apikey == null ? $this->Users->info() : $this->Users->find(array('token' => $apikey))['data'][0]);
			if($after->balance >= $balance_before)
			{
				$this->db->trans_rollback(); 
				$this->Users->update($after->id, array('status' => '1'));
				throw new Exception('Not Enough Balance',1);
			}
			$url = $data->url.''.str_replace($order_find, $order_replace, $format->form->url);
			$this->curl->post($url,$order);
			$res = (array) json_decode(json_encode($this->curl->response),true);
			$msg_path = $this->findarray($format->result->msg, $res);
			$orderid_path = $this->findarray($format->result->orderid, $res);
			if($orderid_path == null)
			{
				if($msg_path != null)
				{
					return array('error' => $msg_path);
				}
				if($msg_path == null)
				{
					throw new Exception('Something Went Wrong ',1);
				}
			}
			$this->insert(array(
				'user_id' => $user->id,
				'service_id' => $service,
				'order_id_pro' => $orderid_path,
				'start' => 0,
				'remains' => 0,
				'status' => 'Pending',
				'quantity' => $quantity,
				'target' => $target,
				'via' => $via
			));
			$id = $this->db->insert_id();
			$this->Balances->insert(array(
				'balance_before' => $balance_before,
				'balance_used' => $charge,
				'type' => '0',
				'message' => 'Order #'.$id.' [ '.$via.' ]',
				'user_id' => $user->id,
				'balance_after' => $after->balance
			));
						$this->db->trans_complete();
			return array('error' => false,'order' => $id,'service' => $data->name,'charge' => $charge);
		} catch (Exception $e) {
			$this->db->trans_rollback();
			return array('error' => $e->getMessage());
        }

		

	}
	public function checkOrder(int $id)
	{
	    try{
	        $this->db->trans_start();
    		$order = $this->find(array('id' => $id));
    		if($order['count'] != 1)
    		{
    			throw new Exception("Order not found", 1);
    			
    		}
    		$this->db
    		->select('orders.status,orders.user_id,orders.id,orders.order_id_pro,orders.quantity,services.api_id,services.price,apis.name,apis.url,apis.api,api_builder.status as api_status')
    		->from('orders')
    		->join('services','services.id = orders.service_id')
    		->join('apis','apis.id = services.api_id')
    		->join('api_builder','api_builder.name = apis.type')
    		->where('orders.id',$id);
    		$get = $this->db->get();
    		if($get->num_rows() != 1)
    		{
    			throw new Exception("order not found", 1);
    		}
    		$result = $get->result()[0];
    		if($result->order_id_pro == '' || $result->order_id_pro == '0')
    		{
    			throw new Exception("provider id error", 1);
    		}
    		if($result->status == 'Canceled' || $result->status == 'Partial' || $result->status == 'Completed')
    		{
    			throw new Exception("this order has complete", 1);
    		}
    		$format = json_decode($result->api_status);
    		$status_find = array('[key]','[orderid]');
    		$status_replace = array($result->api,$result->order_id_pro);
    		$order = [];
    
    		foreach ($format->form->form as $key => $value) {
    			$order[$key] = str_replace($status_find, $status_replace, $value);
    		}
    		$url = $result->url.''.str_replace($status_find, $status_replace, $format->form->url);
    		$this->curl->post($url,$order);
    		$res = (array) json_decode(json_encode($this->curl->response),true);
    		$status = $this->findarray($format->result->status, $res);
    		$start_count = $this->findarray($format->result->start_count, $res);
    		$remains = $this->findarray($format->result->remains, $res);
    		if($status == null)
    		{
    			throw new Exception("something went wrong", 1);
    		}
    		$currentStatus = null;
    		foreach ($format->result->statusTo as $key => $value) {
    			if($value == $status) $currentStatus = $key;
    		}
    		if($currentStatus == null)
    		{
    			throw new Exception("bad api types or builder or something went wrong", 1);
    		}
    		if($currentStatus == $result->status)
    		{
    			throw new Exception("status not changing", 1);
    		}
    		$this->db
    		->select('orders.status,orders.user_id,orders.id,orders.order_id_pro,orders.quantity,services.api_id,services.price,apis.name,apis.url,apis.api,api_builder.status as api_status')
    		->from('orders')
    		->join('services','services.id = orders.service_id')
    		->join('apis','apis.id = services.api_id')
    		->join('api_builder','api_builder.name = apis.type')
    		->where('orders.id',$id);
    		$get = $this->db->get();
    		if($get->num_rows() != 1)
    		{
    			throw new Exception("order not found", 1);
    		}
    		$result = $get->result()[0];
    		if($result->order_id_pro == '' || $result->order_id_pro == '0')
    		{
    			throw new Exception("provider id error", 1);
    		}
    		if($result->status == 'Canceled' || $result->status == 'Partial' || $result->status == 'Completed')
    		{
    			throw new Exception("this order has complete", 1);
    		}
    		$user = $this->Users->find(array('id' => $result->user_id));
    		if($user['count'] != 1)
    		{
    			throw new Exception("User Not Found", 1);
    		}
    		$refund = 0;
    		if($currentStatus == 'Canceled')
    		{
    			$refund = $result->price * $result->quantity;
    			$this->db->query("UPDATE users set balance = balance + ? , balance_used = balance_used - ? WHERE id = ?",[$refund,$refund,$result->user_id]);
    		}
    		if($currentStatus == 'Partial')
    		{
    			$refund = $result->price * $remains;
    			$this->db->query("UPDATE users set balance = balance + ? , balance_used = balance_used - ? WHERE id = ?",[$refund,$refund,$result->user_id]);
    		}
    		$after = $this->Users->find(array('id' => $result->user_id));
    		if($after['count'] != 1)
    		{
    			throw new Exception("User Not Found", 1);
    		}
    		$this->db->insert('balances',array(
    			'user_id' => $result->user_id,
    			'message' => 'Change Status '.$result->status.' To '.$currentStatus.' #'.$result->id,
    			'balance_used' => $refund,
    			'type' => 1,
    			'balance_before' => $user['data'][0]->balance,
    			'balance_after' => $after['data'][0]->balance
    		));
    		$this->update($result->id, array('status' => $currentStatus,'remains' => $remains,'start' => $start_count));
    		$this->db->trans_complete();
    		return array(
				'error' => false,
				'status' => $currentStatus,
				'status_before' => $result->status,
				'remains' => $remains,
				'start_count' => $start_count,
				'affected_rows' => $this->db->affected_rows(),
				'error_message' => $this->db->error(),
				'trans' => $this->db->trans_status(),
				'last_query' => $this->db->last_query()
			);
		} catch (Exception $e) {
			$this->db->trans_rollback();
			return array('error' => $e->getMessage());
        }

	
	}
	public function countOrders($q = '')
	{
		if(!$this->session->auth) return null;
		$this->db->select('services.name ,
                              services.price,
                              orders.target,
                              orders.status,
                              orders.start,
                              orders.remains,
                              orders.data,
                              orders.created_at,
                              orders.quantity,
                              orders.id')
            ->from('orders')
            ->join("services","services.id = orders.service_id")
            ->where('orders.user_id',$this->Users->id)
            ->group_start()
            ->like('orders.target',$q)
            ->or_like('services.name',$q)
            ->or_like('orders.id',$q)
            ->or_like('orders.created_at',$q)
            ->group_end()
            ->order_by('orders.id','DESC');
            $b = $this->db->get();
            return $b->num_rows();
	}
	public function listOrders($q = '',$limit_start = 0,$limit_end = 25){
            if(!$this->session->auth) return null;
            $this->db->select('services.name ,
                              services.price,
                              orders.target,
                              orders.status,
                              orders.start,
                              orders.via,
                              orders.remains,
                              orders.data,
                              orders.created_at,
                              orders.quantity,
                              orders.id')
            ->from('orders')
            ->join("services","services.id = orders.service_id")
            ->where('orders.user_id',$this->Users->id)
            ->group_start()
            ->like('orders.target',$q)
            ->or_like('services.name',$q)
            ->or_like('orders.status',$q)
            ->or_like('orders.id',$q)
            ->or_like('orders.created_at',$q)
            ->group_end()
            ->limit(25,$limit_start)
            ->order_by('orders.id','DESC');
            $q = $this->db->get();
            
            return $q->result();
		
	}
     
     
}