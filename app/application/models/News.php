<?php 
class News extends CI_Model
{
	protected $table = 'news';
	public function __construct()
	{
		parent::__construct();
	}

	public function find(array $array)
	{
		$data = $this->db->get_where($this->table,$array);
		return array('data' => xss_array($data->result()) , 'count' => $data->num_rows());
	}
	public function update(string $userid,array $update)
	{
		return $this->db->where('id',$userid)->update($this->table,$update);
	}
	public function insert(array $insert)
	{
		return $this->db->insert($this->table,$insert);
	}
}