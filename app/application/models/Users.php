<?php 
class Users extends CI_Model
{
	protected $table = 'users';
	public function __construct()
	{
		parent::__construct();
		$this->info();
		$curl = $this->curl->curl;
		$this->curl = $curl;
	}
	public function info()
	{
		if($this->session->userdata('auth'))
		{
			$result = $this->find(array('id' => $this->session->userdata('auth')));
			if($result['count'] == 1)
			{

				foreach ($result['data'][0] as $key => $value) {
					$this->{$key} = $value;
				}
			}
			if($this->status == 1)
			{
				echo "<small>Your Account Has been banned</small>";
				exit;
			}
		}
		return $this;
	}
	public function find(array $array)
	{
		$data = $this->db->get_where($this->table,$array);
		return array('data' => xss_array($data->result()) , 'count' => $data->num_rows());
	}
	public function update(string $id,array $update)
	{
		return $this->db->where('id',$id)->update($this->table,$update);
	}
	public function insert(array $insert)
	{
		return $this->db->insert($this->table,$insert);
	}
	public function update_ip(int $id,string $ip)
	{
		$this->curl->get('http://ip-api.com/json/'.$ip);
		$res = (array) json_decode(json_encode($this->curl->response),true);
		$this->update($id,array('ip_address' => $ip,'ip_country' => $res['countryCode']));
	}
	public function create_user(string $username,string $password,int $balance,int $level)
	{
		if(!$this->session->userdata('auth')) return array('error' => 'Please sign in');
		$this->db->trans_begin();
		$f = $this->find(array('username' => $username));
		if($f['count'] != 0)
		{
			return array('error' => 'Username has used');
		}
		switch ($level) {
			case '2':
				$display = 'Reseller';
				$cut = 25000;
				break;
			
			default:
				$display = 'Member';
				$cut = 5000;
				$level = 1;
				break;
		}
		if($this->balance < $cut)
		{
			return array('error' => "Not enough Balance");
		}
		$this->db->query("UPDATE users set balance = balance - ?,balance_used = balance_used + ? where id = ?",[$cut,$cut,$this->id]);
		$userm = $this->find(array('id' => $this->id));
		if($userm['data'][0]->balance >= $this->balance)
		{
			$this->db->trans_off();
			$this->update($this->id, array('status' => '1'));
			return array('error' => 'Not Enough Balance');
		}
		$this->Balances->insert(array(
			'user_id' => $this->id,
			'balance_used' => $cut,
			'balance_before' =>  $this->balance,
			'balance_after' => $userm['data'][0]->balance,
			'message' => 'Create User '.$username.' With Balance '.$balance,
			'type' => 0
		));
		$this->insert(array(
			'username' => $username,
			'password' => password_hash($password, PASSWORD_BCRYPT, ['cost' => 12]),
			'level' => $level,
			'balance' => 0,
    		'balance_used' => 0,
    		'via' => 'Reseller '.$this->username,
    		'token' => sha1(md5(time))
 		));
 		$this->send_balance($username,$balance);
		$this->db->trans_complete();
		return array('error' => false,'display' => $display);
	}
	public function send_balance(string $username,int $amount)
	{
		if(!$this->session->userdata('auth')) return array('error' => 'Please sign in');
		$this->db->trans_begin();
		$user = $this->find(array('username' => $username));
		if($user['count'] != 1)
		{
			return array('error' => 'User Not Found');
		}
		if($username == $this->username)
		{
			return array('error' => "Can't Self transfer");
		}
		if($amount < 1)
		{
			return array('error' => 'Minimum Transfer : 1');
		}
		if($this->balance < $amount)
		{
			return array('error' => 'Not enough balance');
		}
		$this->db->query("UPDATE users set balance = balance + ? where id = ?",[$amount,$user['data'][0]->id]);
		$this->db->query("UPDATE users set balance = balance - ?,balance_used = balance_used + ? where id = ?",[$amount,$amount,$this->id]);
		$userm = $this->find(array('id' => $this->id));
		$this->Balances->insert(array(
			'user_id' => $this->id,
			'balance_used' => $amount,
			'balance_before' =>  $this->balance,
			'balance_after' => $userm['data'][0]->balance,
			'message' => 'Transfer Rp.'.$amount.' to '.$username.'',
			'type' => 0
		));
		$usert = $this->find(array('id' => $user['data'][0]->id));
		$this->Balances->insert(array(
			'user_id' => $this->id,
			'balance_used' => $amount,
			'balance_before' =>  $user['data'][0]->balance,
			'balance_after' => $usert['data'][0]->balance,
			'message' => 'Receive Rp.'.$amount.' from '.$username.'',
			'type' => 1
		));
		$this->db->trans_complete();		return array('error' => false);
	}
}