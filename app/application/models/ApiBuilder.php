<?php 
class ApiBuilder extends CI_Model
{
	protected $table = 'api_builder';
	public function __construct()
	{
		parent::__construct();
	}
	public function find(array $array)
	{
		$data = $this->db->get_where($this->table,$array);
		return array('data' => $data->result() , 'count' => $data->num_rows());
	}
	public function update(string $userid,array $update)
	{
		return $this->db->where('id',$userid)->update($this->table,$update);
	}
	public function insert(array $insert)
	{
		return $this->db->insert($this->table,$insert);
	}
}
?>