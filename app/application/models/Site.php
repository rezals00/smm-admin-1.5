<?php 
class Site extends CI_Model
{
	protected $table = 'site';
	public $version = '1.5.0';
	public function __construct()
	{
		parent::__construct();
		$this->info();
	}
	protected function info()
	{
		foreach ($this->find(array('id' => 1))['data'][0] as $key => $value) {
			$this->{$key} = $value;
		}
	}
	public function find(array $array)
	{
		$data = $this->db->get_where($this->table,$array);
		return array('data' => $data->result() , 'count' => $data->num_rows());
	}
	public function update(string $id,array $update)
	{
		return $this->db->where('id',$id)->update($this->table,$update);
	}
	public function insert(array $insert)
	{
		return $this->db->insert($this->table,$insert);
	}
}