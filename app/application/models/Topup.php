<?php 
class Topup extends CI_Model
{
	protected $table = 'topup_method';
	protected $table_request = 'topup_requests';
	public function __construct()
	{
		parent::__construct();
	}

	public function find(array $array)
	{
		$data = $this->db->get_where($this->table,$array);
		return array('data' => xss_array($data->result()) , 'count' => $data->num_rows());
	}
	public function update(string $userid,array $update)
	{
		return $this->db->where('id',$userid)->update($this->table,$update);
	}
	public function insert(array $insert)
	{
		return $this->db->insert($this->table,$insert);
	}

	public function start(int $method,int $amount)
	{
		$this->db->trans_begin();
		$met = $this->find(array('id' => $method,'status' => 1));
		if($met['count'] != 1)
		{
			return array('error' => 'please select available method');
		}
		$d = $met['data'][0];
		if($amount < $d->min)
		{
			return array('error' => 'minimum '.$d->min);
		}
		if($amount > $d->max)
		{
			return array('error' => 'maximum '.$d->max);
		}
		if($d->type == 'BCA' || $d->type == 'Mandiri')
		{
			$current = $this->db->get_where($this->table_request,array('DAY(created_at)' => date('d'),'MONTH(created_at)' => date('m'),'YEAR(date)' => date('Y')));
			$amount += $current->num_rows();
		}
		$this->db->insert($this->table_request,array(
			'user_id' => $this->Users->id,
			'via' => $method,
			'amount' => $amount,
			'status' => '0'
		));
		$id = $this->db->insert_id();
		$this->db->trans_complete();
		return array('error' => false,'id' => $id);
	}
	public function check()
	{
		$last = $this->db->get_where($this->table_request,array(
			'user_id' => $this->Users->id,
			'status' => 0,
			'status' => 1
		));
		if($last->num_rows() != 0)
		{
			redirect(base_url('user/addfunds/'.$last->result()[0]->id));
		}
	}
	public function submit(int $topupid,string $info,string $action)
	{
		$topup = $this->db->select('topup_method.name,topup_method.type,topup_method.info,topup_requests.amount,topup_requests.info,topup_requests.status,topup_method.rate,topup_method.username,topup_method.password')
			->from('topup_requests')
			->join('topup_method','topup_method.id = topup_requests.via')
			->where('topup_requests.id',$topupid)->get();
		if($topup->num_rows() != 1)
		{
			return array('error' => 'topup not found');
		}
		if($action == 'cancel')
		{
			$this->db->where('id',$topupid)->update($this->table_request,array('status' => 3));
			$cancel =  $this->db->select('*')->from($this->table_request)->order_by('id','desc')->get();
			if($cancel->num_rows() > 2){
				$res = $cancel->result();
				if($res[0]->status == 3 && $res[1]->status == 3 && $res[2]->status == 3)
				{
					$this->Users->update($this->Users->id,array('status' => 1));
				}
			}
			return array('error' => false);
		}
		$result = $topup->result()[0];
		if($result->status == '3' || $result->status == '2')
		{
			return array('error' => 'this requests has completed');
		}
		if($result->type =='Telkomsel')
		{
			if($this->db->get_where($this->table_request,array('info' => '62'.$info,'status' => '1','amount' => $result->amount))->num_rows() != 0)
				return array('error' => 'Same request already exist');
			$this->db->where('id',$topupid)->update($this->table_request,array('status' => 1,'info' => $info));
			return array('error' => false);
		} else if($result->type == 'BCA')
		{
			$this->load->library('bca',array('username' => $result->username,'password' => $result->password));
			$Date = new DateTime(date("Y-m-d"));
	    	$Time = $Date->modify("-1 day");
	    	$Data = $this->bca->getListTransaksi($Time->format("Y-m-d"),date("Y-m-d"));
	    	$this->bca->logout();
	    	$ada = false;
	    	$list = [];
	    	for($i=0;$i<count($Data);$i++){
	    		if($Data[$i]['flows'] == 'CR'){
	    			$count = count($Data[$i]['description']);
	    			if(replace_un($Data[$i]['description'][$count-1]) == $_POST['balance']){
	    			    $list[] = $Data[$i];
	    				$ada = true;
	    			} else {
	    			    
	    			  
	    			}
	    		}
	    	}
	    	if($ada){
	    	   	$this->Topup->admin($result->id,'2');
	    	} else {
	    	    return array('error' => "transaction not found");
	    	}
	    } else if($result->type == 'Mandiri')
	    {
	    	$this->load->library('mandiri',array('username' => $result->username,'password' => $password));
	    	$this->mandiri->logout();
		    $this->mandiri->login();
		    $Data = $this->mandiri->getLastTransaction(15);
		    	for($i=0;$i<count($Data);$i++){
	    		if($Data[$i]['flows'] == 'CR'){
	    			$count = count($Data[$i]['description']);
	    			if(replace_un($Data[$i]['kredit']) == $_POST['balance']){
	    			    $list[] = $Data[$i];
	    				$ada = true;
	    				
	    			} else {
	    			    echo replace_un($Data[$i]['kredit']);
	    			}
	    		}
	    	}
	    	if($ada){
	    	   	$this->Topup->admin($result->id,'2');
	    	} else {
	    	    return array('error' => "transaction not found");
	    	}
		} else if($result->type == 'Manual')
		{
			$this->db->where('id',$topupid)->update($this->table_request,array('status' => 1,'info' => $info));
			return array('error' => false);
		}
	}
	public function admin(int $id,int $status)
	{
		$topup = $this->db->get_where($this->table_request,array('id' => $id));
		if($topup->num_rows() != 1)
		{
			return array('error' => 'Request Not Found');
		}
		$res= $topup->result()[0];
		if($res->status == '3' || $res->status == '2')
		{
			return array('error' => 'request has completed');
		}
		$this->db->trans_begin();
		if($status == '2')
		{
			$user = $this->Users->find(array('id' => $res->user_id));
			if($user['count'] != 1)
			{
				return array('error' => 'user not found');
			}
			$resb = $user['data'][0];
			$this->db->query('update users set balance = balance + ? where id = ?',[$res->amount,$res->user_id]);
			$user = $this->Users->find(array('id' => $res->user_id));
			if($user['count'] != 1)
			{
				return array('error' => 'user not found');
			}
			$resa = $user['data'][0];
			if($resa->balance <= $resb->balance)
			{
				return array('error' => 'system error');
			}
			$this->Balances->insert(
				array(
					'balance_before' => $resb->balance,
					'balance_used' => $res->amount,
					'type' => '1',
					'message' => 'Addfund #'.$id,
					'user_id' => $res->user_id,
					'balance_after' => $resa->balance
				)
			);
		}
		$this->db->where('id',$id)->update($this->table_request,array('status' => $status));
		$this->db->trans_complete();

		return array('error' => false);
	}
}