<?php 
class Apis extends CI_Model
{
	protected $table = 'apis';
	public function __construct()
	{
		parent::__construct();
		$curl = $this->curl->curl;
		$this->curl = $curl;
	}

	public function find(array $array)
	{
		$data = $this->db->get_where($this->table,$array);
		return array('data' => xss_array($data->result()) , 'count' => $data->num_rows());
	}
	public function update(string $userid,array $update)
	{
		return $this->db->where('id',$userid)->update($this->table,$update);
	}
	public function insert(array $insert)
	{
		return $this->db->insert($this->table,$insert);
	}

	public function service(int $apiId)
	{
		$api = $this->find(array('id' => $apiId));
		if($api['count'] != 1)
		{
			return array('error' => 'API Not Found');
		} 
		$builder = $this->ApiBuilder->find(array('name' => $api['data'][0]->type));
		if($builder['count'] != 1)
		{
			return array('error' => 'Builder Not Found');
		}
		if($builder['data'][0]->services == "" || $builder['data'][0]->services == null)
		{
			return array('error' => 'Not Supported');
		}
		$json = json_decode($builder['data'][0]->services);
		if(!$json)
		{
			return array('error' => 'Not Valid Builder');
		}
		$service_find = array('[key]');
		$service_replace = array($api['data'][0]->api);
		$form = (array) $json->form->form;
		foreach ($form as $key => $value) {
			$form[$key] = str_replace($service_find, $service_replace, $value);
		}
		$url = $api['data'][0]->url.str_replace($service_find, $service_replace, $json->form->url);
		$this->curl->post($url,$form);
		$res = (array) json_decode(json_encode($this->curl->response),true);
		$ser = (array) findarray($json->result->data, $res);
		$result = array();
		foreach ($ser as $v) {
			$result[] = [
				'id' => findarray($json->result->service->id, (array) $v),
				'name' => findarray($json->result->service->name,(array) $v),
				'min' => findarray($json->result->service->min,(array)(array) $v),
				'max' => findarray($json->result->service->max,(array) $v),
				'price' => findarray($json->result->service->price,(array) $v),
			];
		}
		return array('error' => false,'data' => $result);
	}
}