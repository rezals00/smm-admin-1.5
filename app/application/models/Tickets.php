<?php 
class Tickets extends CI_Model
{
	protected $table = 'tickets';
	public function __construct()
	{
		parent::__construct();
	}
	protected function info()
	{
		foreach ($this->find(array('id' => 1))['data'][0] as $key => $value) {
			$this->{$key} = $value;
		}
	}
	public function find(array $array)
	{
		$data = $this->db->get_where($this->table,$array);
		return array('data' => xss_array($data->result()) , 'count' => $data->num_rows());
	}
	public function update(string $id,array $update)
	{
		return $this->db->where('id',$id)->update($this->table,$update);
	}
	public function insert(array $insert)
	{
		return $this->db->insert($this->table,$insert);
	}
	public function reply(int $id,string $message,int $position = 0)
	{
		if(!$this->session->userdata('auth')) return array('error' => 'please sign in');
		if(strlen($message) < 3) return array('error' => 'message to short');
		$this->db->trans_begin();
		$ticket = $this->db->get_where('tickets',array('id' => $id));
		if($ticket->num_rows() != 1 )
		{
			return array('error' => 'Ticket not found');
		}
		if($position == 0)
		{
			$this->update($id,array('status' => '2'));
		} else if($position == 1)
		{
			$this->update($id,array('status' => '1'));
		}
		$this->db->insert('ticket_message',array(
			'user_id' => $this->Users->id,
			'ticket_id' => $id,
			'position' => $position,
			'message' => $message,
		));
		$this->db->trans_complete();
		return array('error' => false);
	}
}