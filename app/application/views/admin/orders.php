<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$today = $this->Orders->find(array('DAY(created_at)' => date('d'),'MONTH(created_at)' => date('m'),'YEAR(created_at)' => date('Y')));
$date = new DateTime();
$date->add(DateInterval::createFromDateString('yesterday'));
$yesterday = $this->Orders->find(array('DAY(created_at)' => $date->format('d'),'MONTH(created_at)' => $date->format('m'),'YEAR(created_at)' => $date->format('Y')));
function percent($one,$two)
{
  if($one == 0)
  {
    $percent = $two / 100;
  } else
  if($two == 0)
  {
    $percent = $one / 100;
  } else
  if($two != 0 && $one != 0)
  {
      $percent =  round( $one / $two * 100, 2 );
  }
  if($two > $one)
  {
    $n = "-";
  }  else {
    $n = "+";
  }
  return array('n' => $n,'num' =>$percent);
}
$order = percent($today['count'],$yesterday['count']);
?>
<?php $this->load->view('admin/header'); ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
  
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="info-box bg-<?=($order['n'] == '-' ? 'red' : 'green');?>">
            <span class="info-box-icon"><i class="fa fa-shopping-cart"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Order Today</span>
              <span class="info-box-number"><?=$today['count'];?></span>

              <div class="progress">
                <div class="progress-bar"></div>
              </div>
                  <span class="progress-description">
                   <?=$order['n'].number_format( $order['num'] * 100, 4 ) . '%';?> by yesterday (<?=$yesterday['count'];?>)
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
      </div>
     <div class="row">
        <div class="col-md-12">

        <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-list"></i>

              <h3 class="box-title">Orders</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <hr/>
            <br/>
            <table class="table table-responsive table-hover" id="table">
            	<thead>
            	<th>ID</th>
            	<th>Username Order/Balance</th>
            	<th>Service  Name</th>
            	<th>Target</th>
            	<th>Quantity</th>
            	<th>Charge</th>
            	<th>Status</th>
            	<th>Created At</th>
            	<th>Updated At</th>
            	</thead>
            	<tbody id="tbody">
            		
            	</tbody>
</table>
</div>
           </div>
          
           </div>

      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
   <div class="modal fade" id="edit" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit</h4>
      </div>
      <div class="modal-body">
      <form method="post" action="<?=base_url("AjaxAdmin/updateorder");?>" onsubmit="return false;">
        <center><b>User</b></center>
        <label>Username</label>
        <input type="text" name="username" id="username" class="form-control" readonly="readonly"><br>
        <label>Balance</label>
         <input type="number" name="balance" id="balance" readonly="readonly" class="form-control"><br>

      	       <center><b>Detail</b></center>
      <input type="hidden" name="ID" id="ID">
      <label>Service Name</label>
        <input type="text" name="name" id="name" readonly="readonly" class="form-control"><br>
      <label>Target</label>
          <input type="text" name="target" id="target" readonly="readonly" class="form-control"><br>
      </select>
      
       <hr/>
       <center><b>Charge</b></center>
       <br/>
       <div class="row">

       	<div class="col-lg-6">
       		<label>Quantity</label>
       		<input type="number" name="quantity" id="quantity" readonly="readonly" value="1" class="form-control">
    	 </div>  
    	 <div class="col-lg-6">
       		<label>Charge</label>
       		<input type="number" name="charge" id="charge" value="1" readonly="readonly" class="form-control">
    	 </div>  
    </div>
    <hr/>
    <center><b>Status</b></center>
    <br/>
    <div class="row">

        <div class="col-lg-6">
          <label>Start</label>
          <input type="text" name="start" id="start value="0" class="form-control">
       </div>  
       <div class="col-lg-6">
          <label>Remains</label>
          <input type="text" name="remains" id="remains" value="0" class="form-control">
       </div> 
       <div class="col-lg-12">
               <label>Status</label>

           <select class="form-control" name="status" id="status">
             <option value="Waiting">Waiting</option>
             <option value="Pending">Pending</option>
             <option value="Processing">Processing</option>
             <option value="Completed">Completed</option>
             <option value="Canceled">Canceled</option>
              <option value="Partial">Partial</option>
           </select>
       </div> 
    </div>    
    	<label>
       <br>
        <button class="btn btn-success" type="submit">Save</button>
        </form>
        <br/>
        <br/>
           <form method="post" action="<?=base_url('AjaxAdmin/refreshorder');?>" onsubmit="return false;">
            <input type="hidden" name="ID" id="IDR">
            <button type="submit"class="btn btn-success">Refresh Status</button>
          </form>
            <br/>
            <br/>
            <form method="post" action="<?=base_url('AjaxAdmin/processorder');?>" onsubmit="return false;">
                          <input type="hidden" name="ID" id="IDP">
            <button type="submit" value="" class="btn btn-info">Process Order (Waiting Only)</button>
             </form>
        <br/>
      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
  <script type="text/javascript">
  	title = "Orders";
  	tab = "order";
  	var table = $("#table").DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "<?=base_url("AjaxAdmin/listorders");?>",
        "deferRender": true,
        "responsive": true,
         dom: 'Bfrtip',
    buttons: [
        'pageLength','copy', 'excel', 'pdf'
    ]

        
    });
  	setInterval(() => {table.ajax.reload()},30000)
  	function showmodal(id)
  	{
  		$.ajax({
  			url: '<?=base_url("AjaxAdmin/info");?>'+tab,
  			type: 'POST',
  			dataType: 'json',
  			data: {ID: id},
  		})
  		.done(function(res) {
  			     $("#edit").modal();
        $("#name").val(res.name);
        $("#status").val(res.status).change();
        $("#target").val(res.target);
        $("#quantity").val(res.quantity);

        $("#ID").val(id);
        $("#IDP").val(id);
        $("#IDR").val(id);
        $("#charge").val(res.price * res.quantity);
        $("#start").val(res.start);
        $("#remains").val(res.remains);
        $("#username").val(res.username);
        $("#balance").val(res.balance);
  		})
  		.fail(function() {
          toastr.error('','Network Error');
        });
  		
  	}
  	jQuery(document).ready(function($) {
  		$('#table tbody').on('click', 'tr', function () {
	    	var data = table.row( this ).data();
	        showmodal(data[0]);

	    });
	    $("form").submit(function(event) {
        var data = $(this).serializeArray();
        if($(this).attr('action') == '<?=base_url('AjaxAdmin/delete');?>'+tab){
        if(!confirm('Are you sure do this action ?')) return null;
         }
        $.ajax({
          url: $(this).attr('action'),
          type: 'POST',
          dataType: 'json',
          data: data,
        })
        .done(function(res) {
          if(res.error === false){
            toastr.success('','Data sucessfull updated');
          } else {
            toastr.error('',res.error);
          }
        })
        .fail(function() {
          toastr.error('','Network Error');
        })
        .always(function() {
          table.ajax.reload();
        });
        
      });
  	});
  </script>
<?php $this->load->view('admin/footer'); ?>