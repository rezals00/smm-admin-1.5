<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('admin/header'); ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
  
    <!-- Main content -->
    <section class="content">
     <div class="row">
        <div class="col-md-12">

        <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-list"></i>

              <h3 class="box-title">Users</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <hr/>
            <br/>
            <table class="table table-responsive table-hover" id="table">
            	<thead>
            	<th>ID</th>
            	<th>Username</th>
              <th>Level</th>
            	<th>Balance</th>
            	<th>Balance Used</th>
            	<th>Orders Count</th>
            	<th>Register Date</th>
            	<th>Last Activity Date</th>
            	</thead>
            	<tbody id="tbody">
            		
            	</tbody>
</table>
</div>
           </div>
          
           </div>

      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
   <div class="modal fade" id="edit" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit</h4>
      </div>
      <div class="modal-body">
      <form method="post" action="<?=base_url("AjaxAdmin/updateuser");?>" onsubmit="return false;" id="update">
      <input type="hidden" name="ID" id="ID">
      <label>Username</label>
        <input type="text" name="username" id="username" class="form-control" readonly="readonly"><br>
        <label>Password</label>
        <input type="text" name="password" id="password" class="form-control"><br>
        <label>Balance</label>
        <input type="text" name="balance" id="balance" class="form-control"><br>
        <label>Balance Used</label>
        <input type="text" name="balance_used" id="balance_used" class="form-control"><br>
        <label>Level</label>
        <select class="form-control" id="level" name="level">
          <option value="1">Member</option>
          <option value="2">Reseller</option>
          <option value="0">Admin</option>
        </select>
       <hr/>
        <label>Orders Count</label>
        <input type="number" readonly="readonly" name="orders" id="orders" class="form-control"><br>
        <label>Register</label>
        <input type="text" readonly="readonly" name="created_at" id="created_at" class="form-control"><br>
        <label>Last Activity</label>
        <input type="text" readonly="readonly" name="updated_at" id="updated_at" class="form-control"><br>
       <br>
        <button class="btn btn-success" type="submit">Save</button>
        </form>
         <br/>
        	<form method="post" action="<?=base_url('AjaxAdmin/deleteuser');?>" onsubmit="return false;">
        	<input type="hidden" name="ID" id="IDD">
        	<button type="submit" class="btn btn-danger">Delete</button>
		</form>
      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
  <script type="text/javascript">
  	title = "Users";
  	var table = $("#table").DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "<?=base_url("AjaxAdmin/listuser");?>",
        "deferRender": true,
        "responsive": true,
         dom: 'Bfrtip',
    buttons: [
        'pageLength','copy', 'excel', 'pdf'
    ]

        
    });
  	setInterval(() => {table.ajax.reload()},30000)
  	function showmodal(id)
  	{
  		$.ajax({
  			url: '<?=base_url("AjaxAdmin/infouser");?>',
  			type: 'POST',
  			dataType: 'json',
  			data: {ID: id},
  		})
  		.done(function(res) {
  			$("#edit").modal();
  			$("#username").val(res.username);
  			$("#password").val(res.password);
  			$("#ID").val(id);
  			$("#IDD").val(id);
  			$("#balance").val(res.balance);
  			$("#balance_used").val(res.balance_used);
  			$("#level").val(res.level);
  			$("#orders").val(res.orders);
  			$("#created_at").val(res.created_at+" - "+res.via);
  			$("#updated_at").val(res.updated_at);
  		})
  		.fail(function() {
          toastr.error('','Network Error');
        });
  		
  	}
  	jQuery(document).ready(function($) {
  		$('#table tbody').on('click', 'tr', function () {
	    	var data = table.row( this ).data();
	        showmodal(data[0]);

	    });
	    $("form").submit(function(event) {
	    	var data = $(this).serializeArray();
	    	if($(this).attr('action') == '<?=base_url('AjaxAdmin/deleteuser');?>'){
	    	if(!confirm('Are you sure do this action ?')) return null;
	   		 }
	    	$.ajax({
	    		url: $(this).attr('action'),
	    		type: 'POST',
	    		dataType: 'json',
	    		data: data,
	    	})
	    	.done(function(res) {
          if(res.error === false){
            toastr.success('','Data sucessfull updated');
          } else {
            toastr.error('',res.error);
          }
	    	})
	    	.fail(function() {
	    		toastr.error('','Network Error');
	    	})
	    	.always(function() {
	    		$("#edit").modal('toggle');
	    		table.ajax.reload();
	    	});
	    	
	    });
  	});
  </script>
<?php $this->load->view('admin/footer'); ?>