<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('admin/header'); ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
  
    <!-- Main content -->
    <section class="content">
     <div class="row">
        <div class="col-md-12">

        <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-list"></i>

              <h3 class="box-title">Tickets</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <table class="table table-responsive table-hover" id="table">
            	<thead>
            	<th>ID</th>
            	<th>Username</th>
            	<th>Subject</th>
            	<th>Status</th>
            	<th>Created At</th>
            	<th>Updated At</th>
            	</thead>
            	<tbody id="tbody">
            		
            	</tbody>
</table>
</div>
           </div>
          
           </div>

      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script type="text/javascript">
  	title = "Tickets";
  	var table = $("#table").DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "<?=base_url("AjaxAdmin/listtickets");?>",
        "deferRender": true,

        
    });
  	setInterval(() => {table.ajax.reload()},30000)
  	jQuery(document).ready(function($) {
  		$('#table tbody').on('click', 'tr', function () {
	    	var data = table.row( this ).data();
	       window.location.assign('<?=base_url();?>admin/ticket/'+data[0]+'/reply')

	    });
  	});
  </script>

<?php $this->load->view('admin/footer'); ?>