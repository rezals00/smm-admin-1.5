<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('admin/header'); ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
  
    <!-- Main content -->
    <section class="content">
     <div class="row">
        <div class="col-md-12">

        <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-list"></i>

              <h3 class="box-title">Addfund Requests</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <hr/>
            <br/>
            <table class="table table-responsive table-hover" id="table">
            	<thead>
            	<th>ID</th>
            	<th>Username</th>
            	<th>Name</th>
            	<th>Type</th>
            	<th>Amount</th>
              <th>Status</th>
            	<th>Created At</th>
            	<th>Updated At</th>
            	</thead>
            	<tbody id="tbody">
            		
            	</tbody>
</table>
</div>
           </div>
          
           </div>

      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
   <div class="modal fade" id="edit" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit</h4>
      </div>
      <div class="modal-body">
      <form method="post" action="<?=base_url("AjaxAdmin/updateaddfundrequest");?>" onsubmit="return false;" id="update">
      <input type="hidden" name="ID" id="ID">
      <center><h3 id="type"></h3></center>
      <label>Username</label>
      <input type="text" name="username" id="username" class="form-control" readonly="readonly"><br>
      <label>Name</label>
      <input type="text" name="name" id="name" class="form-control" readonly="readonly"><br>
      <div class="row">
      <div class="col-lg-6">
      	<label>Amount</label>
      	<input type="number" name="amount" id="amount" class="form-control" value="10000" readonly="readonly">
      </div>
      <div class="col-lg-6">
      	<label>funds will get</label>
      	<input type="text" name="fund" id="fund" class="form-control" value="1" readonly="readonly">
      </div>
      </div> 
      <label>Information</label>
      <textarea class="form-control" name="info" id="info" readonly="readonly"></textarea>
      <hr/>
      <br/>
  <center><b>Status</b></center>
  <select class="form-control" id="status" name="status">
  	<option value="0">Disable</option>
  	<option value="1">Waiting  Validation</option>
    <option value="2">Completed</option>
    <option value="3">Canceled</option>
  </select>
  <br/>
        <button class="btn btn-success" type="submit">Save</button>
        </form>
         <br/>
      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
  <script type="text/javascript">
  	title = "Add Funds Method";
  	var table = $("#table").DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "<?=base_url("AjaxAdmin/listaddfundrequests");?>",
        "deferRender": true,

        
    });
  	setInterval(() => {table.ajax.reload()},30000)
  	function showmodal(id)
  	{
  		$.ajax({
  			url: '<?=base_url("AjaxAdmin/infoaddfundrequests");?>',
  			type: 'POST',
  			dataType: 'json',
  			data: {ID: id},
  		})
  		.done(function(res) {
  			$("#edit").modal();
        $("#username").val(res.username);
  			$("#name").val(res.name);
  			$("#status").val(res.status).change();
  			$("#fund").val(res.amount*res.rate).change();
  			$("#amount").val(res.amount).change();
  			$("#info").val(res.info).change();
  			$("#ID").val(id);
  			$("#IDD").val(id);
        $("#type").html(res.type)
  		})
  		.fail(function() {
          toastr.error('','Network Error');
        });
  		
  	}
  	jQuery(document).ready(function($) {
  		$('#table tbody').on('click', 'tr', function () {
	    	var data = table.row( this ).data();
	        showmodal(data[0]);

	    });
	    $("form").submit(function(event) {
        var data = $(this).serializeArray();
        if($(this).attr('action') == '<?=base_url('AjaxAdmin/deleteaddfundmethod');?>'){
        if(!confirm('Are you sure do this action ?')) return null;
         }
        $.ajax({
          url: $(this).attr('action'),
          type: 'POST',
          dataType: 'json',
          data: data,
        })
        .done(function(res) {
          if(res.error === false){
            toastr.success('','Data sucessfull updated');
          } else {
            toastr.error('',res.error);
          }
        })
        .fail(function() {
          toastr.error('','Network Error');
        })
        .always(function() {
          table.ajax.reload();
        });
        
      });
  	});
  </script>
<?php $this->load->view('admin/footer'); ?>