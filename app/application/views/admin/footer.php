 <?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
</div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Time Execute</b> {elapsed_time}
      <b>Memory Used</b>  {memory_usage}  
      <b>Version</b> <?=$this->Site->version;?>
    </div>
    <strong>Copyright &copy; <a href='<?=base_url();?>' target="_blank"><?=$this->Site->name;?> </a> & Made by Erlangga Lab.</strong> All rights
    reserved.
  </footer>

</body>

<script src="<?=base_url();?>assets/app.js"></script>
</html>
