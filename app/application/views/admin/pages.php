<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('admin/header'); ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
  
    <!-- Main content -->
    <section class="content">
     <div class="row">
        <div class="col-md-12">

        <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-list"></i>

              <h3 class="box-title">Pages</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               

            <table class="table table-responsive table-hover">
            	<thead>
            	<th>ID</th>
            	<th>Title</th>
            	<th>Status</th>
            	<th>Created At</th>
            	<th>Updated At</th>
            	</thead>
            	<tbody>
            		<?php 
            		 foreach ($this->db->get_where('pages',array())->result() as $v) { ?>
            		 	<tr link='<?=base_url('admin/page/'.$v->id.'/edit');?>'>
            		 		<td><?=$v->id;?></td>
            		 		<td><?=$v->name;?></td>
            		 		<td><?=($v->status == '1' ? 'Enable' : 'Disable');?></td>
            		 		<td><?=$v->created_at;?></td>
            		 		<td><?=$v->updated_at;?></td>
            		 	</tr>
            		 <?php } ?>
            	</tbody>
</table>
</div>
           </div>
          
           </div>

      </div>
    </section>
    <!-- /.content -->
  </div>
  <script type="text/javascript">
  	$("tr").click(function(event) {
  		window.location.assign($(this).attr('link'))
  	});
  </script>
<?php $this->load->view('admin/footer'); ?>