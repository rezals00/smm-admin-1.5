  <?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?=$this->Site->name;?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <?=$this->Site->meta;?>
   <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?=base_url();?>assets/bootstrap.min.css">
  <link rel="stylesheet" href="<?=base_url();?>assets/dataTables.bootstrap.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="<?=base_url();?>assets/AdminLTE.min.css">
  <link rel="stylesheet" href="<?=base_url();?>assets/_all-skins.min.css">
  <link rel="stylesheet" href="<?=base_url();?>assets/blue.css">
  <link rel="stylesheet" href="<?=base_url();?>assets/morris.css">
  <link rel="stylesheet" href="<?=base_url();?>assets/jquery-jvectormap-1.2.2.css">
  <link rel="stylesheet" href="<?=base_url();?>assets/datepicker3.css">
  <link rel="stylesheet" href="<?=base_url();?>assets/daterangepicker.css">
  <link rel="stylesheet" href="<?=base_url();?>assets/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="<?=base_url();?>assets/pace.min.css">
  <link rel="stylesheet" href="<?=base_url();?>assets/select2.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
  <script src="<?=base_url();?>assets/jquery-2.2.3.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
  <script src='https://www.google.com/recaptcha/api.js'></script>
  <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
  <script src="<?=base_url();?>assets/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
  <script src="<?=base_url();?>assets/morris.min.js"></script>
  <script src="<?=base_url();?>assets/bootstrap-datepicker.js"></script>
  <script src="<?=base_url();?>assets/jquery.dataTables.min.js"></script>
  <script src="<?=base_url();?>assets/dataTables.bootstrap.min.js"></script>
   <script src="<?=base_url();?>assets/jquery.flot.js"></script>
  <script src="<?=base_url();?>assets/jquery.flot.resize.js"></script>
  <script src="<?=base_url();?>assets/jquery.flot.pie.js"></script>
  <script src="<?=base_url();?>assets/jquery.flot.categories.js"></script>
  <script src="<?=base_url();?>assets/pace.min.js"></script>
  <script src="<?=base_url();?>assets/slim.min.js"></script>
  <script src="<?=base_url();?>assets/select2.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs-3.3.7/jszip-2.5.0/dt-1.10.18/af-2.3.0/b-1.5.2/b-colvis-1.5.2/b-flash-1.5.2/b-html5-1.5.2/b-print-1.5.2/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.4.0/r-2.2.2/rg-1.0.3/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.css"/>
 
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs-3.3.7/jszip-2.5.0/dt-1.10.18/af-2.3.0/b-1.5.2/b-colvis-1.5.2/b-flash-1.5.2/b-html5-1.5.2/b-print-1.5.2/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.4.0/r-2.2.2/rg-1.0.3/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.js"></script>



</head>
<style type="text/css">
  table.grid {
    width:100%;
    border:none;
    background-color:#F7F7F7;
    padding:0px;    
}
table.grid tr {
    text-align:center;
}
table.grid td {
    border:4px solid white;
    padding:6px;     
}
.height-info{
      height: 920px;
  }
@media  screen and (max-width: 780px) {

  .title-beli{
      font-size:11px;
  }
  .height-info{
      height: 500px;
  }
}
.modal-content {
  border-radius: 5px;
}
.input-group-addon,.input-group,.form-control,button,span,textarea,.input-group,i,.fa,.progress,.progress-bar {
  border-radius: 5px;
}
.progress-bar-gray {
  background-color: #39CCCC;
  color:#39CCCC;
}



</style>
<script type="text/javascript">
  base_url = '<?=base_url();?>';
</script>
<body class="hold-transition  sidebar-mini <?=($this->Site->theme == '1' ? 'skin-blue' : $this->Site->theme);?> sidebar-mini">
	<?php if($this->session->userdata('auth')){ ?>
<div class="wrapper">

   <header class="main-header" id="header">
    <!-- Logo -->
    <a href="<?=base_url();?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>S</b>MM</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b><?=$this->Site->name;?></b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <!-- Notifications: style can be found in dropdown.less -->
          <!-- Tasks: style can be found in dropdown.less -->
          <!-- User Account: style can be found in dropdown.less -->

         
          <!-- Control Sidebar Toggle Button -->
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
 <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MENU UTAMA</li>
                 <li>
          <a href="<?=base_url();?>admin">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
         <li>
          <a href="<?=base_url("admin/orders");?>">
            <i class="fa fa-shopping-cart"></i> <span>Orders</span>
          </a>
        </li>
          <li>
          <a href="<?=base_url("admin/tickets");?>">
            <i class="fa fa-ticket"></i> <span>Tickets</span>
          </a>
        </li>
         <li class="treeview">
          <a href="#">
            <i class="fa fa-list"></i> <span>Services</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url("admin/services");?>"><i class="fa fa-circle-o"></i>Services</a></li>
            <li><a href="<?=base_url("admin/categories");?>"><i class="fa fa-circle-o"></i>Categories</a></li>
            <li><a href="<?=base_url("admin/service/checker");?>"><i class="fa fa-circle-o"></i>Service Checker ( NEW )</a></li>
            <li><a href="<?=base_url("admin/service/import");?>"><i class="fa fa-circle-o"></i>Service Import</a></li>
       </ul>
     </li>
       <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i> <span>Users</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url("admin/users");?>"><i class="fa fa-circle-o"></i>Users</a></li>
            <li><a href="<?=base_url("admin/users/balances");?>"><i class="fa fa-circle-o"></i>Balances</a></li>
       </ul>
     </li>
         <li class="treeview">
          <a href="#">
            <i class="fa fa-money"></i> <span>Balances</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url("admin/addfund/request");?>"><i class="fa fa-circle-o"></i>Add Funds Request</a></li>
            <li><a href="<?=base_url("admin/addfund/method");?>"><i class="fa fa-circle-o"></i>Add funds Method</a></li>
       </ul>
     </li>
      <li>
          <a href="<?=base_url("admin/news");?>">
            <i class="fa fa-newspaper-o"></i> <span>News</span>
          </a>
        </li>
      <li class="treeview">
          <a href="#">
            <i class="fa fa-file"></i> <span>Pages</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url("admin/page/create");?>"><i class="fa fa-circle-o"></i>Create Page</a></li>
            <li><a href="<?=base_url("admin/page/list");?>"><i class="fa fa-circle-o"></i>Page List</a></li>
       </ul>
     </li>
         <li class="treeview">
          <a href="#">
            <i class="fa fa-flask"></i> <span>API</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url("admin/apis");?>"><i class="fa fa-circle-o"></i>APIs</a></li>
       </ul>
     </li>
      <li>
          <a href="<?=base_url("admin/settings");?>">
            <i class="fa fa-gear"></i> <span>Settings</span>
          </a>
        </li>


    
      
    
        <li class="header">PAGE</li>
       
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
<?php } 

