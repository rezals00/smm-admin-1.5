<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('admin/header'); ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
  
    <!-- Main content -->
    <section class="content">
     <div class="row">
        <div class="col-md-12">

        <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-list"></i>

              <h3 class="box-title">Balances</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <hr/>
            <br/>
            <table class="table table-responsive table-hover" id="table">
            	<thead>
            	<th>ID</th>
            	<th>Username</th>
              	<th>Activty</th>
              	<th>Type</th>
            	<th>Balance Before</th>
            	<th>Balance Used</th>
            	<th>Balance After</th>
            	<th>Date</th>
            	</thead>
            	<tbody id="tbody">
            		
            	</tbody>
</table>
</div>
           </div>
          
           </div>

      </div>
    </section>
    <!-- /.content -->
  </div>
  <script type="text/javascript">
  	title = "Balances";
  	var table = $("#table").DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "<?=base_url("AjaxAdmin/listbalances");?>",
        "deferRender": true,
        "responsive": true,
         dom: 'Bfrtip',
    buttons: [
        'pageLength','copy', 'excel', 'pdf'
    ]

        
    });
  	setInterval(() => {table.ajax.reload()},30000)
  </script>
<?php $this->load->view('admin/footer'); ?>