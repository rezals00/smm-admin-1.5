<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('admin/header'); ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
  
    <!-- Main content -->
    <section class="content">
     <div class="row">
        <div class="col-md-12">

        <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-list"></i>

              <h3 class="box-title">Services</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <button type="button" class="btn btn-success" onclick="$('#add').modal()">Add</button>
                <hr/>
            <br/>
            <table class="table table-responsive table-hover" id="table">
            	<thead>
            	<th>ID</th>
            	<th>API/Mode</th>
            	<th>Category Name</th>
            	<th>Name / Status</th>
            	<th>Orders Count</th>
            	<th>Min/Max</th>
            	<th>Price</th>
            	<th>Created At</th>
            	<th>Updated At</th>
            	</thead>
            	<tbody id="tbody">
            		
            	</tbody>
</table>
</div>
           </div>
          
           </div>

      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
   <div class="modal fade" id="edit" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit</h4>
      </div>
      <div class="modal-body">
      <form method="post" action="<?=base_url("AjaxAdmin/updateservice");?>" onsubmit="return false;">
      	       <center><b>Detail</b></center>
      <input type="hidden" name="ID" id="ID">

      <label>Service Name</label>
        <input type="text" name="name" id="name" class="form-control"><br>
      <label>Category</label>
      <select class="form-control" id="category" name="category">
      	<?php 
      	foreach ($this->Categories->find(array('status' => 1))['data'] as $v) { ?>
      		<option value="<?=$v->id;?>"><?=$v->name;?></option>
      	<?php } ?>
      </select>
       <br/>
       <div class="row">

       	<div class="col-lg-6">
       		<label>Minimum</label>
       		<input type="number" name="min" id="min" value="1" class="form-control">
    	 </div>  
    	 <div class="col-lg-6">
       		<label>Maximum</label>
       		<input type="number" name="max" id="max" value="1" class="form-control">
    	 </div>  
    </div>    
    	<div class="row">
      <div class="col-lg-6">
      <label>Price Per Item</label>
          <input type="text" name="price" id="price" class="form-control">
        </div>
        <div class="col-lg-6">
          <label>Profit Per Item</label>
          <input type="text" name="profit" id="profit" class="form-control">
        </div>
      </div>
       	<label>Description</label>
       	<textarea class="form-control" id="description" name="description"></textarea>
       	<hr/>
       	<div class="row">
       		<center><b>API</b></center>
       		<br/>
       		<div class="col-lg-6">
       			<label>API Provider</label>
       			<select class="form-control" id="api" name="api">
       				<option value="0">MANUAL</option>
       				<?php 
			      	foreach ($this->Apis->find(array())['data'] as $v) { ?>
			      		<option value="<?=$v->id;?>"><?=$v->name;?></option>
			      	<?php } ?>
       			</select>
       		</div>
          <div class="col-lg-3">
          <label>Mode</label>
          <select  class="form-control" name="mode" id="mode">
             <option value="Instant">Instant</option>
             <option value="Manual">Manual</option>
          </select>
       </div>  
       		<div class="col-lg-3">
       		<label>Service ID</label>
       		<input type="text" id="serviceid" name="serviceid" class="form-control" value="0" placeholder="if manual skip">
    	 </div>
       
       	</div>
        <center><b>Status</b></center>
        <select  class="form-control" name="status" id="status">
             <option value="1">Enable</option>
             <option value="0">Disable</option>
          </select>
       <br>
        <button class="btn btn-success" type="submit">Save</button>
        </form>
        <br/>
         <form method="post" action="<?=base_url('AjaxAdmin/deleteservice');?>" onsubmit="return false;">
        	<input type="hidden" name="ID" id="IDD">
        	<button type="submit" class="btn btn-danger">Delete</button>
		</form>
      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
 <div class="modal fade" id="add" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add</h4>
      </div>
      <div class="modal-body">
      <form method="post" action="<?=base_url("AjaxAdmin/addservice");?>" onsubmit="return false;">
      	       <center><b>Detail</b></center>

      <label>Service Name</label>
        <input type="text" name="name" class="form-control"><br>
      <label>Category</label>
      <select class="form-control" name="category">
      	<?php 
      	foreach ($this->Categories->find(array('status' => 1))['data'] as $v) { ?>
      		<option value="<?=$v->id;?>"><?=$v->name;?></option>
      	<?php } ?>
      </select>
       <br/>
       <div class="row">

       	<div class="col-lg-6">
       		<label>Minimum</label>
       		<input type="number" name="min" value="1" class="form-control">
    	 </div>  
    	 <div class="col-lg-6">
       		<label>Maximum</label>
       		<input type="number" name="max" value="1" class="form-control">
    	 </div>  
    </div>    
    <div class="row">
      <div class="col-lg-6">
    	<label>Price Per Item</label>
       		<input type="text" name="price" class="form-control">
        </div>
        <div class="col-lg-6">
          <label>Profit Per Item</label>
          <input type="text" name="profit" class="form-control">
        </div>
      </div>
       	<label>Description</label>
       	<textarea class="form-control" name="description"></textarea>
       	<hr/>
       	<div class="row">
       		<center><b>API</b></center>
       		<br/>
       		<div class="col-lg-6">
       			<label>API Provider</label>
       			<select class="form-control" name="api">
       				<option value="0">MANUAL</option>
       				<?php 
			      	foreach ($this->Apis->find(array())['data'] as $v) { ?>
			      		<option value="<?=$v->id;?>"><?=$v->name;?></option>
			      	<?php } ?>
       			</select>
       		</div>
       		<div class="col-lg-3">
          <label>Mode</label>
          <select  class="form-control" name="mode" id="mode">
             <option value="Instant">Instant</option>
             <option value="Manual">Manual</option>
          </select>
       </div>  
          <div class="col-lg-3">
          <label>Service ID</label>
          <input type="text" id="serviceid" name="serviceid" class="form-control" value="0" placeholder="if manual skip">
       </div>
       	</div>
       <br>
        <button class="btn btn-success" type="submit">Add</button>
        </form>

         <br/>  
      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
  <script type="text/javascript">
  	title = "Services";
  	tab = "service";
  	var table = $("#table").DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "<?=base_url("AjaxAdmin/listservices");?>",
        "deferRender": true,
        "responsive": true,
         dom: 'Bfrtip',
    buttons: [
        'pageLength','copy', 'excel', 'pdf'
    ]
        
    });
  	setInterval(() => {table.ajax.reload()},30000)
  	function showmodal(id)
  	{
  		$.ajax({
  			url: '<?=base_url("AjaxAdmin/info");?>'+tab,
  			type: 'POST',
  			dataType: 'json',
  			data: {ID: id},
  		})
  		.done(function(res) {
  			$("#edit").modal();
  			$("#name").val(res.name);
  			$("#status").val(res.status).change();
  			$("#api").val(res.api_id).change;
  			$("#category").val(res.category_id).change;

  			$("#ID").val(id);
  			$("#IDD").val(id);
  			$("#price").val(res.price);
  			$("#min").val(res.min);
  			$("#max").val(res.max);
  			$("#description").val(res.info);
  			$("#serviceid").val(res.pro_id);
        $("#mode").val(res.mode).change();
        $("#status").val(res.status).change();
        $("#profit").val(res.profit);
  		})
  		.fail(function() {
          toastr.error('','Network Error');
        });
  		
  	}
  	jQuery(document).ready(function($) {
  		$('#table tbody').on('click', 'tr', function () {
	    	var data = table.row( this ).data();
	        showmodal(data[0]);

	    });
	    $("form").submit(function(event) {
        var data = $(this).serializeArray();
        if($(this).attr('action') == '<?=base_url('AjaxAdmin/delete');?>'+tab){
        if(!confirm('Are you sure do this action ?')) return null;
         }
        $.ajax({
          url: $(this).attr('action'),
          type: 'POST',
          dataType: 'json',
          data: data,
        })
        .done(function(res) {
          if(res.error === false){
            toastr.success('','Data sucessfull updated');
          } else {
            toastr.error('',res.error);
          }
        })
        .fail(function() {
          toastr.error('','Network Error');
        })
        .always(function() {
          table.ajax.reload();
        });
        
      });
  	});
  </script>
<?php $this->load->view('admin/footer'); ?>