<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('admin/header'); ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
  
    <!-- Main content -->
    <section class="content">
     <div class="row">
        <div class="col-md-12">

        <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-list"></i>

              <h3 class="box-title">Addfund Method</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <button type="button" class="btn btn-success" onclick="$('#add').modal()">Add</button>
                <hr/>
            <br/>
            <table class="table table-responsive table-hover" id="table">
            	<thead>
            	<th>ID</th>
            	<th>Name</th>
            	<th>Total Request</th>
            	<th>Rate</th>
            	<th>Type</th>
            	<th>Min/Max</th>
            	<th>Created At</th>
            	<th>Updated At</th>
            	</thead>
            	<tbody id="tbody">
            		
            	</tbody>
</table>
</div>
           </div>
          
           </div>

      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
   <div class="modal fade" id="edit" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit</h4>
      </div>
      <div class="modal-body">
      <form method="post" action="<?=base_url("AjaxAdmin/updateaddfundmethod");?>" onsubmit="return false;" id="update">
      <input type="hidden" name="ID" id="ID">
         <label>Name</label>
      <input type="text" name="name" id="name" class="form-control"><br>
      <div class="row">
      <div class="col-lg-4">
      	<label>Min</label>
      	<input type="number" name="min" id="min" class="form-control" value="10000">
      </div>
      <div class="col-lg-4">
      	<label>Max</label>
      	<input type="number" name="max" id="max" class="form-control" value="10000000">
      </div>
      <div class="col-lg-4">
      	<label>Rate</label>
      	<input type="text" name="rate" id="rate" class="form-control" value="1">
      </div>
      </div> 
      <label>Information</label>
      <textarea class="form-control" name="info" id="info"></textarea>
      <hr/>
      <center><b>Type</b></center>
      <select class="form-control" name="type" id="type"> 
      	<option value="Manual">Manual</option>
      	<option value="Telkomsel">Telkomsel</option>
      	<option value="BCA">BCA</option>
      	<option value="Mandiri">Mandiri</option>

      </select>
      <br/>
      <div class="row">
      	<div class="col-lg-6">
      <label>Username</label>
      <input type="text" name="username" id="username" class="form-control">
  	</div>
  	  <div class="col-lg-6">
      <label>Password </label>
      <input type="text" name="password" id="password" class="form-control">
  </div>
    </div>

  <center><b>Status</b></center>
  <select class="form-control" id="status" name="status">
  	<option value="0">Disable</option>
  	<option value="1">Enable</option>
  </select>
  <br/>
        <button class="btn btn-success" type="submit">Save</button>
        </form>
         <br/>
        	<form method="post" action="<?=base_url('AjaxAdmin/deleteaddfundmethod');?>" onsubmit="return false;">
        	<input type="hidden" name="ID" id="IDD">
        	<button type="submit" class="btn btn-danger">Delete</button>
		</form>
      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
 <div class="modal fade" id="add" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add</h4>
      </div>
      <div class="modal-body">
      <form method="post" action="<?=base_url("AjaxAdmin/addaddfundmethod");?>" onsubmit="return false;">
      <label>Name</label>
      <input type="text" name="name" id="name" class="form-control"><br>
      <div class="row">
      <div class="col-lg-4">
      	<label>Min</label>
      	<input type="number" name="min" class="form-control" value="10000">
      </div>
      <div class="col-lg-4">
      	<label>Max</label>
      	<input type="number" name="max" class="form-control" value="10000000">
      </div>
      <div class="col-lg-4">
      	<label>Rate</label>
      	<input type="text" name="rate" class="form-control" value="1">
      </div>
      </div> 
      <label>Information</label>
      <textarea class="form-control" name="info"></textarea>
      <hr/>
      <center><b>Type</b></center>
      <select class="form-control" name="type">
      	<option value="Manual">Manual</option>
      	<option value="Telkomsel">Telkomsel</option>
      	<option value="BCA">BCA</option>
      	<option value="Mandiri">Mandiri</option>

      </select>
      <br/>
      <div class="row">
      	<div class="col-lg-6">
      <label>Username</label>
      <input type="text" name="username" class="form-control">
  	</div>
  	  <div class="col-lg-6">
      <label>Password </label>
      <input type="text" name="password" class="form-control">
  </div>
  </div>
      
       <br>
        <button class="btn btn-success" type="submit">Add</button>
        </form>
         <br/>  
      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
  <script type="text/javascript">
  	title = "Add Funds Method";
  	var table = $("#table").DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "<?=base_url("AjaxAdmin/listaddfundmethods");?>",
        "deferRender": true,

        
    });
  	setInterval(() => {table.ajax.reload()},30000)
  	function showmodal(id)
  	{
  		$.ajax({
  			url: '<?=base_url("AjaxAdmin/infoaddfundmethod");?>',
  			type: 'POST',
  			dataType: 'json',
  			data: {ID: id},
  		})
  		.done(function(res) {
  			$("#edit").modal();
  			$("#name").val(res.name);
  			$("#status").val(res.status).change();
  			$("#rate").val(res.rate).change();
  			$("#min").val(res.min).change();
  			$("#max").val(res.max).change();
  			$("#type").val(res.type).change();
  			$("#username").val(res.username).change();
  			$("#password").val(res.password).change();
  			$("#ID").val(id);
  			$("#IDD").val(id);
  			$("#info").val(res.info);
  		})
  		.fail(function() {
          toastr.error('','Network Error');
        });
  		
  	}
  	jQuery(document).ready(function($) {
  		$('#table tbody').on('click', 'tr', function () {
	    	var data = table.row( this ).data();
	        showmodal(data[0]);

	    });
	    $("form").submit(function(event) {
        var data = $(this).serializeArray();
        if($(this).attr('action') == '<?=base_url('AjaxAdmin/deleteaddfundmethod');?>'){
        if(!confirm('Are you sure do this action ?')) return null;
         }
        $.ajax({
          url: $(this).attr('action'),
          type: 'POST',
          dataType: 'json',
          data: data,
        })
        .done(function(res) {
          if(res.error === false){
            toastr.success('','Data sucessfull updated');
          } else {
            toastr.error('',res.error);
          }
        })
        .fail(function() {
          toastr.error('','Network Error');
        })
        .always(function() {
          table.ajax.reload();
        });
        
      });
  	});
  </script>
<?php $this->load->view('admin/footer'); ?>