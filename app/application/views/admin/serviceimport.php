<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('admin/header'); ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
  
    <!-- Main content -->
    <section class="content">
     <div class="row">
        <div class="col-md-12">

        <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-list"></i>

              <h3 class="box-title">Services Import</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form method="get">
              	<label>API</label>

              	<select class="form-control" name="api" onchange="window.location.assign( '<?=base_url('admin/service/import/?api=');?>'+$(this).val())">
              		<option value="0">SELECT API</option>
              		<?php 
              		foreach ($this->db->select('apis.name,apis.id')->from('apis')->join('api_builder','apis.type = api_builder.name')->where('api_builder.services <>','')->get()->result() as $d) { ?>
              			<option value="<?=$d->id;?>"><?=$d->name;?></option>
              	<?php
              		}
              	?>
              	</select>
              </form>
                <hr/>
            <br/>
            <table class="table table-responsive table-hover" id="table">
            	<thead>
            	<th>ID</th>
            	<th>Name</th>
            	<th>Min</th>
            	<th>Max</th>
            	<th>Price</th>
            	</thead>
            	<tbody id="tbody">
            		<?php
            		if($this->input->get('api') != null)
            		{
            			$services = $this->Apis->service($this->input->get('api'));
            			if($services['error'] === false)
            			{
            				foreach ($services['data'] as $v) {
            					echo "
            					<tr>
            					<td>{$v['id']}</td>
            					<td>{$v['name']}</td>
            					<td>{$v['min']}</td>
            					<td>{$v['max']}</td>
            					<td>{$v['price']}</td>
            					</tr>
            					";
            				}
            			} else {
            				echo "<td colspan='12'>{$services['error']}</td>";
            			}
            		} 
            		?>	
            	</tbody>
</table>
</div>
           </div>
          
           </div>

      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
   <div class="modal fade" id="edit" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit</h4>
      </div>
      <div class="modal-body">
      <form method="post" action="<?=base_url("AjaxAdmin/addservice");?>" onsubmit="return false;">
      	       <center><b>Detail</b></center>
      <input type="hidden" name="ID" id="ID">

      <label>Service Name</label>
        <input type="text" name="name" id="name" class="form-control"><br>
      <label>Category</label>
      <select class="form-control" id="category" name="category">
      	<?php 
      	foreach ($this->Categories->find(array('status' => 1))['data'] as $v) { ?>
      		<option value="<?=$v->id;?>"><?=$v->name;?></option>
      	<?php } ?>
      </select>
       <br/>
       <div class="row">

       	<div class="col-lg-6">
       		<label>Minimum</label>
       		<input type="number" name="min" id="min" value="1" class="form-control">
    	 </div>  
    	 <div class="col-lg-6">
       		<label>Maximum</label>
       		<input type="number" name="max" id="max" value="1" class="form-control">
    	 </div>  
    </div>    
    	<div class="row">
      <div class="col-lg-6">
      <label>Price Per Item</label>
          <input type="text" name="price" id="price" class="form-control">
        </div>
        <div class="col-lg-6">
          <label>Profit Per Item</label>
          <input type="text" name="profit" id="profit" class="form-control">
        </div>
      </div>
       	<label>Description</label>
       	<textarea class="form-control" id="description" name="description"></textarea>
       	<hr/>
       	<div class="row">
       		<center><b>API</b></center>
       		<br/>
       		<div class="col-lg-6">
       			<label>API Provider</label>
       			<select class="form-control" id="api" name="api">
       				<option value="0">MANUAL</option>
       				<?php 
			      	foreach ($this->Apis->find(array())['data'] as $v) { ?>
			      		<option value="<?=$v->id;?>"><?=$v->name;?></option>
			      	<?php } ?>
       			</select>
       		</div>
          <div class="col-lg-3">
          <label>Mode</label>
          <select  class="form-control" name="mode" id="mode">
             <option value="Instant">Instant</option>
             <option value="Manual">Manual</option>
          </select>
       </div>  
       		<div class="col-lg-3">
       		<label>Service ID</label>
       		<input type="text" id="serviceid" name="serviceid" class="form-control" value="0" placeholder="if manual skip">
    	 </div>
       
       	</div>
        <center><b>Status</b></center>
        <select  class="form-control" name="status" id="status">
             <option value="1">Enable</option>
             <option value="0">Disable</option>
          </select>
       <br>
        <button class="btn btn-success" type="submit">Save</button>
        </form>
        <br/>
      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
  <script type="text/javascript">
  	title = "Services Import";
  	var table = $("#table").DataTable({
        "deferRender": true,
        "responsive": true,
         dom: 'Bfrtip',
    buttons: [
        'pageLength','copy', 'excel', 'pdf'
    ]
        
    });
    jQuery(document).ready(function($) {
    	$('#table tbody').on('click', 'tr', function () {
	    	var data = table.row( this ).data();
	        $("#edit").modal('toggle')
	        $("#price").val(data[4])
	        $("#min").val(data[2])
	        $("#max").val(data[3])
	        $("#serviceid").val(data[0])
	        $("#name").val(data[1])
	            	$("#api").val('<?=$this->input->get('api');?>').change();


	    });
	    $("form").submit(function(event) {
	        var data = $(this).serializeArray();
	        $.ajax({
	          url: $(this).attr('action'),
	          type: 'POST',
	          dataType: 'json',
	          data: data,
	        })
	        .done(function(res) {
	          if(res.error === false){
	            toastr.success('','Data sucessfull updated');
	          } else {
	            toastr.error('',res.error);
	          }
	        })
	        .fail(function() {
	          toastr.error('','Network Error');
	        });
	        
      });
    });
 
  </script>
<?php $this->load->view('admin/footer'); ?>