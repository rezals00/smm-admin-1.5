<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('admin/header'); ?>
  <div class="content-wrapper">
    <section class="content">
     <div class="row">
        <div class="col-md-12">

        <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-gear"></i>

              <h3 class="box-title">Setting Website</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <?=$this->session->flashdata('messages');?>

          <?=form_open('', '','');?>
           <table class="table table-bordered">
                            <td colspan="2"><center><b>Website Config</b></center></td>
             <tr>
               <td>Name</td>
               <td><input type="text" name="name" class='form-control' value="<?=$this->Site->name;?>">
             </tr>
              <tr>
               <td>Header</td>
               <td><textarea style="height: 400px;" type="text" name="meta" class='form-control' ><?=$this->Site->meta;?></textarea>
             </tr>
             <tr>
              <td>Theme</td>
              <td><select class="form-control" name="theme" id="theme">
                <option value="skin-blue">Blue</option>
                <option value="skin-blue-light">Blue Light</option>
                <option value="skin-red">Red</option>
                <option value="skin-red-light">Red Light</option>
                <option value="skin-yellow">Yellow</option>
                <option value="skin-yellow-light">Yellow Light</option>
                <option value="skin-green">Green</option>
                <option value="skin-green-light">Green Light</option>
                <option value="skin-purple">Purle</option>
                <option value="skin-purple-light">Purple Light</option>
              </select></td>
  
             <tr>
              <tr>
                <td>Register</td>
                <td><select class="form-control" name="register" id="register"><option value="0">Disable</option><option value="1">Enable</option></select></td>
              </tr>
               <tr>
                <td>MODE</td>
                <td><select class="form-control" name="neworder" id="neworder"><option value="0">Manual</option><option value="1">Instant</option></select></td>
              </tr>
              <tr>
                <td>USER VERIF</td>
                <td><select class="form-control" name="user_must_verif" id="user_must_verif">
                  <option value="0">Not Required</option>
                  <option value="1">Required</option>
                </select></td>
              </tr>
              <tr>
                <td>MAINTANCE</td>
                <td><select class="form-control" name="is_maintance" id="is_maintance">
                  <option value="0">No</option>
                  <option value="1">Yes</option>
                </select></td>
              </tr>
              <tr>
                <td colspan="2"><center><b>Email Config</b> <small>( required for some feature )</center></td>
              </tr>
               <tr>
                <td>SMTP HOST</td>
                <td><input type="text" name="smtp_host" class="form-control" value="<?=$this->Site->smtp_host;?>"></td>
              </tr>
               <tr>
                <td>SMTP USERNAME</td>
                <td><input type="text" name="smtp_username" class="form-control"  value="<?=$this->Site->smtp_username;?>"></td>
              </tr>
               <tr>
                <td>SMTP PASSWORD</td>
                <td><input type="text" name="smtp_password" class="form-control"  value="<?=$this->Site->smtp_password;?>"></td>
              </tr>
               <tr>
                <td colspan="2"><center><b>Recaptcha</b> <small>( leave blank to disable )</small></center></td>
              </tr>
              <tr>
                <td>PUBLIC KEY</td>
                <td><input type="text" name="recaptcha_public" class="form-control" value="<?=$this->Site->recaptcha_public;?>"></td>
              </tr>
               <tr>
                <td>SECRET KEY</td>
                <td><input type="text" name="recaptcha_secret" class="form-control"  value="<?=$this->Site->recaptcha_secret;?>"></td>
              </tr>
               <td colspan="2"><button type="submit" class="btn btn-block btn-primary">Save</button></td>
             </tr>
           </table>
           <?=form_close();?>

           </div>
           </div>
           </div>

      </div>
                         
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script type="text/javascript">
  	$(document).ready(function() {
         $("#theme").val("<?=$this->Site->theme;?>").change();
         $("#register").val("<?=$this->Site->register;?>").change();
         $("#neworder").val("<?=$this->Site->neworder;?>").change();
         $("#is_maintance").val("<?=$this->Site->is_maintance;?>").change();
         $("#user_must_verif").val("<?=$this->Site->user_must_verif;?>").change();
  	});
  </script>
<?php $this->load->view('admin/footer'); ?>