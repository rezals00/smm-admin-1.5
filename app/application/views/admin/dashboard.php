<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$date = new DateTime();
$date->add(DateInterval::createFromDateString('first day of previous month'));

function percent($one,$two)
{
  if($one == 0)
  {
    $percent = $two / 100;
  }
  if($two == 0)
  {
    $percent = $one / 100;
  }
  if($two != 0 && $one != 0)
  {
      $percent =  $one / $two;
  }
  if($two > $one)
  {
    $n = "-";
  }  else {
    $n = "+";
  }
  return array('n' => $n,'num' =>$percent);
}
$ordertoday = $this->Orders->find(array('MONTH(created_at)' => date('n'),'YEAR(created_at)' => date('Y')));
$orderlast = $this->Orders->find(array('MONTH(created_at)' => $date->format('n'),'YEAR(created_at)' => $date->format('Y')));
$order = percent($ordertoday['count'],$orderlast['count']);
$usertoday = $this->Users->find(array('MONTH(created_at)' => date('m'),'YEAR(created_at)' => date('Y')));
$userlast = $this->Users->find(array('MONTH(created_at)' => $date->format('m'),'YEAR(created_at)' => $date->format('Y')));
$user = percent($usertoday['count'],$userlast['count']);
$topuptoday = $this->db->get_where('topup_requests',array('MONTH(created_at)' => date('m'),'YEAR(created_at)' => date('Y'),'status' => 2));
$totaltopuptoday = 0;
foreach ($topuptoday->result() as $v) {
  $totaltopuptoday += $v->amount;
}
$topuplast = $this->db->get_where('topup_requests',array('MONTH(created_at)' => date('m'),'YEAR(created_at)' => date('Y'),'status' => 2));
$totaltopuplast = 0;
foreach ($topuplast->result() as $v) {
  $totaltopuplast += $v->amount;
}
$topup = percent($totaltopuptoday,$totaltopuplast);
$ordertodaycompleted = $this->db->select('orders.quantity,services.price')->from('orders')->join('services','services.id = orders.service_id')->where('MONTH(orders.created_at)', date('m'))->where('YEAR(orders.created_at)',date('Y'))->where('orders.status','Completed')->get();
$totalorder = 0;
foreach ($ordertodaycompleted->result() as $v) {
  $totalorder += $v->quantity * $v->price;
}
function thousandsCurrencyFormat($num) {

  if($num>1000) {

        $x = round($num);
        $x_number_format = number_format($x);
        $x_array = explode(',', $x_number_format);
        $x_parts = array('k', 'm', 'b', 't');
        $x_count_parts = count($x_array) - 1;
        $x_display = $x;
        $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
        $x_display .= $x_parts[$x_count_parts - 1];

        return $x_display;

  }

  return $num;
}
?>
<?php $this->load->view('admin/header'); ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
  
    <!-- Main content --> 
    <section class="content">
     <div class="row">
                       
        <div class="col-md-8">
           <p class="text-center">
                    <strong><?=date('Y');?></strong>
                  </p>
        <div class="box box-default">
            <div class="box-header with-border">

              <h3 class="box-title">Monthly Data</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-md-12">


                  <div class="chart-responsive">
                    <!-- Sales Chart Canvas -->
                    <div id="salesChart" style="height: 210px;"></div>
                  </div>
                  <!-- /.chart-responsive -->
                </div>
     
           
</div>
  
              <!-- /.row -->
            </div>
            <!-- /.box-footer -->
           </div>
           <div class='col-lg-4'>
             <p class="text-center">
                    <strong><?=date('M/Y');?></strong>
                  </p>
            <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="fa fa-shopping-cart"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Orders</span>
              <span class="info-box-number"><?=$ordertoday['count'];?>  ( Rp.<?=thousandsCurrencyFormat($totalorder);?> )</span>

              <div class="progress">
                <div class="progress-bar" style="width: 50%"></div>
              </div>
              <span class="progress-description">
                    <?=$order['n'].number_format( $order['num'] * 100, 2 ) . '%';?> by last month 
                  </span>
            </div>
            <!-- /.info-box-content -->
          <!-- /.info-box -->
           </div>
            <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-money"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Topup</span>
              <span class="info-box-number"><?=$topuptoday->num_rows();?> ( Rp.<?=thousandsCurrencyFormat($totaltopuptoday);?> )</span>

              <div class="progress">
                <div class="progress-bar" style="width: 50%"></div>
              </div>
              <span class="progress-description">
                    <?=$topup['n'].number_format( $topup['num'] * 100, 2 ) . '%';?> by last month
                  </span>
            </div>
            <!-- /.info-box-content -->
          <!-- /.info-box -->
           </div>
           <div class="info-box bg-blue">
            <span class="info-box-icon"><i class="fa fa-users"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">USERS</span>
              <span class="info-box-number"><?=$usertoday['count'];?></span>

              <div class="progress">
                <div class="progress-bar" style="width: 50%"></div>
              </div>
              <span class="progress-description">
                    <?=$user['n'].number_format( $user['num'] * 100, 2 ) . '%';?> by last month
                  </span>
            </div>
            <!-- /.info-box-content -->
          <!-- /.info-box -->
           </div>
          
           </div>

      </div>
      <?php 
$alltopup = $this->db->select('topup_requests.amount,topup_requests.status,topup_method.rate')->from('topup_requests')->join('topup_method','topup_method.id = topup_requests.via')->get();
$totaltopup = array();
foreach ($alltopup->result() as $v) {
  if(!array_key_exists($v->status, $totaltopup)){
    $totaltopup[$v->status] = array('total' => 0,'money' => 0);
  }
  $totaltopup[$v->status]['total'] += 1;
  $totaltopup[$v->status]['money'] += $v->amount * $v->rate;
}
$order = $this->db->select('orders.quantity,services.price,orders.status')->from('orders')->join('services','services.id = orders.service_id')->get();

$totalorder = array();
foreach ($order->result() as $v) {
  if(!array_key_exists($v->status, $totalorder)){
    $totalorder[$v->status] = array('total' => 0,'money' => 0);
  }
  $totalorder[$v->status]['total'] += 1;
  $totalorder[$v->status]['money'] += $v->quantity * $v->price;
}
?>
      <div class="row">
                  <p class="text-center">
                    <strong>Total</strong>
                  </p>         
                  <div class='col-lg-6'>
            <div class="box">
            <div class="box-header">
              <h3 class="box-title">Orders</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tr>
                  <th>Status</th>
                  <th>Total</th>
                  <th>Rp.</th>
                </tr>
                <?php
                foreach ($totalorder as $key => $v) {
                  ?>
                  <tr>
                    <td><?=$key;?></td>
                    <td><?=thousandsCurrencyFormat($v['total']);?></td>
                    <td>Rp. <?=thousandsCurrencyFormat($v['money']);?></td>
                  </tr>
                <?php }
                ?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
      </div>
        <div class='col-lg-6'>
            <div class="box">
            <div class="box-header">
              <h3 class="box-title">Topup</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tr>
                  <th>Status</th>
                  <th>Total</th>
                  <th>Rp.</th>
                </tr>
                <?php
                foreach ($totaltopup as $key => $v) {
                  $status = $key;
                  ?>
                  <tr>
                    <td><?php if($status == '0'){ echo '<p class="label label-default">Pending</p>'; } else if($status == '1'){ echo '<p class="label label-info">Waiting Confirmation</p>'; } else if($status == '3'){ echo '<p class="label label-danger">Canceled</p>'; } else { echo '<p class="label label-success">Completed</p>'; } ?></td>
                    <td><?=thousandsCurrencyFormat($v['total']);?></td>
                    <td>Rp. <?=thousandsCurrencyFormat($v['money']);?></td>
                  </tr>
                <?php }
                ?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
      </div>
    </div>
    </section>
    <!-- /.content -->
  </div>
  <script type="text/javascript">
    title = "Dashboard";
    $(document).ready(function() {
  <?php
  $years = date('Y');
  $pembelian = [];
  $array = array(
    'January',
    'Febuary','March','April' , 'May' , 'June' , 'July' ,'August' ,'September' ,'October','November' ,'December');
  for($i=1;$i <= 12 ;$i++){
     foreach($this->db->query("SELECT count(*) as date FROM orders WHERE MONTH(created_at) = '$i'  AND YEAR(created_at) = '$years' AND status = 'Completed'")->result() as $c);
     foreach($this->db->query("SELECT count(*) as date FROM orders WHERE MONTH(created_at) = '$i'  AND YEAR(created_at) = '$years' AND status = 'Canceled'")->result() as $ca);
     foreach($this->db->query("SELECT count(*) as date FROM orders WHERE MONTH(created_at) = '$i'  AND YEAR(created_at) = '$years' AND status = 'Processing'")->result() as $p);
     foreach($this->db->query("SELECT count(*) as date FROM orders WHERE MONTH(created_at) = '$i'  AND YEAR(created_at) = '$years' AND status = 'Pending'")->result() as $pe);
     foreach($this->db->query("SELECT count(*) as date FROM orders WHERE MONTH(created_at) = '$i'  AND YEAR(created_at) = '$years' AND status = 'Partial'")->result() as $pa);
     $pembelian[] = array('y' => $array[$i-1],'success' => $c->date,'pending' => $pe->date,'proses'=> $p->date,'cancel' => $ca->date,'partial' => $pa->date);
  } 
  
   ?>
    
    var data = <?=json_encode($pembelian);?>;
    $("#salesChart").empty();
    var line = new Morris.Line({
      parseTime:false,
      element: 'salesChart',
      resize: true,
      data: data,
      xkey: 'y',
      ykeys: ['success','pending','proses','cancel','partial'],
      labels: ['Completed','Pending','Processing','Cancel','Partial'],
      lineColors: ['#aaa','#bbb','#ccc','#ddd','#eee'],
      hideHover: 'auto'
    });

    

});
  </script>
<?php $this->load->view('admin/footer'); ?>