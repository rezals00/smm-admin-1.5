<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('admin/header'); ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
  
    <!-- Main content -->
    <section class="content">
     <div class="row">
        <div class="col-md-12">

        <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-list"></i>

              <h3 class="box-title">Edit Page # <?=$id;?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            	              <?=$this->session->flashdata('messages');?>
            	              <?=form_open('', '','');?>
            <label>Page Title</label>
            <input type="text" name="title" id="title_text" class="form-control" value="<?=$name;?>">

            <label>Page Body <small>[ see style on <a target="_blank" href='https://adminlte.io'>adminlte.io</a> ] </small></label>
            <textarea class="form-control" id="body_text" name="body" rows="10"><?=$html?></textarea>
            <br/>
            <button type="submit" class="btn btn-success">Save</button>
            <?=form_close();?>
            
</div>
           </div>
          
           </div>
            <div class="col-md-12">

        <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-list"></i>

              <h3 class="box-title" id="title_page"><?=$name;?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body" id="body_page">
            <?=$html;?>
            
</div>
           </div>
          
           </div>

      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
   
  <script type="text/javascript">
  	title = "Edit Page";
  	jQuery(document).ready(function($) {
  		$("#title_text").keyup(function(event) {
  			$("#title_page").html($(this).val())
  		});
  		$("#body_text").keyup(function(event) {
  			$("#body_page").html($(this).val())
  		});
  	});
  </script>
<?php $this->load->view('admin/footer'); ?>