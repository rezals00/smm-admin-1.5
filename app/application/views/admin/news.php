<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('admin/header'); ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
  
    <!-- Main content -->
    <section class="content">
     <div class="row">
        <div class="col-md-12">

        <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-list"></i>

              <h3 class="box-title">News</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <button type="button" class="btn btn-success" onclick="$('#add').modal()">Add</button>

                <hr/>
            <br/>
            <table class="table table-responsive table-hover" id="table">
            	<thead>
            	<th>ID</th>
            	<th>Section</th>
            	<th>Message</th>
            	<th>Created At</th>
            	<th>Updated At</th>
            	</thead>
            	<tbody id="tbody">
            		
            	</tbody>
</table>
</div>
           </div>
          
           </div>

      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
   <div class="modal fade" id="edit" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit</h4>
      </div>
      <div class="modal-body">
      <form method="post" action="<?=base_url("AjaxAdmin/updatenews");?>" onsubmit="return false;" id="update">
      <input type="hidden" name="ID" id="ID">
       <label>Section</label>
     <select class="form-control" name="section" id="section">
     	<option value="Announcement">Announcement</option>
     	<option value="Service">Service</option>
     </select>
        <label>Message</label>
        <textarea class="form-control" id="message" rows="5" name="message"></textarea>
       <hr/>
      
       <hr/>
        <label>Created At</label>
        <input type="text" readonly="readonly" name="created_at" id="created_at" class="form-control"><br>
        <label>Updated At</label>
        <input type="text" readonly="readonly" name="updated_at" id="updated_at" class="form-control"><br>
       <br>
        <button class="btn btn-success" type="submit">Save</button>
        </form>
         <br/>
        	<form method="post" action="<?=base_url('AjaxAdmin/deletenews');?>" onsubmit="return false;">
        	<input type="hidden" name="ID" id="IDD">
        	<button type="submit" class="btn btn-danger">Delete</button>
		</form>
      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
 <div class="modal fade" id="add" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add</h4>
      </div>
      <div class="modal-body">
      <form method="post" action="<?=base_url("AjaxAdmin/addnews");?>" onsubmit="return false;">
      <label>Section</label>
     <select class="form-control" name="section">
     	<option value="Announcement">Announcement</option>
     	<option value="Service">Service</option>
     </select>
        <label>Message</label>
        <textarea class="form-control" rows="5" name="message"></textarea>
       <hr/>
      
       <br>
        <button class="btn btn-success" type="submit">Add</button>
        </form>
         <br/>  
      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
  <script type="text/javascript">
  	title = "News";
  	table = "news";
  	var table = $("#table").DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "<?=base_url("AjaxAdmin/listnews");?>",
        "deferRender": true,

        
    });
  	setInterval(() => {table.ajax.reload()},30000)
  	function showmodal(id)
  	{
  		$.ajax({
  			url: '<?=base_url("AjaxAdmin/info");?>'+table,
  			type: 'POST',
  			dataType: 'json',
  			data: {ID: id},
  		})
  		.done(function(res) {
  			$("#edit").modal();
  			$("#name").val(res.name);
  			$("#status").val(res.status).change;
  			$("#ID").val(id);
  			$("#IDD").val(id);
  			$("#services").val(res.services);
  			$("#created_at").val(res.created_at);
  			$("#updated_at").val(res.updated_at);
  		})
  		.fail(function() {
          toastr.error('','Network Error');
        });
  		
  	}
  	jQuery(document).ready(function($) {
  		$('#table tbody').on('click', 'tr', function () {
	    	var data = table.row( this ).data();
	        showmodal(data[0]);

	    });
	    $("form").submit(function(event) {
        var data = $(this).serializeArray();
        if($(this).attr('action') == '<?=base_url('AjaxAdmin/delete');?>'+table){
        if(!confirm('Are you sure do this action ?')) return null;
         }
        $.ajax({
          url: $(this).attr('action'),
          type: 'POST',
          dataType: 'json',
          data: data,
        })
        .done(function(res) {
          if(res.error === false){
            toastr.success('','Data sucessfull updated');
          } else {
            toastr.error('',res.error);
          }
        })
        .fail(function() {
          toastr.error('','Network Error');
        })
        .always(function() {
          table.ajax.reload();
        });
        
      });
  	});
  </script>

<?php $this->load->view('admin/footer'); ?>