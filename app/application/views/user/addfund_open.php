<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('user/header'); ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
   

    <!-- Main content -->
    <section class="content">
     <div class="row">
     	<div class="col-lg-3"></div>
        <div class="col-md-6">
          <div class="alert alert-danger">
            if you cancel 3x add fund your account will get banned.
          </div>
          <?php
              if($type == 'Telkomsel') { ?>
          <div class="alert alert-danger">
            BEFORE TRANSFER PLEASE SUBMIT YOUR PHONE NUMBER.
          </div>
        <?php } ?>
        <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-money"></i>

              <h3 class="box-title">Add Funds</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                                                      <?=$this->session->flashdata('messages');?>

                  <?=form_open('', '','');?>      

  <table class="table table-bordered">
            <tr>
            <td>Method</td><td> <b><?=$name;?></b> </td>
            </tr>

            <tr><td>Amount</td> <td><?=$amount;?></td> </tr>
            <tr><td>funds will get</td>  <td><?=$rate*$amount;?></td> </tr>
            <tr><td>Status</td> <td><?php if($status == '0'){ echo '<p class="label label-default">Pending</p>'; } else if($status == '1'){ echo '<p class="label label-info">Waiting Confirmation</p>'; } else if($status == '3'){ echo '<p class="label label-danger">Canceled</p>'; } else { echo '<p class="label label-success">Completed</p>'; } ?></td> </tr>
            <tr><td>Instruction</td><td> Please Transfer To <?=$name;?> With amount <?=$amount;?>  And Submit </td></tr>
            <tr>
              <?php
              if($type == 'Telkomsel') { ?>
           <td colspan="2"> <div class="input-group col-lg-12">
                <span class="input-group-addon">62</span>
                <input type="text" class="form-control" name="info" placeholder="822123341323" value="<?=$info;?>">
              </div></td>
             <?php } else if($type == 'Manual') { ?>
           <td colspan="2"> <textarea name="info" class="form-control" placeholder="Information for admin" <?=($status == '2' ? 'disabled' : null);?>><?=$info;?></textarea> </td>
           <?php } ?>
           </tr>
           <tr>
            <?php 
            if($status == '2' || $status == '3') { ?>
                            <td colspan="2"><a href="<?=base_url("user/addfunds");?>" class="btn btn-info btn-block">Back</a></td>

              
            <?php }  else { ?>
           <td colspan="2"><?=($status == '2' ? '<center><b>Completed</b></center>' : '<button name="action" value="submit" type="submit" class="btn btn-success btn-block">SUBMIT</button>');?></button></td></tr>
               <?=form_close()?>      
          <td colspan="2"> <?=form_open('', '','');?> <?=($status == '2' ? '<center><b>Completed</b></center>' : '<button type="submit" class="btn btn-danger btn-block" name="action" value="cancel">Cancel</button>');?></button> <?=form_close()?>   </td></tr>
        <?php } ?>
            </table>
          </div>
            
           </div>
           </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script>
              function spl(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}
title = "Add Fund <?=$id;?>";
  </script>
<?php $this->load->view('user/footer'); ?>