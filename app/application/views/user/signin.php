<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('user/header'); ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
  
    <!-- Main content -->
    <section class="content">
     <div class="row">
     	<div class="col-md-3"></div>
        <div class="col-md-6">

        <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-sign-in"></i>

              <h3 class="box-title">Sign In</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                                      <?=$this->session->flashdata('messages');?>

<?=form_open('', '','');?>      
<div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Username" name="username" value="<?=html_escape($this->session->flashdata('username'));?>" required="required">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="password"  required="required">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <?php             if($this->Site->recaptcha_public != '' && $this->Site->recaptcha_secret != '' && $this->session->userdata('invalid_attemp') > 2) { ?>
      <div class="g-recaptcha" data-sitekey="<?=$this->Site->recaptcha_public;?>"></div>
    <?php } ?>
      <div class="row">
        <div class="col-xs-8">
          
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block">Sign in</button>
        </div>
        <!-- /.col -->
      </div>
    <?=form_close();?>
    <div class="margin-top-30 text-center">
    	<p>Need Account ? <a href="<?=base_url();?>auth/signup" class="text-info m-l-5">Create Account</a></p> <br/>
      <p>Forgot Password ? <a href="<?=base_url();?>auth/forgot" class="text-info m-l-5">Forgot Password</a></p>
    </div>
           </div>
             
           </div>
           </div>

      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
   <script type="text/javascript">
    title = "Sign in";
   </script>
<?php $this->load->view('user/footer'); ?>