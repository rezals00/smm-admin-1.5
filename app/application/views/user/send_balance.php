<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if($this->Users->level == "1"){
	redirect(base_url());
  exit;
}
?>
<?php $this->load->view('user/header'); ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
   
    <!-- Main content -->
    <section class="content">
     <div class="row">
        <div class="col-md-12">

        <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-money"></i>

              <h3 class="box-title">Send Balance</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                          <?=$this->session->flashdata('messages');?>

    <?=form_open('', '','');?>      
            	<label>Username</label>
            <input type="text" name="username" class="form-control" required="required"> <br>
            <label>Amount</label>
            <input type="number" name="balance" class="form-control"  required="required"> <br>
  
            
            <button  type="submit" class="btn btn-success btn-block">Submit</button>
    <?=form_close();?>
           
           </div>
           </div>

      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    <script type="text/javascript">
        title = "Send Balance";
  </script>
<?php $this->load->view('user/footer'); ?>