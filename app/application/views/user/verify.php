<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('user/header'); ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
   

    <!-- Main content -->
    <section class="content">
     <div class="row">
     	<div class="col-lg-3"></div>
        <div class="col-md-6">
          <div class="alert alert-info">
            you must verify your account to access other feature.
          </div>

        <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-lock"></i>

              <h3 class="box-title">Verify</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                                        <?=$this->session->flashdata('messages');?>

                  <?=form_open('', '','');?>
                  <label>Email</label>
                  <input type="email" name="email" id="email" class="form-control" value="<?=$this->Users->email;?>"> 
                  <label>Code</label>
                   <div class="input-group">
                    
                    <input type="text" class="form-control" name="code">
                     <div class="input-group-btn">
                      <button type="button" class="btn btn-success" id="send">SEND</button>
                    </div>
                  </div>
                  <br/>
                  <button type="submit" class="btn btn-success">SUBMIT</button>
                  <?=form_close();?>
          </div>
            
           </div>
           </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script type="text/javascript">
    t = 0;
    title = "Verify Account";
    function tick()
    {
      t--;
      if(t <= 0)
      {
        $("#send").removeAttr('disabled');
        $("#send").html('SEND')
      } else 
      {
        $("#send").attr('disabled','disabled');
        $("#send").html(t);
        setTimeout(tick, 1000)
      }
    }
    $("#send").click(function(event) {
       $.ajax({
         url: '<?=base_url('ajax/send_email');?>',
         type: 'POST',
         dataType: 'json',
         data: {email: $("#email").val()},
       })
       .done(function(res) {
          if(res.error === false){
            toastr.success('','we have send an email,dont forget to check spam');
            t = 60;
            tick();
          } else {
            toastr.error('',res.error);
          }
       })
       .fail(function() {
            toastr.error('','network error');
       });
       
    });
  </script>
<?php $this->load->view('user/footer'); ?>