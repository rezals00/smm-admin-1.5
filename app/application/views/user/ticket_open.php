<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$find = array('
	'); 
$change = array('<br>'); 
?>
<?php $this->load->view('user/header'); ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <section class="content">
  <div class="col-md-12">
 <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-ticket"></i>

              <h3 class="box-title">#
                            <?=$id;?> |
                                <?=xss_clean($subject);?></h3>
          </div>
                      <div class="box-body">
                                                              <?=$this->session->flashdata('messages');?>

                        <div class="chat" style="overflow-y: auto; height: 400px;">

                            <?php foreach($this->db->query("SELECT * FROM ticket_message WHERE ticket_id = ? ORDER BY id DESC",[$id])->result() as $data){ ?>
                            <div class="item">
                                        <small class="text-muted pull-right"><?=($data->position == '0' ? $this->Users->username : 'Support');?> <?=$data->created_at;?></small>
                                    <br/>
<pre><?=xss_clean($data->message);?>
</pre>
                                <!-- /.attachment -->
                            </div>
                            <?php } ?>
                            <!-- /.item -->
                        </div>
                        <?php if($status == '3') { ?>
                          <hr/>
                          <center><h3>Closed</h3></center>
                          <?php } else { ?>
                          <?=form_open('', '',"");?>
                            <div class="box-footer">

                              <input type="hidden" name="id" value="<?=$id;?>">
                              <label>Message</label>
                            <textarea class="form-control" rows="4" name="message"></textarea>
                            <br/>
                                <button class="btn btn-success" type="submit"><i class="fa fa-paper-plane"></i> SUBMIT
                                        </button>
                                <br>
                            </div>
                            <?=form_close();?>
                        <?php } ?>
                    </div>
                </div>     
                     
    </section>
</div>
    <!-- /.content -->
    <script type="text/javascript">
    	title = "<?=$subject;?>";
    </script>
<?php $this->load->view('user/footer'); ?>