<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('user/header'); ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    

    <!-- Main content -->
    <section class="content">
     <div class="row">
      <div class="col-md-12">
      
     </div>
        <div class="col-md-12">

        <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-shopping-bag"></i>

              <h3 class="box-title">Top 5 Orders</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <?php 
            $month = date('m');
            $years = date('Y');
            $pembelian = [];
            $isisaldo = [];
            $i = 0;
            foreach ($this->Users->find(array('level <>' => '0'))['data'] as $result) {
              $data = $this->db
              ->select('orders.quantity,services.price')
              ->from('orders')
              ->join('services','services.id = orders.service_id')
              ->where(array('MONTH(orders.created_at)' => $month,'YEAR(orders.created_at)' => $years,'orders.user_id' => $result->id))
              ->order_by('orders.id','DESC')->get();
              $total = 0;
              foreach ($data->result() as $re) {
                $total += $re->price * $re->quantity;
              }
              $pembelian[] = array('username' => $result->username,'total' => $total);
              $data = $this->db
              ->select('topup_requests.amount,topup_method.rate')
              ->from('topup_requests')
              ->join('topup_method','topup_method.id = topup_requests.via')
              ->where(array('MONTH(topup_requests.created_at)' => $month,'YEAR(topup_requests.created_at)' => $years,'topup_requests.user_id' => $result->id,'topup_requests.status' => 2))
              ->order_by('topup_requests.id','DESC')->get();
              $total = 0;
              foreach ($data->result() as $re) {
                $total += $re->amount;
              }
              $isisaldo[] = array('username' => $result->username,'total' => $total);
              $i++;
            }
    
            function superSort ($array) {
              $newArray = $array;
          for ($i=0; $i < count($array)-1; $i++) { 
            $rank = count($array);
            for ($a=0; $a < count($array); $a++) { 
              if($array[$i]['total'] >= $array[$a]['total']){
                $rank -= 1;
              }
            }
            
            $newArray[$rank] = $array[$i];
          }
          return $newArray;
      }
      function sooort($a,$b){
        return($a['total'] <= $b['total']) ? 1 : -1;
      }
      usort($isisaldo, "sooort");
      usort($pembelian, "sooort");

            ?>
            <table class="table table-striped">
              <thead>
                <th>RANK</th>
                <th>Username</th>
                <th>Total</th>
              </thead>
              <?php 
              $now = 0;
              for($i = 0;$i < count($pembelian)-1;$i++){
                if($i >= 5){
                  break;
                }
                echo "<tr><td>". ($i + 1) ."</td><td>{$pembelian[$i]['username']}</td><td>Rp.".number_format($pembelian[$i]['total'])."</td></tr>";

              }
              ?>
            </table>
          
             
           </div>
          
           </div>
           

      </div>
              <div class="col-md-12">

      <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-money"></i>

              <h3 class="box-title">Top 5 Topup</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               <table class="table table-striped">
              <thead>
                <th>RANK</th>
                <th>Username</th>
                <th>Total</th>
              </thead>
              <?php 
              $now = 0;
              for($i = 0;$i < count($isisaldo)-1;$i++){
                if($i >= 5){
                  break;
                }
                echo "<tr><td>". ($i + 1) ."</td><td>{$isisaldo[$i]['username']}</td><td>".number_format($isisaldo[$i]['total'])."</td></tr>";

              }
              ?>
            </table>
             
           </div>
          
           </div>
           </div>

      </div>
    </section>
    <!-- /.content -->
  </div>
  <script type="text/javascript">
        title = "Member Of The month";
  </script>
<?php $this->load->view('user/footer'); ?>