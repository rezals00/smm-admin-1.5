<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('user/header'); ?>
<?php 
function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? '' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'recently';
}
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
     <div class="row">
        <div class="col-md-12">
          <?php 
          if($this->Users->balance < 5000){ ?>
           <div class="callout callout-danger ">
          
        <h4>Alert !</h4>

        <p>Your Balance Less Than 5000 , Topup For Remove this Notification.</p>
      </div>
    <?php } ?>
          <div class="box box-widget">
            <div class="box-body">  
           <h4 style="margin-bottom: 0px;margin-top: 0px;" class="pull-left text-primary"><span class="fa fa-money" style="margin-right: 6px;"></span> Rp <?=number_format($this->Users->balance);?></h4>
                    <a href="<?=base_url();?>balance" class="btn-loading label label-primary pull-right" style="font-size: 13px;padding-bottom: 5px;padding-top: 5px;"><i class="fa fa-plus-circle" style="margin-right: 3px;"></i> Topup</a>
           </div>
          </div>

          <div class="box box-widget">
            <div class="box-body">  
                <div class="table-responsive">
                        <table class="grid" cellspacing="0">
                             
                            <tr>
                                    <td style="width:33.3%;height:70px;">
                                            <a href="<?=base_url();?>user/order" class="btn-loading">
                                                <i class="fa fa-cart-plus fa-2x"></i><br>
                                                <small class="title-beli">Order</small>
                                            </a>
                                        </td>
                                   <td style="width:33.3%;height:70px;">
                                            <a href="<?=base_url();?>user/balance" class="btn-loading">
                                                <i class="fa fa-money fa-2x"></i><br>
                                                <small class="title-beli">Topup</small>
                                            </a>
                                        </td>
                                   <td style="width:33.3%;height:70px;">
                                            <a href="<?=base_url();?>user/ticket" class="btn-loading">
                                                <i class="fa fa-ticket fa-2x"></i><br>
                                                <small class="title-beli">Ticket</small>
                                            </a>
                                        </td>
                          </tr>
                         
                             
                           
                                                    </table>
                    </div>
           </div>
          </div>
            <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-shopping-cart"></i>

              <h3 class="box-title">Latest 7 Days Orders Chart</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body chart-responsive">
           <div class="chart" id="pembelian" style="height: 300px;"></div>
           </div>
           </div>
           </div>
           <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <i class="fa fa-newspaper-o"></i>

              <h3 class="box-title">News & Information</h3>

             
            </div>
            <div class="box-body">  
               <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Announcement</a></li>
              <li><a href="#tab_2" data-toggle="tab">Service</a></li>
              <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
                <?php
                foreach ($this->db->select('*')->from('news')->where('section','Announcement')->limit(5,0)->order_by('id','desc')->get()->result() as $v) {
                  ?>
                   <blockquote>
                <p><?=$v->message;?></p>
                <small><?=$v->created_at;?></small>
              </blockquote>
                <?php }?>
               
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                <?php
                foreach ($this->db->select('*')->from('news')->where('section','Service')->limit(5,0)->order_by('id','desc')->get()->result() as $v) {
                  ?>
                   <blockquote>
                <p><?=$v->message;?></p>
                <small><?=$v->created_at;?></small>
              </blockquote>
                <?php }?>
              </div>
              <!-- /.tab-pane -->
    
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
           </div>
           </div>

      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    <script type="text/javascript">
      title = "Dashboard";
$(document).ready(function() {
  <?php
  $years = date('Y');
  $month = date('m');
  $day = date('d');
  $pembelian = [];
  for($i=6;$i > -1 ;$i--){
     $now = $day-$i;
     foreach($this->db->query("SELECT count(*) as date FROM orders WHERE DAY(created_at) = '$now'  AND YEAR(created_at) = '$years' AND MONTH(created_at) = '$month' AND user_id = '{$this->Users->id}'")->result() as $c);
     $pembelian[] = array('y' => $now."/".$month,'order' => $c->date);
  } 
  
   ?>
    
    var data = <?=json_encode($pembelian);?>;
    $("#pembelian").empty();
    var line = new Morris.Line({
      parseTime:false,
      element: 'pembelian',
      resize: true,
      data: data,
      xkey: 'y',
      ykeys: ['order'],
      labels: ["Order"],
      lineColors: ['#aaa'],
      hideHover: 'auto',
      behaveLikeLine: true,
    ymin:'auto'
    });

    

});
  </script>
<?php $this->load->view('user/footer'); ?>