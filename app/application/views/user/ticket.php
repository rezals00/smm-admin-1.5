<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('user/header'); ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
   

    <!-- Main content -->
    <section class="content">
     <div class="row">
        <div class="col-md-12">
 <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-ticket"></i>

              <h3 class="box-title">Open Ticket</h3>
          </div>
                      <div class="box-body">	

                          <?=$this->session->flashdata('messages');?>

                             <?=form_open('', '',"");?>
      <label>Subject</label>
        <input type="text" name="subject" value="<?=html_escape($this->session->flashdata('subject'));?>" class="form-control" placeholder="Subject" required="required"><br>
        <label>Message</label>
        <textarea rows="5" class="form-control" value="<?=html_escape($this->session->flashdata('message'));?>" name="message" placeholder="Message..."></textarea> <br>
        <button class="btn btn-success">Open</button>
        <?=form_close();?>
          
           </div>
             
           </div>
           </div>
      </div>
        <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-ticket"></i>

              <h3 class="box-title">Ticket List</h3>
            </div>
            <div class="box-body">	
            <table class="table table-borderan">
            	<thead>
            	<th>ID</th>
            	<th>Subject</th>
            	<th>Status</th>
            	</thead>
            	<tbody>
            		<?php 
            		foreach($this->db->query("SELECT * FROM tickets WHERE user_id = ? ORDER BY id DESC",[$this->Users->id])->result() as $result){ ?>
            		<tr> 
            		<td><?=$result->id;?></td>
            		<td><a href='<?=base_url("user/ticket/{$result->id}");?>'><?=strip_tags($result->subject);?></a></td>
            		<td>
                <?php if($result->status == '0'){ echo '<p class="label label-success">Created</p>'; } else if($result->status == '1'){ echo '<p class="label label-success">Answered</p>'; } else if($result->status == '2'){ echo '<p class="label label-info">Asked</p>'; } else if($result->status == "3") { echo '<p class="label label-danger">Closed</p>'; } ?></td>  
                </td>
              </tr>
                 <?php 
            		}
            		?>
            	</tbody>
            </table>
          
           </div>
             
           </div>
           </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
   <script type="text/javascript">
   	title = "Tickets";
   </script>
<?php $this->load->view('user/footer'); ?>