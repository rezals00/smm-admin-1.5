<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('user/header'); ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
   

    <!-- Main content -->
    <section class="content">
     <div class="row">
     	<div class="col-lg-3"></div>
        <div class="col-md-6">
 
        <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-lock"></i>

              <h3 class="box-title">Forgot Password</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                                        <?=$this->session->flashdata('messages');?>
                  <?=form_open('', '','');?>
                  <label>Verification Code</label>
                  <input type="number" name="code" id="code" class="form-control" value=""> 
                  <hr/>
                  <label>NEW Password</label>
                  <input type="text" name="password" id="password" class="form-control" value=""> 
                  <br/>
                  <button type="submit" class="btn btn-success btn-block">SUBMIT</button>
                  <?=form_close();?>
              
          </div>
            
           </div>
           </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    <script type="text/javascript">
        title = "Forgot Password";
  </script>
<?php $this->load->view('user/footer'); ?>