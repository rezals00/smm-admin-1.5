<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('user/header'); ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
 

    <!-- Main content -->
    <section class="content">
     <div class="row">
     	<div class="col-md-3"></div>
        <div class="col-lg-6">

        <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-sign-in"></i>

              <h3 class="box-title">Sign Up</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <?php 
              if($this->Site->register == 0)
              { ?>
                <center><h3>Currently Register Is Closed</h3></center>
              <?php } else { ?>


               <?=$this->session->flashdata('messages');?>
          <?=form_open('', '','');?>
           <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="Email" value="<?=html_escape($this->session->flashdata('email'));?>" name="email" required="required">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
          <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Username" value="<?=html_escape($this->session->flashdata('username'));?>" name="username" required="required">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-lg-6">
            <div class="form-group has-feedback">
              <input type="password" class="form-control" placeholder="Password" name="password" required="required">
              <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group has-feedback">
              <input type="password" class="form-control" placeholder="Retype password" name="passwordconf" required="required">
              <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
        </div>
      </div>
        <?php             if($this->Site->recaptcha_public != '' && $this->Site->recaptcha_secret != '') { ?>
      <div class="g-recaptcha" data-sitekey="<?=$this->Site->recaptcha_public;?>"></div>
    <?php } ?>
      <div class="row">
        <div class="col-xs-8">
          
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block">Sign Up</button>
        </div>
        <!-- /.col -->
      </div>
     <?=form_close();?>
    <div class="margin-top-30 text-center">
    	<p>Have account ? <a href="<?=base_url();?>auth/signin" class="text-info m-l-5">Signin</a></p>
    </div>
           </div>
             <?php } ?>
           </div>
           </div>

      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
   <script type="text/javascript">
    title = "Sign Up";
   </script>
<?php $this->load->view('user/footer'); ?>