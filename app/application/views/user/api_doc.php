<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('user/header'); ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
  

    <!-- Main content -->
    <section class="content">
     <div class="row">
        <div class="col-md-12">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs pull-right">
              <li><a href="#status" data-toggle="tab">Status</a></li>
             <li class="active"><a href="#order" data-toggle="tab">Order</a></li>
              <li class="pull-left header"><i class="fa fa-flask"></i> API Documentation</li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="order">
                <label>URL</label>
                <input type="text" class="form-control" value="<?=base_url('api/order');?>" readonly>
                <label>METHOD</label>
                <input type="text" class="form-control" value="POST" readonly>
                <hr/>
                <strong>Body</strong>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <td>Name</td>
                      <td>Description</td>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>key</td>
                      <td>API Key. <a href="<?=base_url('user/profile');?>" target="_blank">see here</a></td>
                    </tr>
                     <tr>
                      <td>service</td>
                      <td>Service ID. <a href="<?=base_url('services');?>" target="_blank">see here</a></td>
                    </tr>
                     <tr>
                      <td>target</td>
                      <td>Target.</td>
                    </tr>
                     <tr>
                      <td>quantity</td>
                      <td>Quantity.</td>
                    </tr>
                  </tbody>
                </table>
                <hr/>
                <strong>Response Success</strong> <br/>
                <code>{"error":false,"order":"27","service":"MANUAL","charge":100}</code>
                <hr/>
                <strong>Response Error</strong> <br/>
                <code>{"error":"service not found"}</code>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="status">
               <label>URL</label>
                <input type="text" class="form-control" value="<?=base_url('api/status');?>" readonly>
                <label>METHOD</label>
                <input type="text" class="form-control" value="POST" readonly>
                <hr/>
                <strong>Body</strong>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <td>Name</td>
                      <td>Description</td>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>key</td>
                      <td>API Key. <a href="<?=base_url('user/profile');?>" target="_blank">see here</a></td>
                    </tr>
                     <tr>
                      <td>order</td>
                      <td>Order ID. <a href="<?=base_url('user/history/orders');?>" target="_blank">see here</a></td>
                    </tr>
                  </tbody>
                </table>
                <hr/>
                <strong>Response Success</strong> <br/>
                <code>{"error":false,"status":"Pending","start":"0","remains":"0"}</code>
                <br/>
                Status : <strong>Pending,Processing,Completed,Canceled,Partial</strong>
                <hr/>
                <strong>Response Error</strong> <br/>
                <code>{"error":"order not found"}</code>
              </div>
              <!-- /.tab-pane -->
             
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
       
          </div>
           </div>

           </div>

      </div>
    </section>
    <!-- /.content -->
  <!-- /.content-wrapper -->
  <script type="text/javascript">
    title = "API Documentation"
    $("#type").change(function(event) {
        var vl = $(this).val();
        if(vl == 0){
            $("#default").show();
            $("#cc").hide();
            $("#ccp").hide();
            $("#mcl").hide();
            $("#muf").hide();
        } else if(vl == 1) {
            $("#default").hide();
            $("#cc").show();
            $("#ccp").hide();
            $("#mcl").hide();
            $("#muf").hide();
        } else if(vl == 2){
           $("#default").hide();
            $("#cc").hide();
            $("#ccp").hide();
            $("#mcl").show();
            $("#muf").hide();
        } else if(vl == 3){
           $("#default").hide();
            $("#cc").hide();
            $("#ccp").hide();
            $("#mcl").hide();
            $("#muf").show();
        } else if(vl == 4){
            $("#default").hide();
            $("#cc").hide();
            $("#ccp").show();
            $("#mcl").hide();
            $("#muf").hide();
          }
    });
  </script>
<?php $this->load->view('user/footer'); ?>