<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?=$this->Site->name;?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <?=$this->Site->meta;?>
   <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?=base_url();?>assets/bootstrap.min.css">
  <link rel="stylesheet" href="<?=base_url();?>assets/dataTables.bootstrap.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="<?=base_url();?>assets/AdminLTE.min.css">
  <link rel="stylesheet" href="<?=base_url();?>assets/_all-skins.min.css">
  <link rel="stylesheet" href="<?=base_url();?>assets/blue.css">
  <link rel="stylesheet" href="<?=base_url();?>assets/morris.css">
  <link rel="stylesheet" href="<?=base_url();?>assets/jquery-jvectormap-1.2.2.css">
  <link rel="stylesheet" href="<?=base_url();?>assets/datepicker3.css">
  <link rel="stylesheet" href="<?=base_url();?>assets/daterangepicker.css">
  <link rel="stylesheet" href="<?=base_url();?>assets/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="<?=base_url();?>assets/pace.min.css">
  <link rel="stylesheet" href="<?=base_url();?>assets/select2.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
  <script src="<?=base_url();?>assets/jquery-2.2.3.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
  <script src='https://www.google.com/recaptcha/api.js'></script>
  <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
  <script src="<?=base_url();?>assets/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
  <script src="<?=base_url();?>assets/morris.min.js"></script>
  <script src="<?=base_url();?>assets/bootstrap-datepicker.js"></script>
  <script src="<?=base_url();?>assets/jquery.dataTables.min.js"></script>
  <script src="<?=base_url();?>assets/dataTables.bootstrap.min.js"></script>
  <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
  <script src="<?=base_url();?>assets/jquery.flot.js"></script>
  <script src="<?=base_url();?>assets/jquery.flot.resize.js"></script>
  <script src="<?=base_url();?>assets/jquery.flot.pie.js"></script>
  <script src="<?=base_url();?>assets/jquery.flot.categories.js"></script>
  <script src="<?=base_url();?>assets/pace.min.js"></script>
  <script src="<?=base_url();?>assets/slim.min.js"></script>
  <script src="<?=base_url();?>assets/select2.min.js"></script>


</head>
<style type="text/css">
  table.grid {
    width:100%;
    border:none;
    background-color:#F7F7F7;
    padding:0px;    
}
table.grid tr {
    text-align:center;
}
table.grid td {
    border:4px solid white;
    padding:6px;     
}
.height-info{
      height: 920px;
  }
@media  screen and (max-width: 780px) {

  .title-beli{
      font-size:11px;
  }
  .height-info{
      height: 500px;
  }
}
.modal-content {
  border-radius: 5px;
}
.input-group-addon,.input-group,.form-control,button,span,textarea,.input-group,i,.fa,.progress,.progress-bar {
  border-radius: 5px;
}
.progress-bar-gray {
  background-color: #39CCCC;
  color:#39CCCC;
}



</style>
<script type="text/javascript">
  base_url = '<?=base_url();?>';
</script>
<body class="hold-transition  sidebar-mini <?=($this->Site->theme == '1' ? 'skin-blue' : $this->Site->theme);?> sidebar-mini">
	<?php if($this->session->userdata('auth')){ ?>
	 <?php 
if($this->Users->level == "0"){
  $role = "Admin";
} else if($this->Users->level == "1"){
  $role = "Member";
} else if($this->Users->level == "2"){
  $role = "Reseller";
}
            ?>
            <?php
if($this->Users->status != 2 && $this->Site->user_must_verif == 1 && $this->Site->smtp_host != '' && current_url() != base_url('user/verify'))
{
  redirect('user/verify');
} 
?>
<div class="wrapper">

   <header class="main-header" id="header">
    <!-- Logo -->
    <a href="<?=base_url();?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>S</b>MM</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b><?=$this->Site->name;?></b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <!-- Notifications: style can be found in dropdown.less -->
          <!-- Tasks: style can be found in dropdown.less -->
          <!-- User Account: style can be found in dropdown.less -->

          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?=base_url();?>assets/avatar.png" class="user-image" alt="User Image">
              <span class="hidden-xs"><?=$this->Users->username;?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?=base_url();?>assets/avatar.png" class="img-circle" alt="User Image">

                <p>
                  <?=$this->Users->username;?> - <?=$role;?>
                  <small>Rp.<?=number_format($this->Users->balance);?></small>
                </p>
              </li>
              <!-- Menu Body -->
              <!-- Menu Footer-->
              <li class="user-footer">
<div class="pull-left">
                  <a href="<?=base_url();?>user/profile" class="btn btn-default btn-flat open">Profile</a>
                  </div>
                <div class="pull-right">
                  <a href="<?=base_url("auth/signout");?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
 <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MENU UTAMA</li>
                 <li>
          <a href="<?=base_url();?>user/dashboard">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li>
          <a href="<?=base_url("user/motm");?>">
            <i class="fa fa-fire"></i> <span>MOTM</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-shopping-cart"></i> <span>Order</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
                   <li><a href="<?=base_url();?>user/order"><i class="fa fa-circle-o"></i> Single Order</a></li>
                   <li><a href="<?=base_url();?>user/multiorder"><i class="fa fa-circle-o"></i> Multi Order</a></li>
          </ul>
        </li>
          <li class="treeview">
          <a href="#">
            <i class="fa fa-list"></i> <span>History</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
                   <li><a href="<?=base_url("user/history/orders");?>"><i class="fa fa-circle-o"></i>Orders</a></li>
                   <li><a href="<?=base_url("user/history/balances");?>"><i class="fa fa-circle-o"></i>Balances</a></li>
          </ul>
        </li>
         <li>
          <a href="<?=base_url("user/addfunds");?>">
            <i class="fa fa-money"></i> <span>Add Funds</span>
           
          </a>
        </li>
         <li>
          <a href="<?=base_url("services");?>">
            <i class="fa fa-shopping-bag"></i> <span>Services</span>
          </a>
        </li>
           

       <li>
          <a href="<?=base_url("user/ticket");?>">
            <i class="fa fa-ticket"></i> <span>Ticket</span>
           
          </a>
        </li>
      
      
         <li>
          <a href="<?=base_url("api/doc");?>">
            <i class="fa fa-flask"></i> <span>API Documentation</span>
           
          </a>
        </li>
      
        <?php 
        if($this->Users->level != '1'){ ?>
        <li class="header">Reseller Feature</li>
         <li>
         <a href="<?=base_url();?>user/create_user">
            <i class="fa fa-user-plus"></i> <span>Create User</span>
           
          </a>
        </li>
         <li>
           <a href="<?=base_url();?>user/send_balance">
            <i class="fa fa-money"></i> <span>Send Balance</span>
           
          </a>
           <li>
        <?php }
        ?>
        <?php 
        if($this->Users->level == '0'){ ?>
        <li class="header">Admin Feature</li>
         <li>
         <a href="<?=base_url();?>admin">
            <i class="fa fa-user-plus"></i> <span>Admin Page</span>
           
          </a>
        </li>
         
        <?php }
        ?>
        </li>
        <li class="header">PAGE</li>
        <?php 
        foreach ($this->db->get_where('pages',array('status' => 1))->result() as $v) {
          ?>
           <li>
<<<<<<< HEAD
         <a href="<?=base_url('page?id='.$v->id);?>">
=======
         <a href="<?=base_url('pages/'.$v->id);?>">
>>>>>>> 5f22bf57b58558ce378746383a0aa20003cfcef1
            <i class="fa fa-file"></i> <span><?=$v->name;?></span>
           
          </a>
        </li>
        <?php }
        ?>
       
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
<?php } else { ?>
<div class="wrapper">

   <header class="main-header">
    <!-- Logo -->
    <a href="<?=base_url();?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>S</b>MM</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b><?=$this->Site->name;?></b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <!-- Notifications: style can be found in dropdown.less -->
          <!-- Tasks: style can be found in dropdown.less -->
          <!-- User Account: style can be found in dropdown.less -->
          
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
 <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MENU UTAMA</li>
                 <li>
          <a href="<?=base_url();?>auth/signin">
            <i class="fa fa-sign-in"></i> <span>Sign In</span>
          </a>
        </li>
         <li>
          <a href="<?=base_url();?>auth/signup">
            <i class="fa fa-user-plus"></i> <span>Sign Up</span>
          </a>
        </li>
         <li>
          <a href="<?=base_url();?>services">
            <i class="fa fa-shopping-bag"></i> <span>Services</span>
          </a>
        </li>
        </li>
        <li>
          <a href="<?=base_url();?>api/doc">
            <i class="fa fa-flask"></i> <span>API Documentation</span>
           
          </a>
        </li>
        <li class="header">PAGE</li>
        <?php 
        foreach ($this->db->get_where('pages',array('status' => 1))->result() as $v) {
          ?>
           <li>
<<<<<<< HEAD
         <a href="<?=base_url('page?id='.$v->id);?>">
=======
         <a href="<?=base_url('pages/'.$v->id);?>">
>>>>>>> 5f22bf57b58558ce378746383a0aa20003cfcef1
            <i class="fa fa-file"></i> <span><?=$v->name;?></span>
           
          </a>
        </li>
        <?php }
        ?>
        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
  
 <?php } ?>

