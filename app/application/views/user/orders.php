<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('user/header'); ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
    <!-- Main content -->
    <section class="content">
     <div class="row">
        <div class="col-md-12">

        <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-list"></i>

              <h3 class="box-title">Orders</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
               
            <form method="get">
                <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-search"></i></span>
              <input type="text" class="form-control" name="q" placeholder="Search.." value="<?=$this->input->get('q',true);?>">
            </div>
            </form>
            <br>
            <table class="table table-bordered" id="History">
            	<thead>
                  <tr>
            	<th style="width:10%;">ID</th>
            	<th style="width:20%;">Service</th>
            	<th style="width:20%;">Target</th>
            	<th style="width:10%;">Quantity</th>
            	<th style="width:10%;">Status</th>
            	<th style="width:10%;">Start / Remains</th>
              <th style="width:10%;">Charge</th>
              <th style="width:10%;">VIA</th>
            	<th style="width:10%;">Date Time</th>
                  </tr>
            	</thead>
            	<tbody>
            		<?php 
                $limit_start = 0;
                $count = $this->Orders->countOrders($this->input->get('q',true));
                $pe = ceil($count / 25);
                $limit_end = 25;
                if(isset($_GET['p'])){

                   $page = (int)$this->input->get('p',true);
                   if($page < 0 || $page > $pe){
                      $limit_start = 0;
                      $limit_end = 25;
                   } else {
                      $limit_end = $page * 25;
                      $limit_start = ($page - 1) * 25;
                   }
                } 
                $Historys =  $this->Orders->listOrders($this->input->get('q',true),$limit_start,$limit_end);
                if(count($Historys) == 0){
                   echo "<tr><td colspan='10'><center>No Order Found</center></td></tr>";
                }
            		foreach($Historys as $result){ ?>
            		<tr> 
            		<td><?=$result->id;?></td>
            		<td ><?=$result->name;?></td>
            		<td><input class='form-control' style='width:200px' value='<?=$result->target;?>' disabled></td>
            		<td><?=$result->quantity;?></td>
            		<td><?=$result->status;?></td>
            		<td><?=($result->start == null ? "0" : $result->start);?> / <?=($result->remains == null ? "0" : $result->remains);?></td>
                <td><?="Rp.".number_format($result->price*$result->quantity);?></td>
                <td><?=$result->via;?></td>                
            		<td><?=$result->created_at;?></td>
        
                
                 <?php 
            		}
            		?>
            	</tbody>
            </table>
            <ul class="pagination">
  <?php
  function builder($array = array()){
     if(isset($array)){
        $_get = Array();
        foreach ($array as $name => $value) {
            $_get[] = $name.'='.urlencode($value);
        }
        $join = join("&",$_get);
        return  current_url()."?{$join}";
     }
     return "";
  }
  for($i = 1;$i < $pe+1;$i++){
    $get = $this->input->get();
    $get['p'] = $i;
    $url = builder($get);
    if($this->input->get('p',true) == $i){
       echo '<li class="active"><a href="javascript:void(0)">'.$i.'</a></li>';
    } else if(!$this->input->get('p') && $i == 1){
       echo '<li class="active"><a href="javascript:void(0)">'.$i.'</a></li>';
    } else {
      echo '<li><a href="'.$url.'">'.$i.'</a></li>';
    }

  }
  ?>
</ul>
           </div>
             
           </div>
           </div>

      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    <script type="text/javascript">
        title = "Orders";
  </script>
<?php $this->load->view('user/footer'); ?>