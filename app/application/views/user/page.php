<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('user/header'); ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$name;?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?=$name;?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     <div class="row">
        <div class="col-md-12">

        <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-file"></i>

              <h3 class="box-title"><?=$name;?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
           <?=$html;?>
           </div>
           </div>
           </div>

      </div>
    </section>
    <!-- /.content -->
  </div>
  <script type="text/javascript">title = "<?=$name;?>";</script>
  <!-- /.content-wrapper -->
<?php $this->load->view('user/footer'); ?>