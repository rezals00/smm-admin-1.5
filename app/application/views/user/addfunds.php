<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('user/header'); ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
     <div class="row">
     	<div class="col-md-3"></div>
        <div class="col-md-6">

        <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-money"></i>

              <h3 class="box-title">Add Funds</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            	                          <?=$this->session->flashdata('messages');?>

                  <?=form_open('', '','');?>    
            <label>Type</label>
            <select class="form-control" id="type">
              <option>Select Type</option>
               <option>Manual</option>
               <option>Telkomsel</option>
               <option>BCA</option>
               <option>Mandiri</option>
            </select>  
            <label>Method</label>
            <select class="form-control" id="method" name="method">
            <option value="0">Select Type</option>
           
            </select>
            <div class="row">
            <div class="col-md-6">
            <label>Min</label>
            <input type="text" id="min" class="form-control" placeholder="Select Method" readonly="readonly"> <br>
            </div>
             <div class="col-md-6">
            <label>Max</label>
            <input type="text" id="max" class="form-control" placeholder="Select Method" readonly="readonly"> <br>
            </div>
           
            </div>
            <center><b>Detail</b></center>
            <label>Amount</label>
            <input type="number" id="amount" name="amount" class="form-control" min="0" placeholder="10.000" readonly="readonly" required="required"> <br>
            <div class="row">
             <div class="col-md-8">
            <label>Balance Will get</label>
            <input type="text" id="balance" class="form-control" placeholder="Select Method" readonly="readonly"> <br>
            </div>
            <div class="col-md-4">
            <label>Rate</label>
            <input type="number" id="rate" class="form-control" placeholder="Select Method" readonly="readonly"> <br>
            </div>
        </div>
            <div id="telkomsel" style="display: none;">
            </div>
            <button type="submit" class="btn btn-success btn-block">SUBMIT</button>
            <?=form_close();?>
          </div>
            
           </div>
           </div>
    </section>
    <!-- /.content -->
  </div>
  
  <!-- /.content-wrapper -->
  <?php 
  $list = array();
  $list = $this->db->select('info,max,min,rate,id,name,type')->from('topup_method')->where('status',1)->get()->result();

  ?>
  <script>
      title = "Add Fund";
	var method_list = <?=json_encode($list);?>;
	var rate = 0;
	 jQuery(document).ready(function($) {
    $("#type").change(function(event) {
      $("#method").html('');
      $("#method").append('<option value="0">Select Method</option>');
      for (var i = method_list.length - 1; i >= 0; i--) {
        if(method_list[i].type == $(this).val())
        {
          $("#method").append('<option value="'+method_list[i].id+'">'+method_list[i].name+'</option>')
        }
      }
    });
	 	$("#method").change(function(event) {
	 		for (var i = method_list.length - 1; i >= 0; i--) {
	 			if(method_list[i].id == $(this).val())
	 			{
	 				$("#min").val(method_list[i].min)
	 				$("#max").val(method_list[i].max)
	 				$("#info").val(method_list[i].info)
	 				$("#rate").val(method_list[i].rate)
	 				$("#balance").val("0")
	 				rate = method_list[i].rate
	 				$("#amount").removeAttr('readonly')
	 			}
	 		}
	 	})
	 	$("#amount").keyup(function(event) {
	 		$("#balance").val(rate * $(this).val())
	 	})
	 })
  </script>
<?php $this->load->view('user/footer'); ?>