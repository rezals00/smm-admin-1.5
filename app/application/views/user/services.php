<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('user/header'); ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
   

    <!-- Main content -->
    <section class="content">
     <div class="row">
        <div class="col-md-12">

        <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-shopping-bag"></i>

              <h3 class="box-title">Services</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <select class="form-control">
                  <?php
                  $categories= $this->Categories->find(array('status' => 1))['data'];
                  foreach ($categories as $category) {
                     echo "<option value='category_id{$category->id}'>{$category->name}</option>";
                  }
                  ?>
                </select>
                <br/>
                <br/>
            <table class="table table-bordered table-hover">
            	   <thead>
              <th>Service ID</th>
              <th>NAME</th>
              <th>MIN</th>
              <th>MAX</th>
              <th>PRICE / MIN ITEM</th>
              </thead>
              <tbody>
               <?php 
              
                foreach($categories as $category){ 
                  echo "<tr id='category_id{$category->id}'><td colspan='12'><center>{$category->name}</center></td></tr>";
                  $services = $this->Services->find(array('status' => '1','category_id' => $category->id))['data'];
                  foreach ($services as $result) { ?>
                     <tr>
                <td><?=$result->id;?></td>
                <td><?=$result->name;?></td>
                <td><?=$result->min;?></td>
                <td><?=$result->min;?></td>
                <td>Rp.<?=number_format($result->price * $result->min);?></td>
                </tr>
                 <?php }
                  ?>
               
                <?php } ?>
              </tbody>
            </table>

           </div>
          
           </div>
           </div>

      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script type="text/javascript"> $('.select2').select2()
  title = "Services";
  $("select").change(function(event) {
      document.getElementById($(this).val()).scrollIntoView(true);
      return true;
  });
</script>
<?php $this->load->view('user/footer'); ?>