<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('user/header'); ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
  

    <!-- Main content -->
    <section class="content">
     <div class="row">
        <div class="col-md-12">

        <div class="box box-default">
            <div class="box-header with-border">
               <i class="fa fa-gear">	</i>

              <h3 class="box-title">Settings</h3>

            </div>
            <div class="box-body">
<?=$this->session->flashdata('messages');?>
                <div class="form-horizontal">
                <?=form_open('', '', '');?>
                  <div class="form-group">
                    <label  class="col-sm-2 control-label">Current Password</label>

                    <div class="col-sm-10">
                      <input type="password" class="form-control" name="passwordold">
                    </div>
                  </div>
                  <hr/>
                  <div class="form-group">
                    <label class="col-sm-2 control-label">New Password</label>

                    <div class="col-sm-10">
                      <input type="password" class="form-control" name="passwordnew" placeholder="" id="pw1">
                    </div>
                  </div>
                  <div class="form-group">
                    <label  class="col-sm-2 control-label">Retype New Password</label>

                    <div class="col-sm-10">
                      <input type="password" class="form-control" name="passwordnew1" placeholder="" id="pw2">
                    </div>
                  </div>
                  <button type="submit" class="btn btn-success">Save</button>
                  <?=form_close();;?>
                  <hr>
              </div>
                  <div class="form-group">
                  	<label>Your API KEY</label>
                      <input type="text" class="form-control" value="<?=$this->Users->token;?>" readonly>
                  </div>
                  <?=form_open('user/generateapikey', '', '');?><br>
                      	<button type="submit" class="btn btn-primary btn-small">Change</button>
                      <?=form_close();?>
           </div>
           </div>
           </div>

      </div>
  </div>
    </section>
    <!-- /.content -->
  </div>
   <script type="text/javascript">
    title = "Profile";
   </script>

<?php $this->load->view('user/footer'); ?>