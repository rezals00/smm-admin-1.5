<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('user/header'); ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">
     <div class="row">
      
          <div class="col-md-12">

        <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-info"></i>

              <h3 class="box-title">Information</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <p>
                1. we can't cancel order <br>
                2. check data ( link ,service , quantity ) again before submit 

              </p>  
           </div>
           </div>
           </div>

<div class="col-md-8">
        <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-shopping-cart"></i>

              <h3 class="box-title">Single Order</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                          <?=$this->session->flashdata('messages');?>
          
      
    <?=form_open('', '','');?>      
            <label>Category</label>
            <select class="form-control" id="category" name="category">

            <option value="0">Choose Category</option>
              <?php 
              $categories = $this->db->select('*')->from('categories')->where('status',1)->order_by('name','ASC');
              foreach($categories->get()->result() as $result){ ?>
              <option value="<?=$result->id;?>"><?=$result->name;?></option>
                <?php } ?>
            </select>
            <label>Service</label>
            <select class="form-control" id="service" name="service">
            <option value="0" min="0" max="0" price="0" info="0">Choose Category</option>
            </select>
            <div class="row">
              <div class="col-lg-4">
                <label>Price/Min</label>
                <div class="input-group">
                   <div class="input-group-addon">
                                        <i class="fa fa-dollar"></i>
                                    </div>
                                    <input type="text" class="form-control" id="priceOrder" readonly> 
                                </div>
              </div>
              <div class="col-lg-4">
                <label>Minimum</label>
                <div class="input-group">
                   <div class="input-group-addon">
                                        <i class="fa fa-arrow-down"></i>
                                    </div>
                                    <input type="text" class="form-control" id="minOrder" readonly>

                                   
                                </div>
              </div>
              <div class="col-lg-4">
                <label>Maximum</label>
                <div class="input-group">
                   <div class="input-group-addon">
                                        <i class="fa fa-arrow-up"></i>
                                    </div>
                                    <input type="text" class="form-control" id="maxOrder" readonly>

                                   
                                </div>
              </div>
            </div>
            <label>Desc</label>
            <textarea class="form-control" readonly id="ketOrder"></textarea>
            <label>Link/Target</label>
            <input type="text" id="link" required="required" class="form-control" placeholder="" name="target">
            <div style="display: none;" id="basic">
                <label>Quantity</label>
                <input type="text" id="quantity" class="form-control" placeholder="0" value="0" name="quantity">
               
            </div>
             <div style="display: none;" id="mention">
            <label>Usernames</label>
            <textarea id="usernames"  class="form-control" placeholder="pisahkan dengan enter" name="usernames"></textarea>
          </div>
            <div style="display: none;" id="mentionf">
                  <label>Username</label>
                  <input type="text" id="username"  class="form-control" placeholder="0" name="username">
                  
           </div>
             <div style="display: none;" id="comment">
                    <label>Comments</label>
                    <textarea  id="comments" class="form-control" placeholder="pisahkan dengan enter" name="comments">
                    </textarea>
                   
            </div>
            <label>Charge</label>
            <input type="number" id="charge" required="required" class="form-control" placeholder="0" readonly><br>		

         
            		</br>
             <button class="btn btn-default" type="submit"><i class="fa fa-shopping-cart"></i> Order</button>
             <button class="btn btn-default" type="reset"><i class="fa fa-refresh"></i> Reset</button>
    <?=form_close();?>
    
           </div>
          

           </div>
         </div>
              <div class="col-lg-4">
            <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-shopping-cart"></i>

              <h3 class="box-title">Status Service</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <label>Pending</label>
                  <div class="progress">
                <div class="progress-bar progress-bar-gray" id="pending" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                  <span class="sr-only">Pending</span>
                </div>
              </div>
              <label>Proses</label>
                <div class="progress">
                <div class="progress-bar progress-bar-info" id="proccess" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                  <span class="sr-only">Proses</span>
                </div>
                
              </div>
              <label>Compeletd</label>
                              <div class="progress">
                <div class="progress-bar progress-bar-green" id="completed" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                  <span class="sr-only">Completed</span>
                </div>
              </div>
              <label>Canceled</label>
                 <div class="progress">
                <div class="progress-bar progress-bar-red" id="canceled" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                  <span class="sr-only">Canceled</span>
                </div>
              </div>
              <label>Partial</label>
                 <div class="progress">

                <div class="progress-bar progress-bar-yellow" id="partial" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                  <span class="sr-only">Partial</span>
                </div>
              </div>
                 
             
                    </div>
                  </div>
                </div>

          
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script type="text/javascript">
    title = "Single Order";
    <?php
    $services = $this->db->select('id,category_id,name,price,min,max,type,info')
    ->from('services')
    ->where('status','1')
    ->order_by('name','ASC'); 
    ?>
    var service_data = <?=json_encode($services->get()->result());?>;
    $("#category").change(function(event) {
      $("#service").html("<option value='0'>Choose Service</option>");
      var th = $(this).val();
      for (var i = 0; i < service_data.length; i++) {
            if(service_data[i].category_id == th){

                  $("#service").append('<option value="'+service_data[i].id+'">'+service_data[i].name+'</option>');
            }
      }
});
$("#comments").keyup(function(event) {
     $("#harga").val($("#comments").val().split("\n").length * price);
});
$("#usernames").keyup(function(event) {
     $("#harga").val($("#usernames").val().split("\n").length * price);
});
$("#quantity").keyup(function(event) {
  $("#charge").val($(this).val() * price);
});
$("#jumlaha").keyup(function(event) {
  $("#hargaa").val($(this).val() * $("#links").val().split("\n").length * price);
});
$("#service").change(function(event) {
      var th = $(this).val();
      for (var i = 0; i < service_data.length; i++) {
            if(service_data[i].id == th){
                   price = service_data[i].price;
                   if(service_data[i].type == 'Default'){
                        $("#basic").show();
                        $("#mention").hide();
                        $("#mentionf").hide();
                        $("#comment").hide();
                   } else if(service_data[i].type == 'CC' || service_data[i].type == 'CCP'){
                        $("#basic").hide();
                        $("#mention").hide();
                        $("#mentionf").hide();
                        $("#comment").show();
                   } else if(service_data[i].type == 'MCL'){
                        $("#basic").hide();
                        $("#mention").show();
                        $("#mentionf").hide();
                        $("#comment").hide();
                  } else if(service_data[i].type == 'MUF'){
                        $("#basic").show();
                        $("#mention").hide();
                        $("#mentionf").show();
                        $("#comment").hide();
                  }     
                   $("#namaLayanan").html(service_data[i].name)
                   $("#minOrder").val(service_data[i].min)
                   $("#maxOrder").val(service_data[i].max)
                   $("#ketOrder").val(service_data[i].info)
                   $("#priceOrder").val(service_data[i].price*service_data[i].min);
                    if($("#service").val() > 0){
    $(".overlay").show();
     
     $.ajax({
       url: '<?=base_url();?>ajax/checkservice?id='+$("#service").val(),
       type: 'GET',
       dataType: 'json',
     })
     .done(function(res) {
      $("#completed").attr({
        style: 'width: '+res.completed+'%'
      });
      $("#canceled").attr({
        style: 'width: '+res.canceled+'%'
      });
      $("#partial").attr({
        style: 'width: '+res.partial+'%'
      });
      $("#proccess").attr({
        style: 'width: '+res.proccess+'%'
      });
      $("#pending").attr({
        style: 'width: '+res.pending+'%'
      });
      
     })
     .fail(function() {
       alert("error,please reload");
     }).always(function(){
        $(".overlay").hide();
     });
   }
   

            }
      }
});
  </script>
<?php $this->load->view('user/footer'); ?>