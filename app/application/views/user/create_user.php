<?php defined('BASEPATH') OR exit('No direct script access allowed');
if($this->Users->level == "1"){
	redirect(base_url());
  exit;
}?>
<?php $this->load->view('user/header'); ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
     <div class="row">
        <div class="col-md-12">

        <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-user-plus"></i>

              <h3 class="box-title">Create User</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             	                          <?=$this->session->flashdata('messages');?>

    <?=form_open('', '','');?>      
            	<label>Username</label>
            <input type="text" name="username" class="form-control" required="required"> <br>
            <label>Password</label>
            <input type="password" name="password" class="form-control"  required="required"> <br>
            <label>Level</label>
            <select class="form-control" name="level">
            	<option value="1">Member [ Rp.5000 ]</option>
            	<?php if($User->level == "0"){ ?>
            	<option value="2">Reseller [ Rp.25000 ]</option>
            	<?php } ?>
            </select> <br>
            <label>Balance</label>
            <input type="number" name="balance" class="form-control" value="0" required="required"> <br>
            <pre>
Register Cost + Balance = Actual Cost 

Member ( 5000 ) + Balance ( 5000 ) = 10000
            </pre>
            
            <button  type="submit" class="btn btn-success btn-block">Submit</button>
    <?=form_close();?>
           
           </div>
           </div>

      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script type="text/javascript">
        title = "Create user";
  </script>
<?php $this->load->view('user/footer'); ?>